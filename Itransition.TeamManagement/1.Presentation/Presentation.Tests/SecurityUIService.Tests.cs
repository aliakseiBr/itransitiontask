﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.EmailServise;
using Itransition.TeamManagement.Presentation.WebUI.Services.SecurityService;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Presentation.Tests
{
    [TestFixture]
    public class SecurityUIServiceTests
    {

        private SortedList<int,User> users = null;
        private ISecurityUIService securityUIService = null;        

        public SecurityUIServiceTests()
        {          
            this.securityUIService = new SecurityUIService(new EmailSenderService());
            users = new SortedList<int, User>()
            {
                {0, new User() {Password = "hRf8wNedh6j21Rac3/PsgQ==", PasswordSalt = "3F#Y7kPw"} },
                {1, new User() {Password = "IWOkNwmVJ+L0vIFzH3t9eg==", PasswordSalt = "!v627/4r"} },
                {2, new User() {Password = "Bf8EGE2ynFTAJGecFEiLLw==", PasswordSalt = "7kZG^WIV"} },
            };
        }

        [Test]        
        [TestCase(0,"X24w9-Yi",ExpectedResult = true)]
        [TestCase(1, "73VyelN0", ExpectedResult = true)]
        [TestCase(2, "dQ-33*+3", ExpectedResult = true)]
        public bool CheckUserPassword_User_Passwors_Result_true(int userNumber, string password)
        {
            return securityUIService.CheckUserPassword(users[userNumber], password);           
        }
    }
}
