﻿using System;
using System.Linq;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using Itransition.TeamManagement.Domain.Services.IServices;
using Itransition.TeamManagement.Presentation.WebUI.App_Start;
using Itransition.TeamManagement.Presentation.WebUI.Services.AccountServices;

namespace Itransition.TeamManagement.Presentation.WebUI
{
    public class MvcApplication : HttpApplication
    {
        private const string UserIdRegex = "(?<=userId=)([0-9]*)(?=;)";
        private const string CompanyIdRegex = "(?<=companyId=)([0-9]*)(?=;)";
        private const string UserRolesRegex = "(?<=userRoles=)(.*)(?=;)";
        private const char UserRolesSeparator = '|';

        protected void Application_Start()
        {
            AutofacConfig.ConfigureContainer();
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_AuthenticateRequest()
        {
            var a = DependencyResolver.Current.GetService<IUserDomainService>();
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                int userId = GetUserId(authTicket);
                int companyId = GetCompanyId(authTicket);
                string[] roles = GetUserRoles(authTicket);
                IIdentity id = new CustomFormsIdentity(authTicket, userId, companyId);
                IPrincipal principal = new GenericPrincipal(id, roles);
                Context.User = principal;
            }
        }

        private int GetUserId(FormsAuthenticationTicket authTicket)
        {
            string strUserId = Regex.Match(authTicket.UserData, UserIdRegex).Value;
            return Convert.ToInt32(strUserId);
        }

        private int GetCompanyId(FormsAuthenticationTicket authTicket)
        {
            string strCompanyId = Regex.Match(authTicket.UserData, CompanyIdRegex).Value;
            return Convert.ToInt32(strCompanyId);
        }

        private string[] GetUserRoles(FormsAuthenticationTicket authTicket)
        {
            string strUserRoles = Regex.Match(authTicket.UserData, UserRolesRegex).Value;
            string[] roles = strUserRoles.Split(UserRolesSeparator);
            return roles;
        }
    }
}
