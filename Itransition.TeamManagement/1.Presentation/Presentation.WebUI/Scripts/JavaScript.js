﻿$(document).ready(function () {
    $('#searchInput').AddXbutton({ img: '@Url.Content("~/Content/x.gif")' });
    //$('#searchInput').focus();
});
(function ($) {
    $.fn.AddXbutton = function (options) {
        var defaults = {
            img: 'x.gif'
        };
        var opts = $.extend(defaults, options);
        $obj = $(this);
        $(this).each(
            function (i) {
                $(this).after(
                    $('<input type="image" id="xButton' + i + '" src="' + opts['img'] + '" />')
                        .css({ 'display': 'none', 'cursor': 'pointer', 'marginLeft': '-15px' })
                        .click(function () {
                            $obj.val('').focus();
                            $("#xButton" + i).hide();
                        }))
                    .keyup(function () {
                        if ($(this).val().length > 0) {
                            $("#xButton" + i).show();
                        } else {
                            $("#xButton" + i).hide();
                        }
                        if ($(this).val() != '') $("#xButton" + i).show();
                    });
                if ($(this).val().length > 0) {
                    $("#xButton" + i).show();
                } else {
                    $("#xButton" + i).hide();
                }
                if ($(this).val() != '') $("#xButton" + i).show();
            });
    };
})(jQuery); 