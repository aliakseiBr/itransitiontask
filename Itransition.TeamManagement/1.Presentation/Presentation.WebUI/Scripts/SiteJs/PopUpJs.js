﻿    $(document).ready(function () {
        $("#popUpResult").dialog({
            autoOpen: false,
            title: 'Comment',
            width: 500,
            height: 'auto',
            modal: true
        });
});

$(function () {
    $('.myTable').on('click', '.popUp', function () {
        var userId = $(this).attr('id');
        $.ajax({
            type: 'GET',
            url: '/UserManager/PopUpComment',
            data: {
                "userId": userId
            },
            dataType: 'text',
            success: function (data) {
                $("#popUpResult").html(data);
                $("#popUpResult").dialog("open");
            },
            error: function (data) {
                console.log(data);
            }
        });

        return false;
    });
});
