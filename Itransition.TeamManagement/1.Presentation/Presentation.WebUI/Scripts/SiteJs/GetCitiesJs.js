﻿$(function () {
    $('#country').change(function () {
        var countryId = $(this).val();
        $.ajax({
            type: 'GET',
            url: '/UserManager/GetCities',
            data: {
                "countryId": countryId
            },
            dataType: 'text',
            success: function (data) {
                $('#city').html(data);
                console.log("append");
            },
            error: function (data) {
                console.log(data);
            }
        });

        return false;
    });
});