﻿Vue.component('work-logs-pop-up', {
    props: ['logs', 'taskid', 'logday', 'mouseposition'],
    template: '#work-logs-pop-up-template',
    methods:
    {
        deleteWorkLog: function (id) {
            this.$emit('delete-log-work', id);
        },
        addWorkLog: function () {
            this.$emit('get-log-work-form-pop-up', this.taskid, this.logday, this.mouseposition);
        },
        reductionStrTo20Char: function (str) {
            if (str == null) {
                return null;
            }
            if (str.length < 20) {
                return str;
            }
            else {
                return str.slice(0, 19) + "...";
            }
        },
        convertNetDateToViewTableDate: function (netDate) {
            return moment(netDate).format("DD/MMM");
        },
        getformatTableValue: function (value) {
            if (Number.isFinite(value)) {
                if (value - Math.floor(value) > 0) {
                    return value.toFixed(2);
                }
                else {
                    return value.toFixed(0);
                }
            }
            else {
                return null;
            }
        },

    }
})