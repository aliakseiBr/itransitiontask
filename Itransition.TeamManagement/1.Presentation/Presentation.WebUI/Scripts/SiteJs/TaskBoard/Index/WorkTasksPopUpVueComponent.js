﻿Vue.component('work-tasks-pop-up', {
    props: ['tasks', 'mouseposition'],
    template: '#work-tasks-pop-up-template',
    methods:
    {
        addWorkTaskToTable: function (name, description, id) {
            this.$emit('add-work-task-to-table', name, description, id);
        }
    }
})