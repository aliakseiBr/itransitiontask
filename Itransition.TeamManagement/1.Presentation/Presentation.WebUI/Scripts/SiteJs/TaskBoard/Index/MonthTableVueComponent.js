﻿Vue.component('month-table', {
    template: '#month-table-template',
    data() {
        return {
            tableWeeksName: null,
            responseData: new Array(),
            logsDay: null,
            startPeriod: null,
            endPeriod: null,
            counterMonths: 0,
            errors: new Array()
        }
    },
    methods: {
        switchTableType: function (type) {
            this.$emit('switch-table-type', type);
        },
        dataLoading: function () {
            var self = this;
            axios
                .post(TaskBoard_MonthTable,
                    {
                        StartPeriod: self.startPeriod,
                        EndPeriod: self.endPeriod
                    })
                .then(response => (self.responseData = response.data))
                .catch(error => {
                    console.log(error.response)
                });
        },       
        getTableWeeksName: function () {    
            Date.prototype.getWeek = function () {
                var onejan = new Date(this.getFullYear(), 0, 1);
                return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
            }

            var tableWeeksName = new Array();
            var startPeriod = new Date(this.startPeriod);
            var endPeriod = new Date(this.endPeriod);
            var numberOfWeeks = Math.ceil((endPeriod.getDate() - (endPeriod.getDay() ? endPeriod.getDay() : 7)) / 7) + 1;

            for (var i = 0; i < numberOfWeeks; i++) {
                tableWeeksName.push('Week' + ' ' + startPeriod.getWeek());
                startPeriod = new Date(startPeriod.setHours(168));
            }

            return tableWeeksName;
        },     
        getPreviousMonth: function () {
            this.counterMonths--;
            this.switchMonth();
        },
        getNetxMonth: function () {
            this.counterMonths++;
            this.switchMonth();     
        },
        getCurrentMonth: function () {
            this.setDefaultPeriod()
            this.dataLoading();
        },
        switchMonth: function () {
            var date = new Date();
            this.startPeriod = new Date(date.getFullYear(), date.getMonth() + this.counterMonths, 1);
            this.endPeriod = new Date(date.getFullYear(), date.getMonth() + this.counterMonths + 1, 0);
            this.dataLoading();
            this.tableWeeksName = this.getTableWeeksName();
        },
        getWorkLogsPopUp: function (event) {
            var currentTaskId = this.getCurrentTaskId(event);
            var currentLogDay = this.getCurrentLogDay(event);
            var dayOfWeek = this.getDayOfWeek(event);
            var startPeriod = dayOfWeek.startOf('isoWeek').toDate();
            var endPeriod = dayOfWeek.endOf('isoWeek').toDate(); 
            let mousePosition = this.getCurrentMousePosition(event);

            this.$emit('get-work-logs-pop-up', currentTaskId, currentLogDay, startPeriod, endPeriod, mousePosition);  
        },
        getLogWorkFormPopUp: function (event) {
            var currentTaskId = this.getCurrentTaskId(event);
            var currentLogDay = this.getCurrentLogDay(event);
            let mousePosition = this.getCurrentMousePosition(event);

            this.$emit('get-log-work-form-pop-up', currentTaskId, currentLogDay, mousePosition);
        },
        getCurrentTaskId: function (event) {
            var taskPosition = event.currentTarget.parentNode.rowIndex - 2;
            var currentTaskId = this.responseData.TaskWithLogsTime[taskPosition].WorkTask.WorkTaskId;

            return currentTaskId;
        },
        getCurrentLogDay: function (event) {
            var dayOfWeek = this.getDayOfWeek(event);
            var currentLogDay = dayOfWeek.startOf('isoWeek').toDate();

            return currentLogDay;
        },
        getDayOfWeek: function (event) {
            var weekPosition = event.currentTarget.cellIndex - 2;
            var startMonth = new Date(this.startPeriod);
            var dayOfWeek = moment(startMonth.setHours(weekPosition * 7 * 24));            

            return dayOfWeek;
        },
        getCurrentMousePosition: function (event) {
            var cursorPositionX = event.pageX;
            var cursorPositionY = event.pageY;
            let mousePosition = { x: cursorPositionX, y: cursorPositionY };

            return mousePosition;
        },
        getformatTableValue: function (value) {
            if (Number.isFinite(value)) {
                if (value - Math.floor(value) > 0) {
                    return value.toFixed(2);
                }
                else {
                    return value.toFixed(0);
                }
            }
            else {
                return null;
            }
        },
        getFormatTableDate: function (startPeriod, endPeriod) {
            return "from " + moment(startPeriod).format("DD/MMM/YY") + " to " + moment(endPeriod).format("DD/MMM/YY");
        },
        addWorkTaskToTable: function (name, id) {
            var self = this;
            axios
                .post(TaskBoard_MonthTable,
                    {
                        StartPeriod: self.startPeriod,
                        EndPeriod: self.endPeriod
                    })
                .then(
                    function (response) {
                        self.responseData = response.data;
                        var taskHoursPerMonth = new Array();
                        for (var i = 0; i < self.tableWeeksName.length; i++) {
                            taskHoursPerMonth.push(0);
                        }
                        self.responseData.TaskWithLogsTime.push({ "WorkTask": { "WorkTaskId": id, "Name": name }, "TaskHoursPerMonth": taskHoursPerMonth, "SumTaskHoursPerMonth": 0 })
                    }
                )
                .catch(error => {
                    console.log(error.response)
                });
        },
        setDefaultPeriod: function () {
            var date = new Date();
            this.startPeriod = new Date(date.getFullYear(), date.getMonth(), 1);
            this.endPeriod = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            this.counterMonths = 0;
            this.tableWeeksName = this.getTableWeeksName();
        },
    },
    created() {
        this.setDefaultPeriod();
        this.dataLoading();
    }
})