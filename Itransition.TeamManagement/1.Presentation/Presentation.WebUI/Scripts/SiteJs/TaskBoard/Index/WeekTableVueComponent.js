﻿Vue.component('week-table', {
    template: '#week-table-template',
    data() {
        return {
            responseData: new Array(),
            taskWithLogsTimeCache: new Array(),
            startPeriod: null,
            endPeriod: null,
            currentDayTablePosition: 1,
            counterWeeks: 0,
            numberOfdays: 7,
            tableDaysName: new Array(),
        }
    },
    methods: {
        switchTableType: function (type) {
            this.$emit('switch-table-type', type);
        },
        dataLoading: async function () {
            var self = this;
            axios
                .post(TaskBoard_WeekTable,
                    {
                        StartPeriod: self.startPeriod,
                        EndPeriod: self.endPeriod
                    })
                .then(response => (self.responseData = response.data, self.taskWithLogsTimeCache = new Array()))
                .catch(error => {
                    console.log(error.response)
                });
        },
        getTableDaysName: function () {
            var m_names = new Array("Jan", "Feb", "Mar",
                "Apr", "May", "Jun", "Jul", "Aug", "Sep",
                "Oct", "Nov", "Dec");
            var d_names = new Array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat')
            var days = new Array();
            var startPeriod = new Date(this.startPeriod);

            for (var i = 0; i < this.numberOfdays; i++) {
                days.push(d_names[startPeriod.getDay()] + " " + startPeriod.getDate() + '/' + m_names[startPeriod.getMonth()]);
                startPeriod = new Date(startPeriod.setHours(24));
            }

            return days;
        },
        getPreviousWeek: function () {
            var startDay = new Date(this.startPeriod)
            this.startPeriod = new Date(startDay.setHours(-7 * 24));
            this.endPeriod = getSunday(new Date(this.startPeriod));
            this.counterWeeks--;
            this.dataLoading();
            this.tableDaysName = this.getTableDaysName();

        },
        getNetxWeek: function () {
            var startDay = new Date(this.startPeriod)
            this.startPeriod = new Date(startDay.setHours(7 * 24));
            this.endPeriod = getSunday(new Date(this.startPeriod));
            this.counterWeeks++;
            this.dataLoading();
            this.tableDaysName = this.getTableDaysName();
        },
        getCurrentWeek: function () {
            this.setDefaultPeriod()
            this.dataLoading();
        },
        getWorkLogsPopUp: function (event) {  
            var currentTaskId = this.getCurrentTaskId(event);
            var currentLogDay = this.getCurrentLogDay(event);
            var dayOfWeek = this.getDayOfWeek(event);
            var startDay = new Date(this.startPeriod);
            var startLogPeriod = new Date(startDay.setHours(dayOfWeek * 24));
            var endLogPeriod = startLogPeriod;
            let mousePosition = this.getCurrentMousePosition(event);
            this.$emit('get-work-logs-pop-up', currentTaskId, currentLogDay, startLogPeriod, endLogPeriod, mousePosition);
        },
        getWorkTasksPopUp: function (event) {
            let mousePosition = this.getCurrentMousePosition(event);
            this.$emit('get-work-tasks-pop-up', this.startPeriod, this.endPeriod, this.responseData.TaskWithLogsTime, mousePosition);
        },
        getLogWorkFormPopUp: function (event) {
            var currentTaskId = this.getCurrentTaskId(event);
            var currentLogDay = this.getCurrentLogDay(event);
            let mousePosition = this.getCurrentMousePosition(event);

            this.$emit('get-log-work-form-pop-up', currentTaskId, currentLogDay, mousePosition);
        },
        getCurrentTaskId: function (event) {
            var taskPosition = event.currentTarget.parentNode.rowIndex - 2;
            var currentTaskId = this.responseData.TaskWithLogsTime[taskPosition].WorkTask.WorkTaskId;

            return currentTaskId;
        },
        getCurrentLogDay: function (event) {
            var dayOfWeek = event.currentTarget.cellIndex - 2;
            var startDay = new Date(this.startPeriod)
            var currentLogDay = new Date(startDay.setHours(dayOfWeek * 24));

            return currentLogDay;
        },
        getDayOfWeek: function (event) {
            return event.currentTarget.cellIndex - 2;
        },    
        getCurrentDayTablePosition: function () {             
            var currentTime = new Date().getTime();
            var startTime = this.startPeriod.getTime();
            var endTime = this.endPeriod.getTime();

            if (currentTime > startTime && currentTime < endTime) {
                var date = moment(new Date());
                var correctingArrayIndex = 1

                return date.day() - correctingArrayIndex;
            }
            else {
                return -1;
            }
           
        },
        getCurrentMousePosition: function (event) {
            var cursorPositionX = event.pageX;
            var cursorPositionY = event.pageY;
            let mousePosition = { x: cursorPositionX, y: cursorPositionY };

            return mousePosition;
        },
        getformatTableValue: function (value) {
            if (Number.isFinite(value)) {
                if (value - Math.floor(value) > 0) {
                    return value.toFixed(2);
                }
                else {
                    return value.toFixed(0);
                }
            }
            else {
                return null;
            }
        },
        getTaskHoursPerWeekDefault: function (numberOfdays) {
            var taskHoursPerWeek = new Array();

            for (var i = 0; i < numberOfdays; i++) {
                taskHoursPerWeek.push(0);
            }

            return taskHoursPerWeek;
        },
        addWorkTaskToTable: function (name, description, id) {
            var self = this;
            var taskHoursPerWeek = self.getTaskHoursPerWeekDefault(self.numberOfdays);

            self.responseData.TaskWithLogsTime.push({ "WorkTask": { "WorkTaskId": id, "Name": name, "Description": description }, "TaskHoursPerWeek": taskHoursPerWeek, "SumTaskHoursPerWeek": 0 });
            self.taskWithLogsTimeCache.push({ "WorkTask": { "WorkTaskId": id, "Name": name, "Description": description }, "TaskHoursPerWeek": taskHoursPerWeek, "SumTaskHoursPerWeek": 0 });

        },
        updateTableValue: function () {
            var self = this;

            axios
                .post(TaskBoard_WeekTable,
                    {
                        StartPeriod: self.startPeriod,
                        EndPeriod: self.endPeriod
                    })
                .then(
                    function (response) {

                        self.responseData = response.data;
                        var taskHoursPerWeek = self.getTaskHoursPerWeekDefault(self.numberOfdays);

                        for (var i = 0; i < self.taskWithLogsTimeCache.length; i++) {
                            self.taskWithLogsTimeCache[i].TaskHoursPerWeek = taskHoursPerWeek;
                        }
                        
                        for (var i = 0; i < self.taskWithLogsTimeCache.length; i++) {
                            for (var j = 0; j < self.responseData.TaskWithLogsTime.length; j++) {
                                if (self.taskWithLogsTimeCache[i].WorkTask.WorkTaskId == self.responseData.TaskWithLogsTime[j].WorkTask.WorkTaskId) {
                                    self.taskWithLogsTimeCache[i].TaskHoursPerWeek = self.responseData.TaskWithLogsTime[j].TaskHoursPerWeek;
                                    self.taskWithLogsTimeCache[i].SumTaskHoursPerWeek = self.responseData.TaskWithLogsTime[j].SumTaskHoursPerWeek;
                                    self.responseData.TaskWithLogsTime.splice(j, 1);
                                    break;
                                }
                            }
                        }

                        for (var i = 0; i < self.taskWithLogsTimeCache.length; i++) {
                            self.responseData.TaskWithLogsTime.push(self.taskWithLogsTimeCache[i])
                        }
                    },
                )
                .catch(error => {
                    console.log(error.response)
                });
        },
        setDefaultPeriod: function () {
            this.startPeriod = getMonday(new Date());
            this.endPeriod = getSunday(new Date(this.startPeriod));
            this.counterWeeks = 0;
            this.tableDaysName = this.getTableDaysName();
        },
    },
    created() {
        this.setDefaultPeriod();
        this.dataLoading();   
    },
    mounted() {
           
    },
    beforeUpdate() {
        this.currentDayTablePosition = this.getCurrentDayTablePosition();
    }
})