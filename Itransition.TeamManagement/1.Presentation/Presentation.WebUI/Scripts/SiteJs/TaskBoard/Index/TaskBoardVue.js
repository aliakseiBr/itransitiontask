﻿function getMonday(d) {
    d = new Date(d.setHours(0, 0, 0, 0));
    var day = d.getDay(),
        diff = d.getDate() - day + (day == 0 ? -6 : 1);
    return new Date(d.setDate(diff));
}

function getSunday(monday) {
    return new Date(monday.setHours(144));
}

var vue = new Vue({
    el: '#task-board',
    data() {
        return {
            tableType: 'week-table',
            showLogWorkFormPopUp: false,
            showWorkLogsPopUp: false,
            showWorkTasksPopUp: false,
            currentTaskId: null,
            currentLogDay: null,
            mousePosition: { x: 0, y: 0},
            errors: new Array(),
            dayLogs: new Array(),
            workTasks: new Array()
        }
    },
    methods: {
        switchTableType: function (type) {
            this.tableType = type;
        },
        getLogWorkFormPopUp: function (currentTaskId, currentLogDay, mousePosition) {
            this.showWorkLogsPopUp = false;
            this.showLogWorkFormPopUp = true;
            this.currentTaskId = currentTaskId;
            this.currentLogDay = currentLogDay;
            this.mousePosition = mousePosition;
        },   
        getWorkLogsPopUp: function (currentTaskId, currentLogDay, startPeriod, endPeriod, mousePosition) {
            this.currentTaskId = currentTaskId;
            this.currentLogDay = currentLogDay;
            this.mousePosition = mousePosition;

            axios
                .post(TaskBoard_LogsForDay,
                    {
                        taskId: currentTaskId,
                        startPeriod: startPeriod,
                        endPeriod: endPeriod
                    })
                .then(response => (this.dayLogs = response.data))
                .catch(error => {
                    console.log(error.response)
                });

            this.showWorkLogsPopUp = true;
        },
        getWorkTasksPopUp: function (startPeriod, endPeriod, taskWithLogsTime, mousePosition) {
            var self = this
            axios
                .post(TaskBoard_GetOpenWorkTaskWithoutLogs,
                    {
                        endPeriod: endPeriod,
                        startPeriod: startPeriod,
                    })
                .then(function (response) {
                    var tasks = response.data;

                    for (var i = 0; i < taskWithLogsTime.length; i++) {
                        for (var j = 0; j < tasks.length; j++) {
                            if (taskWithLogsTime[i].WorkTask.WorkTaskId == tasks[j].WorkTaskId) {
                                tasks.splice(j, 1);
                                break;
                            }
                        }
                    }
                    self.workTasks = tasks;
                    self.mousePosition = mousePosition;
                })
                .catch(error => {
                    console.log(error.response)
                });

            this.showWorkTasksPopUp = true;
        },
        addWorkTaskToTable: function (name, description, id) {
            this.showWorkTasksPopUp = false;
            vue.$refs.table.addWorkTaskToTable(name, description, id);
        },    
        closeLogWorkFormPopUp: function () {
            this.showLogWorkFormPopUp = false;
            this.errors = new Array();
        },
        convertStrTimeSpentToNum: function (timeSpent) { 
            var hoursInputRegex = /^(?<hours>[1-9]\d*)h$/;
            var minInputRegex = /(?<min>^[1-9]\d*)m$/;
            var hoursAndMinInputRegex = /^(?<hours>[1-9]\d*)h( ((?<min>[1-9]\d*)m))$/;
            var fractionalHoursInputRegex = /^(?<hours>([1-9]\d*)(\.[0-9]\d*)?)h$/;

            if (hoursInputRegex.test(timeSpent)) {
                var result = hoursInputRegex.exec(timeSpent);

                return Number(result.groups.hours);
            }
            else if (minInputRegex.test(timeSpent)) {
                var result = minInputRegex.exec(timeSpent);

                return (Number(result.groups.min)/60);
            }            
            else if (fractionalHoursInputRegex.test(timeSpent)) {
                var result = fractionalHoursInputRegex.exec(timeSpent);

                return Number(result.groups.hours);
            }
            else if (hoursAndMinInputRegex.test(timeSpent)) {
                var result = hoursAndMinInputRegex.exec(timeSpent);

                return Number(result.groups.hours) + (Number(result.groups.min)/60);
            }
            else {
                return null;
            }
        },
        saveWorkLogForm: function (timeSpentStr, description) {
            var self = this;

            var timeSpentNumber = self.convertStrTimeSpentToNum(timeSpentStr);

            if (timeSpentNumber == null) {
                self.errors = ["Time spent is not correct. Valid format: 1h; 5m; 1.5h; 1h 5m"];
            }
            else if (timeSpentNumber > 12)
            {
                self.errors = ["Time spent cannot be more than 12 hours"];
            }
            else {
                axios
                    .post(TaskBoard_CreateLogWork,
                        {
                            taskId: self.currentTaskId,
                            logDay: self.currentLogDay,
                            timeSpent: timeSpentNumber,
                            description: description
                        })
                    .then(function (response) {
                        self.errors = response.data.ErrorMessages;

                        if (response.data.IsValid) {
                            self.showLogWorkFormPopUp = false;
                            vue.$refs.table.updateTableValue();
                        }
                    })
                    .catch(error => {
                        console.log(error.response)
                    });
            }            
        },        
        deleteLogWork: function (id) {
            if (confirm('Delete?')) {

                this.showWorkLogsPopUp = false;

                axios
                    .post(TaskBoard_DeleteLogWork,
                        {
                            logId: id,
                        })
                    .then(
                        function () {
                            vue.$refs.table.updateTableValue();
                        })
                    .catch(error => {
                        console.log(error.response)
                    });
            } 
        },
    }
});