﻿Vue.component('log-work-form-pop-up', {
    data() {
        return {
            timeSpent: null,
            description: null
        }
    },
    props: ['errors', 'logdate', 'mouseposition'],
    template: '#log-work-form-pop-up-template',
    methods:
    {
        sendWorkLogForm: function () {
            this.$emit('send-work-log-form', this.timeSpent, this.description);
        },
        getFormatLogDate: function(logdate) {
            return moment(logdate).format("DD/MMM/YY");
        }
    }
})