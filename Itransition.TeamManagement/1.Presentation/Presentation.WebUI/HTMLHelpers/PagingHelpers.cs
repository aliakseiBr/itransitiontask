﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.PlaginationViewModels;

namespace Itransition.TeamManagement.Presentation.WebUI.HtmlHelpers
{
    public static class PagingHelpers
    {
        public static List<PlaginationResultViewModel> PageLinks(this HtmlHelper html, PageInfoViewModel pageInfo, Func<int, string> pageUrl)
        {
            List<PlaginationResultViewModel> pageResults = new List<PlaginationResultViewModel>();
            int startIndex = GetStartIndexForPageLinks(pageInfo.CurrentPage, pageInfo.TotalPages);
            int endIndex = GetEndIndexForPageLinks(pageInfo.CurrentPage, pageInfo.TotalPages);

            if (pageInfo.CurrentPage > 1)
            {
                pageResults.Add(new PlaginationResultViewModel() { TypeLink = HtmlLinkTypeEnum.FirstSkipLink, PageUrl = pageUrl(pageInfo.CurrentPage - 1) });
            }

            GetLinks(startIndex, endIndex, pageResults, pageInfo, pageUrl);

            if (pageInfo.CurrentPage < pageInfo.TotalPages)
            {
                pageResults.Add(new PlaginationResultViewModel() { TypeLink = HtmlLinkTypeEnum.LastSkipLink, PageUrl = pageUrl(pageInfo.CurrentPage + 1) });
            }

            return pageResults;
        }

        private static void GetLinks(int startIndex, int endIndex, List<PlaginationResultViewModel> pageResults, PageInfoViewModel pageInfo, Func<int, string> pageUrl)
        {
            for (int page = startIndex; page <= endIndex; page++)
            {
                if (page == pageInfo.CurrentPage)
                {
                    pageResults.Add(new PlaginationResultViewModel() { TypeLink = HtmlLinkTypeEnum.SelectedLink, PageName = page.ToString(), PageUrl = pageUrl(page) });
                    continue;
                }

                pageResults.Add(new PlaginationResultViewModel() { PageName = page.ToString(), PageUrl = pageUrl(page) });
            }
        }

        private static int GetStartIndexForPageLinks(int currentPage, int totalPage)
        {
            int startIndex = 0;

            if (currentPage == totalPage)
            {
                startIndex = currentPage - 2;
            }
            else
            {
                startIndex = currentPage - 1;
            }

            if (startIndex < 1)
            {
                startIndex = 1;
            }

            return startIndex;
        }

        private static int GetEndIndexForPageLinks(int currentPage, int totalPage)
        {
            int endIndex = 0;

            if (currentPage == 1)
            {
                endIndex = currentPage + 2;
            }
            else
            {
                endIndex = currentPage + 1;
            }

            if (endIndex > totalPage)
            {
                endIndex = totalPage;
            }

            return endIndex;
        }
    }
}
