﻿using Itransition.TeamManagement.Presentation.WebUI.Services.SortingService;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;

namespace Itransition.TeamManagement.Presentation.WebUI.HtmlHelpers
{
    public static class SortingHelpers
    {
        private const string PageName = "page";
        private const string FilterName = "search";

        public static RouteValueDictionary GetRouteValueDictionary(
            this HtmlHelper html,
            SortedList<int, SortingFieldAndType> currentSorting,
            int page,
            string currentFilter)
        {
            RouteValueDictionary routeDictionary = new RouteValueDictionary { { PageName, page }, { FilterName, currentFilter } };

            foreach (var item in currentSorting)
            {
                routeDictionary.Add(item.Value.SortingUserTableField.ToString(), item.Value.SortingType.ToString());
            }

            return routeDictionary;
        }

        public static RouteValueDictionary GetRouteValueDictionary(
            this HtmlHelper html,
            SortedList<int, SortingFieldAndType> currentSorting,
            SortingUserTableFieldsEnum? sortingField,
            int currentPage,
            string currentFilter)
        {
            RouteValueDictionary routeDictionary = new RouteValueDictionary { { PageName, currentPage }, { FilterName, currentFilter } };
            AddUpdateCurrentSortingToDictionary(sortingField, currentSorting, routeDictionary);

            return routeDictionary;
        }

        private static void AddUpdateCurrentSortingToDictionary(
            SortingUserTableFieldsEnum? sortingField,
            SortedList<int, SortingFieldAndType> currentSorting,
            RouteValueDictionary routeDictionary)
        {
            bool containCurrentField = false;

            foreach (var item in currentSorting)
            {
                if (item.Value.SortingUserTableField == sortingField)
                {
                    containCurrentField = true;
                    if (item.Value.SortingType == SortingTypeEnum.Asc)
                    {
                        routeDictionary.Add(item.Value.SortingUserTableField.ToString(), SortingTypeEnum.Des.ToString());
                    }
                    else if (item.Value.SortingType == SortingTypeEnum.Des)
                    {
                        continue;
                    }
                }
                else
                {
                    routeDictionary.Add(item.Value.SortingUserTableField.ToString(), item.Value.SortingType.ToString());
                }
            }

            if (containCurrentField == false)
            {
                routeDictionary.Add(sortingField.ToString(), SortingTypeEnum.Asc.ToString());
            }
        }
    }
}