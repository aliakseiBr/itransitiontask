﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.UserViewModels.AbstractUserViewModels
{
    public abstract class UserViewModel
    {
        public virtual int? UserId { get; set; }

        [Required(
            ErrorMessageResourceType = typeof(Properties.LocalResource.User.CreateUser),
            ErrorMessageResourceName = "FirstNameRequired")]
        [StringLength(
            25,
            MinimumLength = 2,
            ErrorMessageResourceType = typeof(Properties.LocalResource.User.CreateUser),
            ErrorMessageResourceName = "FirstNameLength")]
        public virtual string FirstName { get; set; }

        [Required(
            ErrorMessageResourceType = typeof(Properties.LocalResource.User.CreateUser),
            ErrorMessageResourceName = "LastNameRequired")]
        [StringLength(
            25,
            MinimumLength = 2,
            ErrorMessageResourceType = typeof(Properties.LocalResource.User.CreateUser),
            ErrorMessageResourceName = "LastNameLength")]
        public virtual string LastName { get; set; }

        [DataType(DataType.PhoneNumber)]
        [StringLength(
            17,
            MinimumLength = 13,
            ErrorMessageResourceType = typeof(Properties.LocalResource.User.CreateUser),
            ErrorMessageResourceName = "PhoneLength")]
        [RegularExpression(
            "^[0-9 +]+$",
            ErrorMessageResourceType = typeof(Properties.LocalResource.User.CreateUser),
            ErrorMessageResourceName = "PhoneExpression")]
        public virtual string Phone { get; set; }

        [Required(
           ErrorMessageResourceType = typeof(Properties.LocalResource.User.CreateUser),
           ErrorMessageResourceName = "EmailRequired")]
        [RegularExpression(
            ".+\\@.+\\..+",
            ErrorMessageResourceType = typeof(Properties.LocalResource.User.CreateUser),
            ErrorMessageResourceName = "EmailExpression")]
        [StringLength(
            50,
            ErrorMessageResourceType = typeof(Properties.LocalResource.User.CreateUser),
            ErrorMessageResourceName = "EmailLength")]
        public virtual string Email { get; set; }

        [Required(
           ErrorMessageResourceType = typeof(Properties.LocalResource.User.CreateUser),
           ErrorMessageResourceName = "TitleRequired")]
        public virtual int? TitleId { get; set; }

        public virtual List<SelectListItem> Titles { get; set; }

        [Required(
           ErrorMessageResourceType = typeof(Properties.LocalResource.User.CreateUser),
           ErrorMessageResourceName = "RoleRequired")]
        public virtual int? RoleId { get; set; }

        public virtual List<SelectListItem> Roles { get; set; }

        [Required(
           ErrorMessageResourceType = typeof(Properties.LocalResource.User.CreateUser),
           ErrorMessageResourceName = "CountryRequired")]
        public virtual int? CountryId { get; set; }

        public virtual List<SelectListItem> Contries { get; set; }

        [Required(
           ErrorMessageResourceType = typeof(Properties.LocalResource.User.CreateUser),
           ErrorMessageResourceName = "CityRequired")]
        public virtual int? CityId { get; set; }

        public virtual List<SelectListItem> Cities { get; set; }

        [StringLength(
            250,
            ErrorMessageResourceType = typeof(Properties.LocalResource.User.CreateUser),
            ErrorMessageResourceName = "CommentLength")]

        public virtual string Comment { get; set; }

        public virtual int? CommentId { get; set; }
    }
}