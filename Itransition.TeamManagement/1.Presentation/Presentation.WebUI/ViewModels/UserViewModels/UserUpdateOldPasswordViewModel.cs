﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.UserViewModels
{
    public class UserUpdateOldPasswordViewModel
    {
        public int? UserId { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [Required]
        [RegularExpression(
            "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^\\w\\s]).{6,}",
            ErrorMessageResourceType = typeof(Properties.LocalResource.User.PasswordForm),
            ErrorMessageResourceName = "PasswordExpression")]
        [StringLength(
            25,
            MinimumLength = 8,
            ErrorMessageResourceType = typeof(Properties.LocalResource.User.PasswordForm),
            ErrorMessageResourceName = "PasswordLength")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [RegularExpression(
            "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^\\w\\s]).{6,}",
            ErrorMessageResourceType = typeof(Properties.LocalResource.User.PasswordForm),
            ErrorMessageResourceName = "PasswordExpression")]
        [StringLength(
            25,
            MinimumLength = 8,
            ErrorMessageResourceType = typeof(Properties.LocalResource.User.PasswordForm),
            ErrorMessageResourceName = "PasswordLength")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}