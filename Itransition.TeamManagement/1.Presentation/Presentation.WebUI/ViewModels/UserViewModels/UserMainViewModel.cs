﻿using Itransition.TeamManagement.Domain.Core.Entities;
using System.Collections.Generic;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.UserViewModels
{
    public class UserMainViewModel
    {
        public int? UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Title { get; set; }

        public string CountryName { get; set; }

        public string CityName { get; set; }

        public bool AccessToChange { get; set; }
    }
}