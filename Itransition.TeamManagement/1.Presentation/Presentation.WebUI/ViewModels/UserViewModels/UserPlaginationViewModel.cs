﻿using Itransition.TeamManagement.Presentation.WebUI.Services.SortingService;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.PlaginationViewModels;
using System.Collections.Generic;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.UserViewModels
{
    public class UserPlaginationViewModel
    {
        public List<UserMainViewModel> Users { get; set; }

        public SortedList<int, SortingFieldAndType> СurrentSorting { get; set; }

        public PageInfoViewModel PageInfo { get; set; }

        public string Filter { get; set; }
    }
}