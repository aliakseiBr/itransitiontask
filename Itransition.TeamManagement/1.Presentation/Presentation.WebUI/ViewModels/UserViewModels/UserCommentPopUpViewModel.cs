﻿using Itransition.TeamManagement.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.UserViewModels
{
    public class UserCommentPopUpViewModel
    {
        public int UserId { get; set; }

        public List<Comment> UserComments { get; set; }
    }
}