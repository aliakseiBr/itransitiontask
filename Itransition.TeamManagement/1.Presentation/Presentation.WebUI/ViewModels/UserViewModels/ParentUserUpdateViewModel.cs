﻿namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.UserViewModels
{
    public class ParentUserUpdateViewModel
    {
        public UserUpdateViewModel UserUpdateViewModel { get; set; }

        public UserUpdateOldPasswordViewModel UserUpdateOldPasswordViewModel { get; set; }

        public string ErrorMessage { get; set; }
    }
}