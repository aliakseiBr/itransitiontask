﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.TaskBoardViewModels
{
    public class WorkLogsPeriod
    {
        public DateTime StartPeriod { get; set; }

        public DateTime EndPeriod { get; set; }
    }
}