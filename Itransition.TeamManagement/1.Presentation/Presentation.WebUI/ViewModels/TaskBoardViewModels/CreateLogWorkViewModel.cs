﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.TaskBoardViewModels
{
    public class CreateLogWorkViewModel
    {
       public int? TaskId { get; set; }

       public DateTime? LogDay { get; set; }

        [Required(
            ErrorMessageResourceType = typeof(Properties.LocalResource.TaskBoard.Index),
            ErrorMessageResourceName = "TimeSpentRequired")]
        [Range(
            0.01,
            12.00,
            ErrorMessageResourceType = typeof(Properties.LocalResource.TaskBoard.Index),
            ErrorMessageResourceName = "TimeSpentRange")]
        public double TimeSpent { get; set; }

        [Required(
            ErrorMessageResourceType = typeof(Properties.LocalResource.TaskBoard.Index),
            ErrorMessageResourceName = "DescriptionRequired")]
        public string Description { get; set; }
    }
}