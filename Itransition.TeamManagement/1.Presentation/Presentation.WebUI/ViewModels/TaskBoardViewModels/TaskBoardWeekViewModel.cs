﻿using System.Collections.Generic;
using System.Linq;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.TaskBoardViewModels
{
    public class TaskBoardWeekViewModel
    {
        public List<TaskWithLogsTimeWeekViewModel> TaskWithLogsTime { get; set; }

        public List<double> SumHoursPerDay { get; set; }

        public double SumHoursPerWeek
        {
            get
            {
                return SumHoursPerDay.Sum();
            }
        }
    }
}