﻿using Itransition.TeamManagement.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.TaskBoardViewModels
{
    public class TaskWithLogsTimeMonthViewModel
    {
        public WorkTaskViewModel WorkTask { get; set; }

        public List<double> TaskHoursPerMonth { get; set; }

        public double SumTaskHoursPerMonth
        {
            get
            {
                return TaskHoursPerMonth.Sum();
            }
        }
    }
}