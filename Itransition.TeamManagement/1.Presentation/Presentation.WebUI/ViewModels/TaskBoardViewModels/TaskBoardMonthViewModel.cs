﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.TaskBoardViewModels
{
    public class TaskBoardMonthViewModel
    {
        public List<TaskWithLogsTimeMonthViewModel> TaskWithLogsTime { get; set; }

        public List<double> SumHoursPerWeek { get; set; }

        public double SumHoursPerMonth
        {
            get
            {
                return SumHoursPerWeek.Sum();
            }
        }
    }
}