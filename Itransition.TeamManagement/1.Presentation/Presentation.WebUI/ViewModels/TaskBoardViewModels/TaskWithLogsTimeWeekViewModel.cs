﻿using Itransition.TeamManagement.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.TaskBoardViewModels
{
    public class TaskWithLogsTimeWeekViewModel
    {
        public WorkTaskViewModel WorkTask { get; set; }

        public List<double> TaskHoursPerWeek { get; set; }

        public double SumTaskHoursPerWeek
        {
            get
            {
                return TaskHoursPerWeek.Sum();
            }
        }
    }
}