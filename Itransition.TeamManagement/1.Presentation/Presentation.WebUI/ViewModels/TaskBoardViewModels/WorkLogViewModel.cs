﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.TaskBoardViewModels
{
    public class WorkLogViewModel
    {
        public int Id { get; set; }

        public double TimeSpent { get; set; }

        public string Description { get; set; }

        public DateTime CreateDate { get; set; }
    }
}