﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.TaskBoardViewModels
{
    public class WorkTaskViewModel
    {
        public int WorkTaskId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}