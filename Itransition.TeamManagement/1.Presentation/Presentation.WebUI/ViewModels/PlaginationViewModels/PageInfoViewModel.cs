﻿using System;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.PlaginationViewModels
{
    public class PageInfoViewModel
    {
        public int CurrentPage { get; set; }

        public int PageSize { get; set; }

        public int TotalItems { get; set; }

        public int TotalPages
        {
            get { return (int)Math.Ceiling((decimal)this.TotalItems / this.PageSize); }
        }
    }
}