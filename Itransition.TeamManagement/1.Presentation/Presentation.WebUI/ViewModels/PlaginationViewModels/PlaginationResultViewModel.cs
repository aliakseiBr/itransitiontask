﻿namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.PlaginationViewModels
{
    public class PlaginationResultViewModel
    {
        public string PageUrl { get; set; }

        public string PageName { get; set; }

        public HtmlLinkTypeEnum? TypeLink { get; set; }
    }
}