﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.PlaginationViewModels
{
    public enum HtmlLinkTypeEnum
    {
        FirstSkipLink,
        LastSkipLink,
        SelectedLink
    }
}