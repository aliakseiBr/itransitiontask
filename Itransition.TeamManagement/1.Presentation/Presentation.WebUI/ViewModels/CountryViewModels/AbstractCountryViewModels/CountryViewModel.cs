﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.CountryViewModels.AbstractCountryViewModels
{
    public abstract class CountryViewModel
    {
        public virtual int? CountryId { get; set; }

        [Required(
            ErrorMessageResourceType = typeof(Properties.LocalResource.Country.CreateCountry),
            ErrorMessageResourceName = "CountryNameRequired")]
        [StringLength(
            25,
            MinimumLength = 2,
            ErrorMessageResourceType = typeof(Properties.LocalResource.Country.CreateCountry),
            ErrorMessageResourceName = "CountryNameStringLength")]
        public virtual string Name { get; set; }
    }
}