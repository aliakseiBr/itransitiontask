﻿using Itransition.TeamManagement.Presentation.WebUI.ViewModels.PlaginationViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.CountryViewModels
{
    public class CountryPlaginationViewModel
    {
        public List<CountryMainViewModel> Countries { get; set; }

        public PageInfoViewModel PageInfo { get; set; }
    }
}