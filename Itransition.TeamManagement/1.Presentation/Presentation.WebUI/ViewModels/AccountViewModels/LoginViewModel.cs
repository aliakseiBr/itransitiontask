﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.AccountViewModels
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email", ResourceType = typeof(Properties.LocalResource.Account.Login))]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password", ResourceType = typeof(Properties.LocalResource.Account.Login))]
        public string Password { get; set; }

        [Display(Name = "RememberMe", ResourceType = typeof(Properties.LocalResource.Account.Login))]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}