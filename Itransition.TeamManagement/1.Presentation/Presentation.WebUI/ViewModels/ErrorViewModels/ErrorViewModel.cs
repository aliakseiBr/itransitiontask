﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.ErrorViewModels
{
    public class ErrorViewModel
    {
        public string Message { get; set; }
    }
}