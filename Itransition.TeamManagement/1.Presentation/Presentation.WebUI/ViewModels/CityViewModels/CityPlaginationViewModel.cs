﻿using Itransition.TeamManagement.Presentation.WebUI.ViewModels.PlaginationViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.CityViewModels
{
    public class CityPlaginationViewModel
    {
        public int CountryId { get; set; }

        public string CountryName { get; set; }

        public List<CityMainViewModel> Cities { get; set; }

        public PageInfoViewModel PageInfo { get; set; }
    }
}