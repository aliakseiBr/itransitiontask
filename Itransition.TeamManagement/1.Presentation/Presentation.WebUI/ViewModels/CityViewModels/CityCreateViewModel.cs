﻿using Itransition.TeamManagement.Presentation.WebUI.ViewModels.CityViewModels.AbstractCityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.CityViewModels
{
    public class CityCreateViewModel : CityViewModel
    {
        public List<SelectListItem> Contries { get; set; }
    }
}