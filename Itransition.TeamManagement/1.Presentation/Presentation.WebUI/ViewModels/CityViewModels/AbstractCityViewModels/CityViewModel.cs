﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.CityViewModels.AbstractCityModels
{
    public abstract class CityViewModel
    {
        public virtual int? CityId { get; set; }

        [Required(
            ErrorMessageResourceType = typeof(Properties.LocalResource.City.CreateCity),
            ErrorMessageResourceName = "CountryRequired")]
        public virtual int? CountryId { get; set; }

        [Required(
            ErrorMessageResourceType = typeof(Properties.LocalResource.City.CreateCity),
            ErrorMessageResourceName = "CityNameRequired")]
        [StringLength(
            25,
            MinimumLength = 2,
            ErrorMessageResourceType = typeof(Properties.LocalResource.City.CreateCity),
            ErrorMessageResourceName = "CityNameStringLength")]
        public virtual string Name { get; set; }
    }
}