﻿using Itransition.TeamManagement.Presentation.WebUI.ViewModels.CityViewModels.AbstractCityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.ViewModels.CityViewModels
{
    public class CityMainViewModel : CityViewModel
    {
        public string CountryName { get; set; }
    }
}