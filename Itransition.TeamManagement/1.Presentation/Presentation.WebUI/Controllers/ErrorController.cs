﻿using Itransition.TeamManagement.Presentation.WebUI.ViewModels.ErrorViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Itransition.TeamManagement.Presentation.WebUI.Controllers
{
    public partial class ErrorController : Controller
    {
        [HttpGet]
        public virtual ActionResult Error(string errorMessage = null)
        {
            return View(new ErrorViewModel() { Message = errorMessage });
        }
    }
}