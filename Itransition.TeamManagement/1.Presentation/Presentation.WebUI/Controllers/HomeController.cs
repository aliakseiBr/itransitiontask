﻿using Itransition.TeamManagement.Presentation.WebUI.Services.Shared;
using System.Web.Mvc;

namespace Itransition.TeamManagement.Presentation.WebUI.Controllers
{
    [CustomHandleError]
    public partial class HomeController : Controller
    {
        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult About()
        {
            return View();
        }

        public virtual ActionResult Contact()
        {
            return View();
        }
    }
}