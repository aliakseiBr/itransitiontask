﻿using System.Web.Mvc;
using Itransition.TeamManagement.Presentation.WebUI.Services.CityServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.Shared;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.CityViewModels;

namespace Itransition.TeamManagement.Presentation.WebUI.Controllers
{
    [Authorize]
    [CustomHandleError]
    public partial class CityController : Controller
    {
        private IPresentationCityService presentationCityService;

        public CityController(IPresentationCityService presentationCityService)
        {
            this.presentationCityService = presentationCityService;
        }

        [HttpGet]
        public virtual ActionResult GetCities(int page = 1)
        {
            CityPlaginationViewModel model = presentationCityService.GetCityPlaginationViewModel(page, Constants.PageSize);

            return View(model);
        }

        [HttpGet]
        public virtual ActionResult CreateCity()
        {
            CityCreateViewModel cityCreateViewModel = presentationCityService.GetCityCreateViewModel();

            return View(cityCreateViewModel);
        }

        [HttpPost]
        public virtual ActionResult CreateCity(CityCreateViewModel cityCreateViewModel)
        {
            if (ModelState.IsValid)
            {
                ResultModel result = presentationCityService.CreateCity(cityCreateViewModel);
                if (result.IsValid)
                {
                    return RedirectToAction(MVC.City.GetCities());
                }
                else
                {
                    ModelState.AddModelError(nameof(cityCreateViewModel.Name), result.ErrorMessage);
                }
            }

            cityCreateViewModel = presentationCityService.UpdateCityCreateViewModel(cityCreateViewModel);

            return View(cityCreateViewModel);
        }

        [HttpGet]
        public virtual ActionResult UpdateCity(int? cityId, int? countryId)
        {
            CityUpdateViewModel cityUpdateViewModel = presentationCityService.GetCityUpdateViewModel(cityId, countryId);

            return View(cityUpdateViewModel);
        }

        [HttpPost]
        public virtual ActionResult UpdateCity(CityUpdateViewModel cityUpdateViewModel)
        {
            if (ModelState.IsValid)
            {
                ResultModel result = presentationCityService.UpdateCity(cityUpdateViewModel);
                if (result.IsValid)
                {
                    return RedirectToAction(MVC.City.GetCities());
                }
                else
                {
                    ModelState.AddModelError(nameof(cityUpdateViewModel.Name), result.ErrorMessage);
                }
            }

            return View(cityUpdateViewModel);
        }

        [HttpPost]
        public virtual ActionResult DeleteCity(int? cityId, int? countryId, int page = 1)
        {
            ResultModel result = presentationCityService.DeleteCity(cityId, countryId);
            if (result.IsValid)
            {
                return RedirectToAction(MVC.City.GetCities(page));
            }

            return RedirectToAction(MVC.Error.Error(result.ErrorMessage));
        }
    }
}