﻿using System.Collections.Generic;
using System.Web.Mvc;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.Shared;
using Itransition.TeamManagement.Presentation.WebUI.Services.UserServices;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.UserViewModels;

namespace Itransition.TeamManagement.Presentation.WebUI.Controllers
{
    [Authorize]
    [CustomHandleError]
    public partial class UserController : Controller
    {
        private IPresentationUserService presentationUserServise;
        private IPresentationAuthenticationService presentationAuthenticationService;

        public UserController(IPresentationUserService presentationUserServise, IPresentationAuthenticationService presentationAuthenticationService)
        {
            this.presentationUserServise = presentationUserServise;
            this.presentationAuthenticationService = presentationAuthenticationService;
        }

        [HttpGet]
        [ValidateInput(false)]
        public virtual ActionResult Users(int page = 1, string search = null)
        {
            UserPlaginationViewModel model = presentationUserServise.GetUserPlaginationViewModel(page, Constants.PageSize, search, Request.Url.PathAndQuery);

            return View(model);
        }

        [HttpGet]
        public virtual PartialViewResult PopUpComment(int? userId)
        {
            UserCommentPopUpViewModel userCommentPopUpViewModel = presentationUserServise.GetCommentPopUpViewModel(userId);

            return PartialView(MVC.Shared.Views._PopUp, userCommentPopUpViewModel);
        }

        [HttpGet]
        public virtual ActionResult CreateUser()
        {
            UserCreateViewModel userCreateViewModel = presentationUserServise.GetUserCreateViewModel();

            return View(userCreateViewModel);
        }

        [HttpPost]
        public virtual ActionResult CreateUser(UserCreateViewModel userCreateModel)
        {
            if (ModelState.IsValid)
            {
                ResultModel result = presentationUserServise.CreateUser(userCreateModel);
                if (result.IsValid)
                {
                    return RedirectToAction(MVC.User.Users());
                }
                else
                {
                    ModelState.AddModelError(nameof(userCreateModel.Email), result.ErrorMessage);
                }
            }

            presentationUserServise.UpdateUserViewModel(userCreateModel);

            return View(userCreateModel);
        }

        [HttpGet]
        public virtual ActionResult GetCities(int? countryId)
        {
            List<City> cities = presentationUserServise.GetCities(countryId);

            return PartialView(MVC.Shared.Views._GetCities, cities);
        }

        [HttpGet]
        public virtual ActionResult UpdateUser(int? userId)
        {
            ParentUserUpdateViewModel parentEditorModel = presentationUserServise.GetParentUserUpdateViewModel(userId.Value);

            return View(parentEditorModel);
        }

        [HttpPost]
        public virtual ActionResult UpdateUser(ParentUserUpdateViewModel parentUserUpdateModel)
        {
            if (ModelState.IsValid)
            {
                ResultModel result = presentationUserServise.UpdateUser(parentUserUpdateModel.UserUpdateViewModel);
                if (result.IsValid)
                {
                    return RedirectToAction(MVC.User.Users());
                }
                else
                {
                    ModelState.AddModelError(nameof(parentUserUpdateModel.UserUpdateViewModel.Email), result.ErrorMessage);
                }
            }

            presentationUserServise.UpdateParentUserUpdateViewModel(parentUserUpdateModel);

            return View(parentUserUpdateModel);
        }

        [HttpPost]
        public virtual ActionResult DeleteUser(int? userId, int page = 0)
        {
            ResultModel result = presentationUserServise.DeleteUser(userId.Value);
            int currentUserId = presentationAuthenticationService.GetCurrentUserId();

            if (result.IsValid)
            {
                if (userId.Value == currentUserId)
                {
                    return RedirectToAction(MVC.Account.LogOff());
                }
                else
                {
                    return RedirectToRoute(MVC.User.Users(page));
                }
            }
            else
            {
                return RedirectToRoute(MVC.Error.Error(result.ErrorMessage));
            }
        }

        [HttpPost]
        public virtual ActionResult ChangeOldPassword(ParentUserUpdateViewModel parentUserUpdateViewModel)
        {
            var model = parentUserUpdateViewModel.UserUpdateOldPasswordViewModel;

            if (ModelState.IsValid)
            {
                ResultModel result = presentationUserServise.ChangeOldPassword(parentUserUpdateViewModel.UserUpdateOldPasswordViewModel);
                if (result.IsValid)
                {
                    return RedirectToAction(MVC.User.Users());
                }
                else
                {
                    ModelState.AddModelError(nameof(parentUserUpdateViewModel.UserUpdateOldPasswordViewModel.OldPassword), result.ErrorMessage);
                }
            }

            presentationUserServise.UpdateParentUserUpdateViewModel(parentUserUpdateViewModel);

            return View(MVC.User.Views.UpdateUser, parentUserUpdateViewModel);
        }

        [HttpPost]
        public virtual ActionResult AddComment(int? userId, string comment)
        {
            presentationUserServise.AddUserComment(userId, comment);
            UserCommentPopUpViewModel userCommentPopUpViewModel = presentationUserServise.GetCommentPopUpViewModel(userId);

            return PartialView(MVC.Shared.Views._PopUp, userCommentPopUpViewModel);
        }
    }
}