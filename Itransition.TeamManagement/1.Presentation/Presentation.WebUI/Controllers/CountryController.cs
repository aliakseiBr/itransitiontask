﻿using System.Web.Mvc;
using Itransition.TeamManagement.Presentation.WebUI.Services.CountryServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.Shared;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.CountryViewModels;

namespace Itransition.TeamManagement.Presentation.WebUI.Controllers
{
    [Authorize]
    [CustomHandleError]
    public partial class CountryController : Controller
    {
        private IPresentationCountryService presentationCountryService;

        public CountryController(IPresentationCountryService presentationCountryService)
        {
            this.presentationCountryService = presentationCountryService;
        }

        [HttpGet]
        public virtual ActionResult GetCountries(int page = 1)
        {
            CountryPlaginationViewModel countryPlaginationViewModel = presentationCountryService.GetCountryPlaginationViewModel(page, Constants.PageSize);

            return View(countryPlaginationViewModel);
        }

        [HttpGet]
        public virtual ActionResult CreateCountry()
        {
            CountryCreateViewModel countryCreateViewModel = presentationCountryService.GetCountryCreateViewModel();

            return View(countryCreateViewModel);
        }

        [HttpPost]
        public virtual ActionResult CreateCountry(CountryCreateViewModel countryCreateViewModel)
        {
            if (ModelState.IsValid)
            {
                ResultModel result = presentationCountryService.CreateCountry(countryCreateViewModel);
                if (result.IsValid)
                {
                    return RedirectToAction(MVC.Country.GetCountries());
                }
                else
                {
                    ModelState.AddModelError(nameof(countryCreateViewModel.Name), result.ErrorMessage);
                }
            }

            return View(countryCreateViewModel);
        }

        [HttpGet]
        public virtual ActionResult UpdateCountry(int? countryId)
        {
            CountryUpdateViewModel model = presentationCountryService.GetCountryUpdateViewModel(countryId);

            return View(model);
        }

        [HttpPost]
        public virtual ActionResult UpdateCountry(CountryUpdateViewModel countryUpdateViewModel)
        {
            if (ModelState.IsValid)
            {
                ResultModel result = presentationCountryService.UpdateCountry(countryUpdateViewModel);
                if (result.IsValid)
                {
                    return RedirectToAction(MVC.Country.GetCountries());
                }
                else
                {
                    ModelState.AddModelError(nameof(countryUpdateViewModel.Name), result.ErrorMessage);
                }
            }

            return View(countryUpdateViewModel);
        }

        [HttpPost]
        public virtual ActionResult DeleteCountry(int? countryId, int page = 1)
        {
            ResultModel result = presentationCountryService.DeleteCountry(countryId);
            if (result.IsValid)
            {
                return RedirectToAction(MVC.Country.GetCountries());
            }

            return RedirectToAction(MVC.Error.Error(result.ErrorMessage));
        }
    }
}