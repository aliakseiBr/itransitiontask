﻿using Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.Shared;
using Itransition.TeamManagement.Presentation.WebUI.Services.TaskBoardServices;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.TaskBoardViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Itransition.TeamManagement.Presentation.WebUI.Controllers
{
    [Authorize]
    [CustomHandleError]
    public partial class TaskBoardController : Controller
    {
        private IPresentationWorkTaskService presentationTaskService;

        public TaskBoardController(IPresentationWorkTaskService presentationTaskService)
        {
            this.presentationTaskService = presentationTaskService;
        }

        [HttpGet]
        public virtual ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public virtual ActionResult WeekTable(WorkLogsPeriod logsPeriod = null)
        {
            TaskBoardWeekViewModel taskBoardViewModel = presentationTaskService.GetTaskBoardViewModel(logsPeriod);

            return Json(taskBoardViewModel);
        }

        [HttpPost]
        public virtual ActionResult MonthTable(WorkLogsPeriod logsPeriod = null)
        {
            TaskBoardMonthViewModel taskBoardMonthViewModel = presentationTaskService.GetTaskBoardMonthViewModel(logsPeriod);

            return Json(taskBoardMonthViewModel);
        }

        [HttpPost]
        public virtual ActionResult LogsForDay(int? taskId, WorkLogsPeriod logsPeriod = null)
        {
            List<WorkLogViewModel> workLogs = presentationTaskService.GetWorkLogsByTask(taskId.Value, logsPeriod);

            return Json(workLogs);
        }

        [HttpPost]
        public virtual ActionResult DeleteWorkLog(int? logId = null)
        {
            ResultListModel resultModel = presentationTaskService.DeleteLogWork(logId.Value);

            return Json(resultModel);
        }

        [HttpPost]
        public virtual ActionResult GetOpenWorkTaskWithoutLogs(WorkLogsPeriod logsPeriod = null)
        {
            List<WorkTaskViewModel> workTasks = presentationTaskService.GetActiveWorkTasksWithoutLogs(logsPeriod);

            return Json(workTasks);
        }

        [HttpPost]
        public virtual ActionResult CreateLogWork(CreateLogWorkViewModel createLogWorkViewModel = null)
        {
            ResultListModel resultModel = null;

            if (!ModelState.IsValid)
            {
                var errorList = ModelState.Values.SelectMany(m => m.Errors)
                                  .Select(e => e.ErrorMessage)
                                  .ToList();
                resultModel = new ResultListModel(result: false, errorMessages: errorList);
            }
            else
            {
                resultModel = presentationTaskService.CreateLogWork(createLogWorkViewModel);
            }

            return Json(resultModel);
        }
    }
}