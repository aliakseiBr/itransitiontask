﻿using Itransition.TeamManagement.Presentation.WebUI.Services.AccountServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.Shared;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.AccountViewModels;
using System.Web.Mvc;

namespace Itransition.TeamManagement.Presentation.WebUI.Controllers
{
    [CustomHandleError]
    public partial class AccountController : Controller
    {
        private IPresentationAccountService presentationAccountService;

        public AccountController(IPresentationAccountService presentationAccountService)
        {
            this.presentationAccountService = presentationAccountService;
        }

        [HttpGet]
        public virtual ActionResult Login(string returnUrl)
        {
            return View(new LoginViewModel() { ReturnUrl = returnUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Login(LoginViewModel loginViewModel, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(loginViewModel);
            }

            ResultModel result = presentationAccountService.Authentication(loginViewModel);
            if (result.IsValid)
            {
                return returnUrl != null ? Redirect(returnUrl) : (ActionResult)RedirectToAction(MVC.Home.Index());
            }
            else
            {
                ModelState.AddModelError(string.Empty, result.ErrorMessage);

                return View(loginViewModel);
            }
        }

        [HttpGet]
        [Authorize]
        public virtual ActionResult LogOff()
        {
            presentationAccountService.LogOff();

            return RedirectToAction(MVC.Home.Index());
        }
    }
}