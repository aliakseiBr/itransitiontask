﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.CountryViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.UIMapper
{
    public interface IPresentationCountryMapper
    {
        Country ConvertToCountryDomain(CountryCreateViewModel countryCreateModel);

        Country ConvertToCountryDomain(Country country, CountryUpdateViewModel countryUpdateModel);

        CountryMainViewModel ConvertToCountryMainModel(Country country);

        CountryUpdateViewModel ConvertToCountryUpdateModel(Country country);
    }
}