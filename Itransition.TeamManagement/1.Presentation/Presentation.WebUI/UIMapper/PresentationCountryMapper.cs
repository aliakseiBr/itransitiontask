﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IServices;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.CountryViewModels;
using System;

namespace Itransition.TeamManagement.Presentation.WebUI.UIMapper
{
    public class PresentationCountryMapper : IPresentationCountryMapper
    {
        private ICountryDomainService countryDomainService;
        private IUserDomainService userDomainService;
        private IPresentationAuthenticationService presentationAuthenticationService;

        public PresentationCountryMapper(
            ICountryDomainService countryDomainService,
            IUserDomainService userDomainService,
            IPresentationAuthenticationService presentationAuthenticationService)
        {
            this.countryDomainService = countryDomainService;
            this.userDomainService = userDomainService;
            this.presentationAuthenticationService = presentationAuthenticationService;
        }

        public Country ConvertToCountryDomain(CountryCreateViewModel countryCreateModel)
        {
            int companyId = presentationAuthenticationService.GetCurrentCompanyId();

            Country country = new Country()
            {
                CountryName = countryCreateModel.Name,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                CompanyId = companyId
            };

            return country;
        }

        public CountryUpdateViewModel ConvertToCountryUpdateModel(Country country)
        {
            return new CountryUpdateViewModel()
            {
                CountryId = country.Id,
                Name = country.CountryName
            };
        }

        public Country ConvertToCountryDomain(Country country, CountryUpdateViewModel countryUpdateModel)
        {
            country.CountryName = countryUpdateModel.Name;
            country.UpdateDate = DateTime.Now;

            return country;
        }

        public CountryMainViewModel ConvertToCountryMainModel(Country country)
        {
            return new CountryMainViewModel()
            {
                CountryId = country.Id,
                Name = country.CountryName,
                CitiesCount = countryDomainService.CountCitiesOfCountry(country)
            };
        }
    }
}