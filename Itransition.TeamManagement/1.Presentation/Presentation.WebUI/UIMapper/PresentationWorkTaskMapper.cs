﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.TaskBoardViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Itransition.TeamManagement.Presentation.WebUI.UIMapper
{
    public class PresentationWorkTaskMapper : IPresentationWorkTaskMapper
    {
        public WorkLog ConvertToWorkLogDomain(CreateLogWorkViewModel createLogWorkViewModel)
        {
            WorkLog workLog = new WorkLog()
            {
                CreateDate = DateTime.Now,
                WorkTaskId = createLogWorkViewModel.TaskId,
                LogDay = createLogWorkViewModel.LogDay.Value,
                Description = createLogWorkViewModel.Description,
                Time = createLogWorkViewModel.TimeSpent
            };

            return workLog;
        }

        public TaskBoardMonthViewModel ConvertLogsToTaskBoardMonthViewModel(
            IEnumerable<WorkLog> logsWithTask,
            WorkLogsPeriod logsPeriod)
        {
            IEnumerable<IGrouping<WorkTask, WorkLog>> groupLogs = logsWithTask.GroupBy(x => x.WorkTask);

            List<TaskWithLogsTimeMonthViewModel> taskWithLogsTimes = new List<TaskWithLogsTimeMonthViewModel>();

            foreach (var item in groupLogs)
            {
                List<double> workLogsOfWeek = GetSumHoursPerWeek(item.AsEnumerable(), logsPeriod);
                var task = new WorkTaskViewModel() { Name = item.Key.Name, WorkTaskId = item.Key.Id, Description = item.Key.Description };
                taskWithLogsTimes.Add(new TaskWithLogsTimeMonthViewModel() { TaskHoursPerMonth = workLogsOfWeek, WorkTask = task });
            }

            return new TaskBoardMonthViewModel() { TaskWithLogsTime = taskWithLogsTimes, SumHoursPerWeek = GetSumHoursPerWeek(logsWithTask, logsPeriod) };
        }

        public TaskBoardWeekViewModel ConvertLogsToTaskBoardViewModel(
            IEnumerable<WorkLog> logsWithTask,
            WorkLogsPeriod logsPeriod)
        {
            List<IGrouping<WorkTask, WorkLog>> groupLogs = logsWithTask.GroupBy(x => x.WorkTask).ToList();

            List<TaskWithLogsTimeWeekViewModel> taskWithLogsTimes = new List<TaskWithLogsTimeWeekViewModel>();

            foreach (var item in groupLogs)
            {
                List<double> workLogsOfWeek = GetSumHoursPerDay(item.AsEnumerable(), logsPeriod);
                var task = new WorkTaskViewModel() { Name = item.Key.Name, WorkTaskId = item.Key.Id, Description = item.Key.Description };
                taskWithLogsTimes.Add(new TaskWithLogsTimeWeekViewModel() { TaskHoursPerWeek = workLogsOfWeek, WorkTask = task });
            }

            return new TaskBoardWeekViewModel() { TaskWithLogsTime = taskWithLogsTimes, SumHoursPerDay = GetSumHoursPerDay(logsWithTask, logsPeriod) };
        }

        public List<WorkLogViewModel> ConvertLogsToWorkLogViewModel(IEnumerable<WorkLog> logsWithTask)
        {
            List<WorkLogViewModel> listWorkLogs = new List<WorkLogViewModel>();

            foreach (var item in logsWithTask)
            {
                var model = new WorkLogViewModel
                {
                    Id = item.Id,
                    CreateDate = item.LogDay,
                    Description = item.Description,
                    TimeSpent = item.Time
                };

                listWorkLogs.Add(model);
            }

            return listWorkLogs;
        }

        public List<WorkTaskViewModel> ConvertWorkTaskToWorkTaskViewModel(IEnumerable<WorkTask> tasks)
        {
            List<WorkTaskViewModel> workTaskViewModels = new List<WorkTaskViewModel>();

            foreach (var item in tasks)
            {
                var model = new WorkTaskViewModel()
                {
                    Name = item.Name,
                    Description = item.Description,
                    WorkTaskId = item.Id
                };

                workTaskViewModels.Add(model);
            }

            return workTaskViewModels;
        }

        private List<double> GetSumHoursPerDay(IEnumerable<WorkLog> logs, WorkLogsPeriod logsPeriod)
        {
            List<double> result = new List<double>();
            DateTime startDay = logsPeriod.StartPeriod;
            double hours;

            while (startDay <= logsPeriod.EndPeriod)
            {
                hours = 0;

                foreach (var item in logs)
                {
                    if (item.LogDay == startDay)
                    {
                        hours += item.Time;
                    }
                }

                result.Add(hours);
                startDay += new TimeSpan(24, 00, 00);
            }

            return result;
        }

        private List<double> GetSumHoursPerWeek(IEnumerable<WorkLog> logs, WorkLogsPeriod logsPeriod)
        {
            List<double> result = new List<double>();
            DateTime startWeek = logsPeriod.StartPeriod;
            DateTime endWeek = GetEndFirstWeek(logsPeriod.StartPeriod, logsPeriod.EndPeriod);
            double hours;

            while (startWeek <= logsPeriod.EndPeriod)
            {
                hours = logs
                .Where(x => x.LogDay >= startWeek && x.LogDay <= endWeek)
                .Select(x => x.Time)
                .DefaultIfEmpty(0)
                .Sum(x => x);

                result.Add(hours);
                startWeek = endWeek + new TimeSpan(24, 00, 00);
                endWeek += new TimeSpan(168, 00, 00);

                if (endWeek > logsPeriod.EndPeriod)
                {
                    endWeek = logsPeriod.EndPeriod;
                }
            }

            return result;
        }

        private DateTime GetEndFirstWeek(DateTime startPeriod, DateTime endPeriod)
        {
            DateTime endWeek = startPeriod;

            if (startPeriod.DayOfWeek == DayOfWeek.Monday)
            {
                endWeek += new TimeSpan(144, 00, 00);
            }
            else
            {
                while (endWeek.DayOfWeek != DayOfWeek.Sunday)
                {
                    endWeek += new TimeSpan(24, 00, 00);
                }
            }

            if (endWeek > endPeriod)
            {
                endWeek = endPeriod;
            }

            return endWeek;
        }
    }
}