﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.CityViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Itransition.TeamManagement.Presentation.WebUI.UIMapper
{
    public interface IPresentationCityMapper
    {
        City ConvertToCityDomain(CityCreateViewModel cityCreateModel);

        City ConvertToCityDomain(City city, CityUpdateViewModel cityUpdateModel);

        CityUpdateViewModel ConvertToCityUpdateModel(City city);

        CityMainViewModel ConvertToCityMainModel(City city);
    }
}