﻿using Itransition.TeamManagement.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Itransition.TeamManagement.Presentation.WebUI.UIMapper
{
    public interface IPresentationSelectListItemMapper
    {
        List<SelectListItem> ConvertToSelectListItem(IEnumerable<Title> titles);

        List<SelectListItem> ConvertToSelectListItem(IEnumerable<Role> roles);

        List<SelectListItem> ConvertToSelectListItem(IEnumerable<Country> countries);

        List<SelectListItem> ConvertToSelectListItem(IEnumerable<City> cities);
    }
}