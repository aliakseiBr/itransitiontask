﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IServices;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.CityViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Itransition.TeamManagement.Presentation.WebUI.UIMapper
{
    public class PresentationCityMapper : IPresentationCityMapper
    {
        private ICountryDomainService countryManager;
        private ICityDomainService cityDomainService;

        public PresentationCityMapper(IUserDomainService userManager, ICountryDomainService countryManager, ICityDomainService cityDomainService)
        {
            this.countryManager = countryManager;
            this.cityDomainService = cityDomainService;
        }

        public CityMainViewModel ConvertToCityMainModel(City city)
        {
            return new CityMainViewModel()
            {
                CityId = city.Id,
                CountryId = city.CountryId,
                CountryName = city.Country.CountryName,
                Name = city.CityName
            };
        }

        public City ConvertToCityDomain(City city, CityUpdateViewModel cityUpdateModel)
        {
            city.CityName = cityUpdateModel.Name;
            city.UpdateDate = DateTime.Now;

            return city;
        }

        public City ConvertToCityDomain(CityCreateViewModel cityCreateModel)
        {
            City country = new City()
            {
                CountryId = cityCreateModel.CountryId,
                CityName = cityCreateModel.Name,
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now
            };

            return country;
        }

        public CityUpdateViewModel ConvertToCityUpdateModel(City city)
        {
            return new CityUpdateViewModel()
            {
                CityId = city.Id,
                CountryId = city.CountryId,
                Name = city.CityName
            };
        }
    }
}