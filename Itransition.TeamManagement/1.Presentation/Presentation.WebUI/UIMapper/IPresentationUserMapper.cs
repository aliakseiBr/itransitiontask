﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Itransition.TeamManagement.Presentation.WebUI.UIMapper
{
    public interface IPresentationUserMapper
    {
        User ConvertToUserDomain(UserCreateViewModel userManagerModel);

        UserUpdateViewModel ConvertToUserEditorModel(User userDomainModel);

        UserMainViewModel ConvertToUserViewModel(User userDomainModel);

        User ConvertToUserDomain(User userDomainModel, UserUpdateViewModel userManagerModel);
    }
}