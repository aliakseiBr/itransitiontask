﻿using Itransition.TeamManagement.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Itransition.TeamManagement.Presentation.WebUI.UIMapper
{
    public class PresentationSelectListItemMapper : IPresentationSelectListItemMapper
    {
        public List<SelectListItem> ConvertToSelectListItem(IEnumerable<Title> titles)
        {
            List<SelectListItem> list = titles.Select(x => new SelectListItem()
            {
                Text = x.TitleName,
                Value = x.Id.ToString()
            })
                .ToList();

            return list;
        }

        public List<SelectListItem> ConvertToSelectListItem(IEnumerable<Role> roles)
        {
            List<SelectListItem> list = roles.Select(x => new SelectListItem()
            {
                Text = x.RoleName,
                Value = x.Id.ToString()
            })
            .ToList();

            return list;
        }

        public List<SelectListItem> ConvertToSelectListItem(IEnumerable<Country> countries)
        {
            var list = countries.Select(x => new SelectListItem()
            {
                Text = x.CountryName,
                Value = x.Id.ToString()
            })
                .ToList();

            return list;
        }

        public List<SelectListItem> ConvertToSelectListItem(IEnumerable<City> cities)
        {
            if (cities == null)
            {
                return new List<SelectListItem>();
            }

            List<SelectListItem> list = cities.Select(x => new SelectListItem()
            {
                Text = x.CityName,
                Value = x.Id.ToString()
            })
                .ToList();

            return list;
        }
    }
}