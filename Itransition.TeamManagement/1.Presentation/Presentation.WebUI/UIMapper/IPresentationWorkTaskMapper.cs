﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.TaskBoardViewModels;
using System.Collections.Generic;

namespace Itransition.TeamManagement.Presentation.WebUI.UIMapper
{
    public interface IPresentationWorkTaskMapper
    {
        TaskBoardWeekViewModel ConvertLogsToTaskBoardViewModel(
            IEnumerable<WorkLog> logsWithTask,
            WorkLogsPeriod logsPeriod);

        TaskBoardMonthViewModel ConvertLogsToTaskBoardMonthViewModel(
            IEnumerable<WorkLog> logsWithTask,
            WorkLogsPeriod logsPeriod);

        List<WorkLogViewModel> ConvertLogsToWorkLogViewModel(IEnumerable<WorkLog> logsWithTask);

        List<WorkTaskViewModel> ConvertWorkTaskToWorkTaskViewModel(IEnumerable<WorkTask> logsWithTask);

        WorkLog ConvertToWorkLogDomain(CreateLogWorkViewModel createLogWorkViewModel);
    }
}
