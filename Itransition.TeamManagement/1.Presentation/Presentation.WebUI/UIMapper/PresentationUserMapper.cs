﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Security;
using Itransition.TeamManagement.Domain.Services.IServices;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Itransition.TeamManagement.Presentation.WebUI.UIMapper
{
    public class PresentationUserMapper : IPresentationUserMapper
    {
        private IUserDomainService userDomainService;
        private ICityDomainService cityDomainService;
        private IRoleDomainService roleDomainService;
        private IPresentationAuthenticationService presentationAuthenticationService;

        public PresentationUserMapper(
            IUserDomainService userDomainService,
            ICityDomainService cityDomainService,
            IRoleDomainService roleDomainService,
            IPresentationAuthenticationService presentationAuthenticationService)
        {
            this.userDomainService = userDomainService;
            this.cityDomainService = cityDomainService;
            this.roleDomainService = roleDomainService;
            this.presentationAuthenticationService = presentationAuthenticationService;
        }

        public User ConvertToUserDomain(UserCreateViewModel userCreateViewModel)
        {
            int currentCompanyId = presentationAuthenticationService.GetCurrentCompanyId();

            User userDomainModel = new User()
            {
                City = cityDomainService.Get(userCreateViewModel.CityId.Value),
                Email = userCreateViewModel.Email,
                FirstName = userCreateViewModel.FirstName,
                LastName = userCreateViewModel.LastName,
                Phone = userCreateViewModel.Phone,
                Title = userDomainService.GetTitle(userCreateViewModel.TitleId.Value),
                Roles = new List<Role>() { roleDomainService.Get(userCreateViewModel.RoleId) },
                CompanyId = currentCompanyId,
                UpdateDate = DateTime.Now,
                CreateDate = DateTime.Now
            };

            return userDomainModel;
        }

        public UserUpdateViewModel ConvertToUserEditorModel(User userDomainModel)
        {
            UserUpdateViewModel userManagerModel = new UserUpdateViewModel()
            {
                UserId = userDomainModel.Id,
                FirstName = userDomainModel.FirstName,
                LastName = userDomainModel.LastName,
                Phone = userDomainModel.Phone,
                Email = userDomainModel.Email,
                TitleId = userDomainModel.Title.Id,
                RoleId = userDomainModel.Roles.Select(x => x.Id).FirstOrDefault(),
                CountryId = userDomainModel.City.Country.Id,
                CityId = userDomainModel.City.Id,
            };

            return userManagerModel;
        }

        public UserMainViewModel ConvertToUserViewModel(User userDomainModelWithRoles)
        {
            if (userDomainModelWithRoles.Roles == null)
            {
                throw new ArgumentNullException(nameof(userDomainModelWithRoles));
            }

            UserMainViewModel userViewModel = new UserMainViewModel()
            {
                UserId = userDomainModelWithRoles.Id,
                FirstName = userDomainModelWithRoles.FirstName,
                LastName = userDomainModelWithRoles.LastName,
                Phone = userDomainModelWithRoles.Phone,
                Email = userDomainModelWithRoles.Email,
                Title = userDomainModelWithRoles.Title.TitleName,
                CountryName = userDomainModelWithRoles.City.Country.CountryName,
                CityName = userDomainModelWithRoles.City.CityName,
                AccessToChange = GetAccessToChange(userDomainModelWithRoles)
            };

            return userViewModel;
        }

        public User ConvertToUserDomain(User userDomainModel, UserUpdateViewModel userManagerModel)
        {
            userDomainModel = userDomainService.Get(userManagerModel.UserId.Value);
            userDomainModel.City = cityDomainService.Get(userManagerModel.CityId.Value);
            userDomainModel.Email = userManagerModel.Email;
            userDomainModel.FirstName = userManagerModel.FirstName;
            userDomainModel.LastName = userManagerModel.LastName;
            userDomainModel.Phone = userManagerModel.Phone;
            userDomainModel.Title = userDomainService.GetTitle(userManagerModel.TitleId.Value);
            userDomainModel.UpdateDate = DateTime.Now;

            return userDomainModel;
        }

        private bool GetAccessToChange(User user)
        {
            if (presentationAuthenticationService.IsInRole(DomainSecurityConstants.User) &&
                user.Roles.Any(x => string.Equals(x.RoleName, DomainSecurityConstants.CompanyOwner, StringComparison.InvariantCultureIgnoreCase)))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}