﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.EmailServise
{
    public interface IPresentationEmailSenderService
    {
        void SendEmail(string email, string subject, string message);
    }
}