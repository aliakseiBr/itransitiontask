﻿using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.EmailServise
{
    public class PresentationEmailSenderService : IPresentationEmailSenderService
    {
        public void SendEmail(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Admin", "testaccbyalexey@gmail.com"));
            emailMessage.To.Add(new MailboxAddress(" ", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                client.Connect("smtp.gmail.com", 465, true);
                client.Authenticate("testaccbyalexey@gmail.com", "123lexa123");
                client.Send(emailMessage);
                client.Disconnect(true);
            }
        }
    }
}