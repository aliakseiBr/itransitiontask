﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.AccountServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.EmailServise;
using System;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.SecurityService
{
    public class PresentationAuthenticationService : IPresentationAuthenticationService
    {
        private IPresentationEmailSenderService emailSenderService;
        private ISecurityDomainService securityDomainService;

        public PresentationAuthenticationService(IPresentationEmailSenderService emailSenderService, ISecurityDomainService securityDomainService)
        {
            this.emailSenderService = emailSenderService;
            this.securityDomainService = securityDomainService;
        }

        public User GetUser(string email, string password)
        {
            return securityDomainService.GetUser(email, password);
        }

        public User GetUser(int userId, string password)
        {
            return securityDomainService.GetUser(userId, password);
        }

        public User GetUserWithRoles(string email, string password)
        {
            return securityDomainService.GetUserWithRoles(email, password);
        }

        public User GetUserWithRoles(int userId, string password)
        {
            return securityDomainService.GetUserWithRoles(userId, password);
        }

        public void AddRandomPassword(User user)
        {
            string password = securityDomainService.AddRandomPassword(user);
            emailSenderService.SendEmail("agrail111@yandex.ru", "Password", password);
        }

        public void ChangePassword(User user, string newPassword)
        {
            securityDomainService.ChangePassword(user, newPassword);
        }

        public int GetCurrentCompanyId()
        {
            if (HttpContext.Current.User.Identity is ICustomIdentity identity)
            {
                return identity.CompanyId;
            }
            else
            {
                throw new Exception(nameof(ICustomIdentity));
            }
        }

        public string GetCurrentUserEmail()
        {
            if (HttpContext.Current.User.Identity is ICustomIdentity identity)
            {
                return identity.Name;
            }
            else
            {
                throw new Exception(nameof(ICustomIdentity));
            }
        }

        public int GetCurrentUserId()
        {
            if (HttpContext.Current.User.Identity is ICustomIdentity identity)
            {
                return identity.UserId;
            }
            else
            {
                throw new Exception(nameof(ICustomIdentity));
            }
        }

        public bool IsInRole(string role)
        {
            return HttpContext.Current.User.IsInRole(role);
        }
    }
}