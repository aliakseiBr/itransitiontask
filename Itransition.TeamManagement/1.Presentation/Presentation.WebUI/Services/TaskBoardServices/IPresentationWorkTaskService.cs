﻿using Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.TaskBoardViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.TaskBoardServices
{
    public interface IPresentationWorkTaskService
    {
        TaskBoardWeekViewModel GetTaskBoardViewModel(WorkLogsPeriod logsPeriod);

        TaskBoardMonthViewModel GetTaskBoardMonthViewModel(WorkLogsPeriod logsPeriod);

        List<WorkLogViewModel> GetWorkLogsByTask(int taskId, WorkLogsPeriod logsPeriod);

        List<WorkTaskViewModel> GetActiveWorkTasksWithoutLogs(WorkLogsPeriod logsPeriod);

        ResultListModel CreateLogWork(CreateLogWorkViewModel createLogWorkViewModel);

        ResultListModel DeleteLogWork(int id);
    }
}