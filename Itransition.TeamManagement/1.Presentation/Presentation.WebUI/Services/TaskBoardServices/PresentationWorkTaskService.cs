﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using Itransition.TeamManagement.Domain.Services.IServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices;
using Itransition.TeamManagement.Presentation.WebUI.UIMapper;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.TaskBoardViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.TaskBoardServices
{
    public class PresentationWorkTaskService : IPresentationWorkTaskService
    {
        private const int MaxTimeSpent = 12;
        private const int MaxTimeLog = 12;
        private IPresentationAuthenticationService presentationAuthenticationService;
        private IWorkTaskDomainService workTaskDomainService;
        private IPresentationWorkTaskMapper presentationWorkTaskMapper;
        private IWorkLogDomainService workLogDomainService;
        private IUnitOfWork uow;

        public PresentationWorkTaskService(
            IPresentationAuthenticationService presentationAuthenticationService,
            IWorkTaskDomainService workTaskDomainService,
            IWorkLogDomainService workLogDomainService,
            IPresentationWorkTaskMapper presentationWorkTaskMapper,
            IUnitOfWork uow)
        {
            this.workLogDomainService = workLogDomainService;
            this.presentationAuthenticationService = presentationAuthenticationService;
            this.workTaskDomainService = workTaskDomainService;
            this.presentationWorkTaskMapper = presentationWorkTaskMapper;
            this.uow = uow;
        }

        public ResultListModel CreateLogWork(CreateLogWorkViewModel createLogWorkViewModel)
        {
            bool limit = workLogDomainService.LogHoursLimit(createLogWorkViewModel.LogDay.Value, createLogWorkViewModel.TimeSpent, MaxTimeLog);
            if (limit)
            {
                var errors = new List<string>() { Properties.LocalResource.TaskBoard.Index.TimeSpentLimit };

                return new ResultListModel(result: false, errorMessages: errors);
            }

            WorkLog workLog = presentationWorkTaskMapper.ConvertToWorkLogDomain(createLogWorkViewModel);
            workLogDomainService.Create(workLog);
            uow.SaveChanges();

            return new ResultListModel();
        }

        public ResultListModel DeleteLogWork(int id)
        {
            var log = workLogDomainService.Get(id);
            bool result = workLogDomainService.Delete(log);
            uow.SaveChanges();

            return new ResultListModel(result: result, errorMessages: null);
        }

        public List<WorkTaskViewModel> GetActiveWorkTasksWithoutLogs(WorkLogsPeriod logsPeriod)
        {
            int currentUserId = presentationAuthenticationService.GetCurrentUserId();
            IEnumerable<WorkTask> tasks = workTaskDomainService.GetActiveWithoutLogsBeforePeriod(currentUserId, logsPeriod.StartPeriod, logsPeriod.EndPeriod);
            List<WorkTaskViewModel> workTaskViewModels = presentationWorkTaskMapper.ConvertWorkTaskToWorkTaskViewModel(tasks);

            return workTaskViewModels;
        }

        public TaskBoardMonthViewModel GetTaskBoardMonthViewModel(WorkLogsPeriod logsPeriod)
        {
            if (logsPeriod.StartPeriod == default(DateTime) || logsPeriod.EndPeriod == default(DateTime))
            {
                throw new ArgumentException(nameof(logsPeriod));
            }

            int currentUserId = presentationAuthenticationService.GetCurrentUserId();
            IEnumerable<WorkLog> logs = workLogDomainService.GetByPeriodWithTask(currentUserId, logsPeriod.StartPeriod, logsPeriod.EndPeriod);
            TaskBoardMonthViewModel taskBoardMonthViewModel = presentationWorkTaskMapper.ConvertLogsToTaskBoardMonthViewModel(logs, logsPeriod);

            return taskBoardMonthViewModel;
        }

        public TaskBoardWeekViewModel GetTaskBoardViewModel(WorkLogsPeriod logsPeriod)
        {
            if (logsPeriod.StartPeriod == default(DateTime))
            {
                logsPeriod = GetStandartLogsPeriod(DateTime.Now, DayOfWeek.Monday);
            }

            int currentUserId = presentationAuthenticationService.GetCurrentUserId();
            IEnumerable<WorkLog> logs = workLogDomainService.GetByPeriodWithTask(currentUserId, logsPeriod.StartPeriod, logsPeriod.EndPeriod);
            TaskBoardWeekViewModel taskBoardViewModel = presentationWorkTaskMapper.ConvertLogsToTaskBoardViewModel(logs, logsPeriod);

            return taskBoardViewModel;
        }

        public List<WorkLogViewModel> GetWorkLogsByTask(int taskId, WorkLogsPeriod logsPeriod)
        {
            IEnumerable<WorkLog> logs = workLogDomainService.GetByPeriod(taskId, logsPeriod.StartPeriod, logsPeriod.EndPeriod);
            List<WorkLogViewModel> workLogViewModels = presentationWorkTaskMapper.ConvertLogsToWorkLogViewModel(logs);

            return workLogViewModels;
        }

        private WorkLogsPeriod GetStandartLogsPeriod(DateTime currentDateTime, DayOfWeek startOfWeek)
        {
            int diff = (7 + (currentDateTime.DayOfWeek - startOfWeek)) % 7;
            DateTime startWeek = currentDateTime.AddDays(-1 * diff).Date;
            DateTime endWeek = startWeek.AddDays(7).AddMilliseconds(-1);

            return new WorkLogsPeriod() { StartPeriod = startWeek, EndPeriod = endWeek };
        }
    }
}