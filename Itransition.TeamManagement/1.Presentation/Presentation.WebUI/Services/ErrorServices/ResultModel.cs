﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices
{
    public class ResultModel
    {
        public ResultModel()
        {
            IsValid = true;
        }

        public ResultModel(bool result, string errorMessage)
        {
            IsValid = result;
            ErrorMessage = errorMessage;
        }

        public bool IsValid { get; private set; }

        public string ErrorMessage { get; private set; }
    }
}