﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices
{
    public class ResultListModel
    {
        public ResultListModel()
        {
            IsValid = true;
        }

        public ResultListModel(bool result, List<string> errorMessages)
        {
            IsValid = result;
            ErrorMessages = errorMessages;
        }

        public bool IsValid { get; private set; }

        public List<string> ErrorMessages { get; private set; }
    }
}