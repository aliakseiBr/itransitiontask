﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using Itransition.TeamManagement.Domain.Services.IServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.PlaginationServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.SortingService;
using Itransition.TeamManagement.Presentation.WebUI.UIMapper;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.CityViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.CityServices
{
    public class PresentationCityService : IPresentationCityService
    {
        private ICountryDomainService countryDomainService;
        private ICityDomainService cityDomainService;
        private IPresentationPlaginationService plaginationService;
        private IPresentationSortingService sortingService;
        private IPresentationCityMapper presentationCityMapper;
        private IPresentationSelectListItemMapper presentationSelectListItemMapper;
        private IPresentationAuthenticationService presentationAuthenticationService;
        private IUnitOfWork uow;

        public PresentationCityService(
            ICountryDomainService countryDomainService,
            ICityDomainService cityDomainService,
            IPresentationPlaginationService plaginationService,
            IPresentationSortingService sortingService,
            IPresentationCityMapper presentationCityMapper,
            IPresentationSelectListItemMapper presentationSelectListItemMapper,
            IPresentationAuthenticationService presentationAuthenticationService,
            IUnitOfWork uow)
        {
            this.countryDomainService = countryDomainService;
            this.cityDomainService = cityDomainService;
            this.plaginationService = plaginationService;
            this.sortingService = sortingService;
            this.presentationCityMapper = presentationCityMapper;
            this.presentationAuthenticationService = presentationAuthenticationService;
            this.presentationSelectListItemMapper = presentationSelectListItemMapper;
            this.uow = uow;
        }

        public CityPlaginationViewModel GetCityPlaginationViewModel(int page, int pageSize)
        {
            int companyId = presentationAuthenticationService.GetCurrentCompanyId();
            CityPlaginationViewModel cityPlaginationViewModel = plaginationService.GetCityPlaginationModel(page, pageSize, companyId);

            return cityPlaginationViewModel;
        }

        public CityCreateViewModel GetCityCreateViewModel()
        {
            int companyId = presentationAuthenticationService.GetCurrentCompanyId();
            IEnumerable<Country> countries = countryDomainService.GetActiveByCompany(companyId);
            List<SelectListItem> listCountries = presentationSelectListItemMapper.ConvertToSelectListItem(countries);

            return new CityCreateViewModel()
            {
                Contries = listCountries
            };
        }

        public ResultModel CreateCity(CityCreateViewModel model)
        {
            Country country = countryDomainService.Get(model.CountryId.Value);
            City city = presentationCityMapper.ConvertToCityDomain(model);
            bool result = cityDomainService.CreateCity(country, city);

            if (result)
            {
                uow.SaveChanges();

                return new ResultModel();
            }
            else
            {
                return new ResultModel(result: false, errorMessage: Properties.LocalResource.City.CityErrorMessages.ErrorNameExists);
            }
        }

        public CityCreateViewModel UpdateCityCreateViewModel(CityCreateViewModel cityCreateViewModel)
        {
            int companyId = presentationAuthenticationService.GetCurrentCompanyId();
            IEnumerable<Country> countries = countryDomainService.GetActiveByCompany(companyId);
            List<SelectListItem> listCountries = presentationSelectListItemMapper.ConvertToSelectListItem(countries);
            cityCreateViewModel.Contries = listCountries;

            return cityCreateViewModel;
        }

        public CityUpdateViewModel GetCityUpdateViewModel(int? cityId, int? countryId)
        {
            City city = cityDomainService.Get(cityId.Value);
            CityUpdateViewModel model = presentationCityMapper.ConvertToCityUpdateModel(city);

            return model;
        }

        public ResultModel UpdateCity(CityUpdateViewModel model)
        {
            City city = cityDomainService.Get(model.CityId.Value);

            if (string.Equals(city.CityName, model.Name, StringComparison.InvariantCultureIgnoreCase))
            {
                return new ResultModel();
            }

            if (cityDomainService.CheckContainsCityName(model.Name, model.CountryId.Value))
            {
                city = cityDomainService.Get(model.CityId.Value);

                return new ResultModel(result: false, errorMessage: Properties.LocalResource.City.CityErrorMessages.ErrorNameExists);
            }
            else
            {
                City updateCity = presentationCityMapper.ConvertToCityDomain(city, model);
                uow.SaveChanges();

                return new ResultModel();
            }
        }

        public ResultModel DeleteCity(int? cityId, int? countryId)
        {
            City city = cityDomainService.Get(cityId.Value);
            bool resultDelete = cityDomainService.Delete(city);

            if (resultDelete)
            {
                uow.SaveChanges();

                return new ResultModel();
            }
            else
            {
                return new ResultModel(result: false, errorMessage: Properties.LocalResource.City.CityErrorMessages.ErrorDeletion);
            }
        }
    }
}