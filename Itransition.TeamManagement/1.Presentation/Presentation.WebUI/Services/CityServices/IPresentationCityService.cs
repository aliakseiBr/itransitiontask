﻿using Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.CityViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.CityServices
{
    public interface IPresentationCityService
    {
        CityPlaginationViewModel GetCityPlaginationViewModel(int page, int pageSize);

        CityCreateViewModel GetCityCreateViewModel();

        ResultModel CreateCity(CityCreateViewModel model);

        CityCreateViewModel UpdateCityCreateViewModel(CityCreateViewModel cityCreateViewModel);

        CityUpdateViewModel GetCityUpdateViewModel(int? cityId, int? countryId);

        ResultModel UpdateCity(CityUpdateViewModel model);

        ResultModel DeleteCity(int? cityId, int? countryId);
    }
}
