﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.AccountViewModels;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.AccountServices
{
    public class PresentationAccountService : IPresentationAccountService
    {
        private const string UserIdDesignation = "userId=";
        private const string CompanyIdDesignation = "companyId=";
        private const string RolesDesignation = "userRoles=";
        private const string UserDataSeparator = ";";
        private const string UserRolesSeparator = "|";
        private IPresentationAuthenticationService presentationAuthenticationService;
        private IUserDomainService userDomainService;
        private IRoleDomainService roleDomainService;

        public PresentationAccountService(IPresentationAuthenticationService presentationAuthenticationService, IUserDomainService userDomainService, IRoleDomainService roleDomainService)
        {
            this.presentationAuthenticationService = presentationAuthenticationService;
            this.userDomainService = userDomainService;
            this.roleDomainService = roleDomainService;
        }

        public ResultModel Authentication(LoginViewModel loginViewModel)
        {
            User user = presentationAuthenticationService.GetUserWithRoles(loginViewModel.Email, loginViewModel.Password);

            if (user == null)
            {
                return new ResultModel(result: false, errorMessage: Properties.LocalResource.Account.Login.ErrorAuthentication);
            }

            SetAuthenticationCookie(user, loginViewModel.RememberMe);

            return new ResultModel();
        }

        public void LogOff()
        {
            FormsAuthentication.SignOut();
        }

        private void SetAuthenticationCookie(User user, bool isPersistent)
        {
            string userData = GetUserData(user);
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                user.Id,
                user.Email,
                DateTime.Now,
                DateTime.Now.AddMinutes(30),
                isPersistent,
                userData,
                FormsAuthentication.FormsCookiePath);
            string encryptedTicket = FormsAuthentication.Encrypt(ticket);
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            cookie.HttpOnly = true;
            SetCookiesToHttpContext(cookie);
        }

        private string GetUserData(User user)
        {
            IEnumerable<string> userRoles = user.Roles.Select(x => x.RoleName);
            string userId = UserIdDesignation + user.Id;
            string companyId = CompanyIdDesignation + user.CompanyId;
            string roles = RolesDesignation + string.Join(UserRolesSeparator, userRoles) + UserDataSeparator;
            string userData = string.Join(UserDataSeparator, userId, companyId, roles);

            return userData;
        }

        private void SetCookiesToHttpContext(HttpCookie cookie)
        {
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
    }
}
