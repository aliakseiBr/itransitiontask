﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.AccountServices
{
    public interface ICustomIdentity : IIdentity
    {
        int CompanyId { get; set; }

        int UserId { get; set; }
    }
}
