﻿using Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.AccountViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.AccountServices
{
    public interface IPresentationAccountService
    {
        ResultModel Authentication(LoginViewModel loginViewModel);

        void LogOff();
    }
}
