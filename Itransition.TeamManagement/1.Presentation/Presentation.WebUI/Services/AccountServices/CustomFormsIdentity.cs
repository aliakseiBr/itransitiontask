﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.AccountServices
{
    public class CustomFormsIdentity : FormsIdentity, ICustomIdentity
    {
        public CustomFormsIdentity(FormsAuthenticationTicket ticket, int userId, int companyId)
            : base(ticket)
        {
            UserId = userId;
            CompanyId = companyId;
        }

        public int CompanyId { get; set; }

        public int UserId { get; set; }
    }
}