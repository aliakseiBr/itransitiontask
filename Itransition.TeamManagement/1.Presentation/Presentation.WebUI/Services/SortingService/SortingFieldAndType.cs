﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.SortingService
{
    public class SortingFieldAndType
    {
        public SortingTypeEnum SortingType { get; set; }

        public SortingUserTableFieldsEnum SortingUserTableField { get; set; }
    }
}