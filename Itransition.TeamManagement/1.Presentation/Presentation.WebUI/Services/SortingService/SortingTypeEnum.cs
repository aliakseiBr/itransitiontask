﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.SortingService
{
    public enum SortingTypeEnum
    {
        Asc = 1,
        Des = 2,
    }
}