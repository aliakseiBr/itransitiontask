﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Filtering;
using Itransition.TeamManagement.Domain.Core.Sorting;
using Itransition.TeamManagement.Domain.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.SortingService
{
    public class PresentationSortingService : IPresentationSortingService
    {
        private const char QuerySeparator = '?';
        private const char ParamsSeparator = '&';
        private const char ValueSeparator = '=';
        private const string FullName = "FullName";
        private const string City = "City";
        private const string CityName = "CityName";
        private const string CountryName = "CountryName";
        private const string Country = "Country";
        private const string TitleName = "TitleName";
        private const string FirstName = "FirstName";
        private const string LastName = "LastName";

        private IUserDomainService userDomainService;
        private ICityDomainService cityDomainService;
        private ICountryDomainService countryDomainService;

        public PresentationSortingService(IUserDomainService userDomainService, ICityDomainService cityDomainService, ICountryDomainService countryDomainService)
        {
            this.userDomainService = userDomainService;
            this.countryDomainService = countryDomainService;
            this.cityDomainService = cityDomainService;
        }

        public SortedList<int, SortingFieldAndType> GetCurrentSortingDictionary(string requestUrlPathAndQuery)
        {
            SortedList<int, SortingFieldAndType> sortingKeys = new SortedList<int, SortingFieldAndType>();
            int index = requestUrlPathAndQuery.IndexOf(QuerySeparator);

            if (index == -1)
            {
                return sortingKeys;
            }

            string stringParams = requestUrlPathAndQuery.Substring(index + 1);
            string[] arrayParams = stringParams.Split(new char[] { ParamsSeparator, ValueSeparator });

            for (int i = 0; i < arrayParams.Length; i++)
            {
                bool a = СheckNonNumber(arrayParams[i]);
                bool b = Enum.TryParse(arrayParams[i], true, out SortingUserTableFieldsEnum sortField);
                bool c = СheckNonNumber(arrayParams[i + 1]);
                bool d = Enum.TryParse(arrayParams[i + 1], true, out SortingTypeEnum sortType);

                if (a && b && c && d)
                {
                    sortingKeys.Add(i, new SortingFieldAndType() { SortingType = sortType, SortingUserTableField = sortField });
                }

                i++;
            }

            return sortingKeys;
        }

        public IEnumerable<User> SortingAndFilteringUserByCompany(int companyId, SortingAndFilteringParams sortingAndFilteringParams)
        {
            IEnumerable<User> users = userDomainService.GetActiveByCompanyWithRoles(
                companyId,
                sortingAndFilteringParams.Page,
                sortingAndFilteringParams.PageSize,
                sortingAndFilteringParams.SortingParams,
                sortingAndFilteringParams.FilteringParams);

            return users;
        }

        public IEnumerable<City> SortingAndFilteringCityByCompany(int companyId, SortingAndFilteringParams sortingAndFilteringParams)
        {
            IEnumerable<City> cities = cityDomainService.GetActiveByCompany(
                companyId,
                sortingAndFilteringParams.Page,
                sortingAndFilteringParams.PageSize);

            return cities;
        }

        public IEnumerable<Country> SortingAndFilteringCountryByCompany(int companyId, SortingAndFilteringParams sortingAndFilteringParams)
        {
            IEnumerable<Country> countries = countryDomainService.GetActiveByCompany(
                companyId,
                sortingAndFilteringParams.Page,
                sortingAndFilteringParams.PageSize);

            return countries;
        }

        public SortedList<int, FilteringParams> GetUserTableFilteringParams(string searchSrting)
        {
            var filters = Enum.GetNames(typeof(SortingUserTableFieldsEnum)).AsEnumerable();
            SortedList<int, FilteringParams> filteringParamsList = new SortedList<int, FilteringParams>();
            FilteringParams filteringParams = null;
            int count = 1;

            foreach (var item in filters)
            {
                filteringParams = new FilteringParams();
                filteringParams.FilterField = item.ToString();
                filteringParams.FilterString = searchSrting;
                UpdateTableFilteringParams(filteringParams, item);
                filteringParamsList.Add(count, filteringParams);
                count++;
            }

            return filteringParamsList;
        }

        public SortedList<int, SortingParams> GetUserTableSortingParams(SortedList<int, SortingFieldAndType> sortingKeys)
        {
            SortedList<int, SortingParams> sortingParamsList = new SortedList<int, SortingParams>();
            SortingParams sortingParams = null;
            int count = 1;

            foreach (var item in sortingKeys)
            {
                sortingParams = new SortingParams();
                sortingParams.SortField = item.Value.SortingUserTableField.ToString();
                sortingParams.DescendingSort = item.Value.SortingType == SortingTypeEnum.Asc ? false : true;

                if (item.Value.SortingUserTableField == SortingUserTableFieldsEnum.Name)
                {
                    sortingParams.SortField = FirstName;
                    SortingParams sorting = new SortingParams();
                    sorting.SortField = LastName;
                    sorting.DescendingSort = item.Value.SortingType == SortingTypeEnum.Asc ? false : true;
                    sortingParamsList.Add(count, sorting);
                    count++;
                }

                UpdateTableSortingParams(sortingParams, item);
                sortingParamsList.Add(count, sortingParams);
                count++;
            }

            if (sortingParamsList.Count() == 0)
            {
                sortingParamsList.Add(1, new SortingParams() { SortField = LastName, DescendingSort = false });
            }

            return sortingParamsList;
        }

        private void UpdateTableFilteringParams(FilteringParams filteringParams, string filterField)
        {
            if (filterField == SortingUserTableFieldsEnum.Name.ToString())
            {
                filteringParams.FilterField = FullName;
            }
            else if (filterField == SortingUserTableFieldsEnum.City.ToString())
            {
                filteringParams.ChildFields = new List<string>() { CityName };
            }
            else if (filterField == SortingUserTableFieldsEnum.Country.ToString())
            {
                filteringParams.FilterField = City;
                filteringParams.ChildFields = new List<string>() { Country, CountryName };
            }
            else if (filterField == SortingUserTableFieldsEnum.Title.ToString())
            {
                filteringParams.ChildFields = new List<string>() { TitleName };
            }
        }

        private void UpdateTableSortingParams(SortingParams sortingParams, KeyValuePair<int, SortingFieldAndType> sortingPair)
        {
            if (sortingPair.Value.SortingUserTableField == SortingUserTableFieldsEnum.City)
            {
                sortingParams.ChildFields = new List<string>() { CityName };
            }
            else if (sortingPair.Value.SortingUserTableField == SortingUserTableFieldsEnum.Country)
            {
                sortingParams.SortField = City;
                sortingParams.ChildFields = new List<string>() { Country, CountryName };
            }
            else if (sortingPair.Value.SortingUserTableField == SortingUserTableFieldsEnum.Title)
            {
                sortingParams.ChildFields = new List<string>() { TitleName };
            }
        }

        private bool СheckNonNumber(string word)
        {
            return !int.TryParse(word, out int num);
        }
    }
}