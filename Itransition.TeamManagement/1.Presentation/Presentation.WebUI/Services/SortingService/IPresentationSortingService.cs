﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Filtering;
using Itransition.TeamManagement.Domain.Core.Sorting;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.SortingService
{
    public interface IPresentationSortingService
    {
        SortedList<int, SortingFieldAndType> GetCurrentSortingDictionary(string requestUrlPathAndQuery);

        IEnumerable<User> SortingAndFilteringUserByCompany(int companyId, SortingAndFilteringParams sortingAndFilteringParams);

        IEnumerable<City> SortingAndFilteringCityByCompany(int companyId, SortingAndFilteringParams sortingAndFilteringParams);

        IEnumerable<Country> SortingAndFilteringCountryByCompany(int companyId, SortingAndFilteringParams sortingAndFilteringParams);

        SortedList<int, SortingParams> GetUserTableSortingParams(SortedList<int, SortingFieldAndType> sortingKeys);

        SortedList<int, FilteringParams> GetUserTableFilteringParams(string searchSrting);
    }
}