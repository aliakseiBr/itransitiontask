﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.SortingService
{
    public enum SortingUserTableFieldsEnum
    {
        Name = 1,
        Title = 2,
        Country = 3,
        City = 4,
        Phone = 5,
        Email = 6
    }
}