﻿using Itransition.TeamManagement.Domain.Core.Filtering;
using Itransition.TeamManagement.Domain.Core.Sorting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.SortingService
{
    public class SortingAndFilteringParams
    {
        public SortingAndFilteringParams(int page, int pageSize)
        {
            Page = (page - 1) * pageSize;
            PageSize = pageSize;
        }

        public SortingAndFilteringParams(SortedList<int, SortingParams> sortingParams, SortedList<int, FilteringParams> filteringParams, int numberPage, int pageSize)
        {
            SortingParams = sortingParams;
            FilteringParams = filteringParams;
            Page = (numberPage - 1) * pageSize;
            PageSize = pageSize;
        }

        public SortedList<int, SortingParams> SortingParams { get; private set; }

        public SortedList<int, FilteringParams> FilteringParams { get; private set; }

        public int Page { get; private set; }

        public int PageSize { get; private set; }
    }
}