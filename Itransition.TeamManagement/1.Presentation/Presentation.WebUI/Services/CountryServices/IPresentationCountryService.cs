﻿using Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.CountryViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.CountryServices
{
    public interface IPresentationCountryService
    {
        CountryPlaginationViewModel GetCountryPlaginationViewModel(int page, int pageSize);

        CountryCreateViewModel GetCountryCreateViewModel();

        ResultModel CreateCountry(CountryCreateViewModel countryCreateViewModel);

        CountryUpdateViewModel GetCountryUpdateViewModel(int? countryId);

        ResultModel UpdateCountry(CountryUpdateViewModel model);

        ResultModel DeleteCountry(int? countryId);
    }
}
