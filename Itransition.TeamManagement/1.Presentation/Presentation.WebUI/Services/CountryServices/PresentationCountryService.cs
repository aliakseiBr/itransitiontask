﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using Itransition.TeamManagement.Domain.Services.IServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.PlaginationServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.SortingService;
using Itransition.TeamManagement.Presentation.WebUI.UIMapper;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.CountryViewModels;
using System;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.CountryServices
{
    public class PresentationCountryService : IPresentationCountryService
    {
        private IUserDomainService userDomainService;
        private ICountryDomainService countryDomainService;
        private IPresentationPlaginationService plaginationService;
        private IPresentationSortingService sortingService;
        private IPresentationCountryMapper mapper;
        private IPresentationAuthenticationService presentationAuthenticationService;
        private IUnitOfWork uow;

        public PresentationCountryService(
            IUserDomainService userDomainService,
            ICountryDomainService countryDomainService,
            IPresentationPlaginationService plaginationService,
            IPresentationSortingService sortingService,
            IPresentationCountryMapper mapper,
            IPresentationAuthenticationService presentationAuthenticationService,
            IUnitOfWork uow)
        {
            this.userDomainService = userDomainService;
            this.countryDomainService = countryDomainService;
            this.plaginationService = plaginationService;
            this.sortingService = sortingService;
            this.mapper = mapper;
            this.presentationAuthenticationService = presentationAuthenticationService;
            this.uow = uow;
        }

        public CountryPlaginationViewModel GetCountryPlaginationViewModel(int page, int pageSize)
        {
            int companyId = presentationAuthenticationService.GetCurrentCompanyId();
            CountryPlaginationViewModel model = plaginationService.GetCountryPlaginationModel(page, pageSize, companyId);

            return model;
        }

        public ResultModel CreateCountry(CountryCreateViewModel countryCreateViewModel)
        {
            Country country = mapper.ConvertToCountryDomain(countryCreateViewModel);
            bool createResult = countryDomainService.Create(country);

            if (createResult)
            {
                uow.SaveChanges();

                return new ResultModel();
            }
            else
            {
                return new ResultModel(result: false, errorMessage: Properties.LocalResource.Country.CountryErrorMessages.ErrorNameExists);
            }
        }

        public ResultModel DeleteCountry(int? countryId)
        {
            Country country = countryDomainService.Get(countryId.Value);
            bool deleteResult = countryDomainService.Delete(country);

            if (deleteResult)
            {
                uow.SaveChanges();

                return new ResultModel();
            }
            else
            {
                return new ResultModel(result: false, errorMessage: Properties.LocalResource.Country.CountryErrorMessages.ErrorDeletion);
            }
        }

        public CountryCreateViewModel GetCountryCreateViewModel()
        {
            return new CountryCreateViewModel();
        }

        public CountryUpdateViewModel GetCountryUpdateViewModel(int? countryId)
        {
            Country country = countryDomainService.Get(countryId.Value);
            CountryUpdateViewModel countryUpdateViewModel = mapper.ConvertToCountryUpdateModel(country);

            return countryUpdateViewModel;
        }

        public ResultModel UpdateCountry(CountryUpdateViewModel model)
        {
            Country country = countryDomainService.Get(model.CountryId.Value);

            if (string.Equals(country.CountryName, model.Name, StringComparison.InvariantCultureIgnoreCase))
            {
                return new ResultModel();
            }

            if (countryDomainService.CheckContainsCountryName(model.Name))
            {
                return new ResultModel(result: false, errorMessage: Properties.LocalResource.Country.CountryErrorMessages.ErrorNameExists);
            }
            else
            {
                Country updateCountry = mapper.ConvertToCountryDomain(country, model);
                uow.SaveChanges();

                return new ResultModel();
            }
        }
    }
}