﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.UserViewModels;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.UserViewModels.AbstractUserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.UserServices
{
    public interface IPresentationUserService
    {
        ResultModel ChangeOldPassword(UserUpdateOldPasswordViewModel userUpdateOldPasswordViewModel);

        UserPlaginationViewModel GetUserPlaginationViewModel(int page, int pageSize, string search, string pathAndQuery);

        UserCommentPopUpViewModel GetCommentPopUpViewModel(int? userId);

        UserCreateViewModel GetUserCreateViewModel();

        void UpdateUserViewModel(UserViewModel userViewModel);

        ResultModel CreateUser(UserCreateViewModel userCreateModel);

        List<City> GetCities(int? countryId);

        ParentUserUpdateViewModel GetParentUserUpdateViewModel(int? userId);

        void UpdateParentUserUpdateViewModel(ParentUserUpdateViewModel userUpdateViewModel);

        ResultModel UpdateUser(UserUpdateViewModel userUpdateViewModel);

        ResultModel DeleteUser(int? userId);

        void AddUserComment(int? userId, string comment);
    }
}