﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Security;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using Itransition.TeamManagement.Domain.Services.IServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.ErrorServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.PlaginationServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.SecurityService;
using Itransition.TeamManagement.Presentation.WebUI.Services.SortingService;
using Itransition.TeamManagement.Presentation.WebUI.UIMapper;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.UserViewModels;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.UserViewModels.AbstractUserViewModels;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.UserServices
{
    public class PresentationUserService : IPresentationUserService
    {
        private IUserDomainService userDomainService;
        private ICountryDomainService countryDomainService;
        private ICityDomainService cityDomainService;
        private IRoleDomainService roleDomainService;
        private IPresentationPlaginationService userPlaginationService;
        private IPresentationSortingService sortingService;
        private IPresentationUserMapper presentationUserMapper;
        private IPresentationAuthenticationService presentationAuthenticationService;
        private IPresentationSelectListItemMapper presentationSelectListItemMapper;
        private IUnitOfWork uow;

        public PresentationUserService(
            IUserDomainService userDomainService,
            ICountryDomainService countryDomainService,
            ICityDomainService cityDomainService,
            IRoleDomainService roleDomainService,
            IPresentationPlaginationService userPlaginationService,
            IPresentationSortingService sortingService,
            IPresentationAuthenticationService presentationAuthenticationService,
            IPresentationUserMapper presentationUserMapper,
            IPresentationSelectListItemMapper presentationSelectListItemMapper,
            IUnitOfWork uow)
        {
            this.userDomainService = userDomainService;
            this.countryDomainService = countryDomainService;
            this.cityDomainService = cityDomainService;
            this.roleDomainService = roleDomainService;
            this.userPlaginationService = userPlaginationService;
            this.sortingService = sortingService;
            this.presentationAuthenticationService = presentationAuthenticationService;
            this.presentationUserMapper = presentationUserMapper;
            this.presentationSelectListItemMapper = presentationSelectListItemMapper;
            this.uow = uow;
        }

        public UserPlaginationViewModel GetUserPlaginationViewModel(int page, int pageSize, string search, string pathAndQuery)
        {
            int companyId = presentationAuthenticationService.GetCurrentCompanyId();
            UserPlaginationViewModel userPlaginationViewModel = userPlaginationService.GetUserPlaginationModel(page, pageSize, search, pathAndQuery, companyId);

            return userPlaginationViewModel;
        }

        public ResultModel ChangeOldPassword(UserUpdateOldPasswordViewModel userUpdateOldPasswordViewModel)
        {
            User user = presentationAuthenticationService.GetUser(userUpdateOldPasswordViewModel.UserId.Value, userUpdateOldPasswordViewModel.OldPassword);

            if (user != null)
            {
                presentationAuthenticationService.ChangePassword(user, userUpdateOldPasswordViewModel.Password);
                uow.SaveChanges();

                return new ResultModel();
            }
            else
            {
                return new ResultModel(result: false, errorMessage: Properties.LocalResource.User.UserErrorMessages.ErrorChangeOldPassword);
            }
        }

        public ResultModel CreateUser(UserCreateViewModel userCreateModel)
        {
            bool uniqueEmail = userDomainService.UniqueEmail(userCreateModel.Email);

            if (!uniqueEmail)
            {
                return new ResultModel(result: false, errorMessage: Properties.LocalResource.User.UserErrorMessages.ErrorUniqueEmail);
            }

            User user = presentationUserMapper.ConvertToUserDomain(userCreateModel);
            userDomainService.Create(user);
            presentationAuthenticationService.AddRandomPassword(user);
            uow.SaveChanges();

            return new ResultModel();
        }

        public ResultModel DeleteUser(int? userId)
        {
            User user = userDomainService.Get(userId.Value);
            bool deletionResult = userDomainService.Delete(user);
            uow.SaveChanges();

            if (deletionResult)
            {
                return new ResultModel();
            }
            else
            {
                return new ResultModel(result: false, errorMessage: Properties.LocalResource.User.UserErrorMessages.ErrorAccessToUsers);
            }
        }

        public List<City> GetCities(int? countryId)
        {
            if (countryId != null)
            {
                return cityDomainService.GetAllActiveCitiesOfCountry(countryId.Value).ToList();
            }
            else
            {
                return new List<City>();
            }
        }

        public UserCommentPopUpViewModel GetCommentPopUpViewModel(int? userId)
        {
            if (userId != null)
            {
                List<Comment> userComments = userDomainService
                    .GetAllComments(userId.Value)
                    .OrderBy(x => x.CreateDate)
                    .ToList();

                return new UserCommentPopUpViewModel() { UserId = userId.Value, UserComments = userComments };
            }
            else
            {
                return null;
            }
        }

        public UserUpdateViewModel GetUserUpdateViewModel(int? userId)
        {
            int currentUserId = presentationAuthenticationService.GetCurrentUserId();
            User user = userDomainService.GetWithRoles(userId.Value);
            UserUpdateViewModel userUpdateViewModel = presentationUserMapper.ConvertToUserEditorModel(user);
            UpdateUserViewModel(userUpdateViewModel);

            return userUpdateViewModel;
        }

        public ParentUserUpdateViewModel GetParentUserUpdateViewModel(int? userId)
        {
            var parentUserUpdateViewModel = new ParentUserUpdateViewModel()
            {
                UserUpdateViewModel = GetUserUpdateViewModel(userId.Value),
                UserUpdateOldPasswordViewModel = new UserUpdateOldPasswordViewModel() { UserId = userId.Value }
            };

            return parentUserUpdateViewModel;
        }

        public UserCreateViewModel GetUserCreateViewModel()
        {
            User user = GetCurrentUserDomainModelWithRoles();

            return new UserCreateViewModel()
            {
                Cities = new List<SelectListItem>(),
                Contries = GetCountriesByCompanyId(user.CompanyId.Value),
                Titles = GetAllUserTitles(),
                Roles = GetAvailableRoles(user.Roles)
            };
        }

        public void UpdateParentUserUpdateViewModel(ParentUserUpdateViewModel parentUserUpdateViewModel)
        {
            if (parentUserUpdateViewModel.UserUpdateViewModel != null)
            {
                UpdateUserViewModel(parentUserUpdateViewModel.UserUpdateViewModel);
                parentUserUpdateViewModel.UserUpdateOldPasswordViewModel = new UserUpdateOldPasswordViewModel()
                {
                    UserId = parentUserUpdateViewModel.UserUpdateViewModel.UserId
                };
            }
            else
            {
                parentUserUpdateViewModel.UserUpdateViewModel = GetUserUpdateViewModel(parentUserUpdateViewModel.UserUpdateOldPasswordViewModel.UserId.Value);
            }
        }

        public ResultModel UpdateUser(UserUpdateViewModel userUpdateViewModel)
        {
            int currentUserId = presentationAuthenticationService.GetCurrentUserId();
            User user = userDomainService.Get(userUpdateViewModel.UserId.Value);

            if (user == null)
            {
                throw new Exception();
            }

            if (user.Email != userUpdateViewModel.Email)
            {
                bool uniqueEmail = userDomainService.UniqueEmail(userUpdateViewModel.Email);

                if (!uniqueEmail)
                {
                    return new ResultModel(result: false, errorMessage: Properties.LocalResource.User.UserErrorMessages.ErrorUniqueEmail);
                }
            }

            User updateUser = presentationUserMapper.ConvertToUserDomain(user, userUpdateViewModel);
            uow.SaveChanges();

            return new ResultModel();
        }

        public void UpdateUserViewModel(UserViewModel userViewModel)
        {
            User user = GetCurrentUserDomainModelWithRoles();
            userViewModel.Roles = GetAvailableRoles(user.Roles);
            userViewModel.Cities = GetSelectListItemCities(userViewModel.CountryId);
            userViewModel.Contries = GetCountriesByCompanyId(user.CompanyId.Value);
            userViewModel.Titles = GetAllUserTitles();
        }

        public void AddUserComment(int? userId, string comment)
        {
            int currentUserId = presentationAuthenticationService.GetCurrentUserId();
            User user = userDomainService.Get(userId.Value);
            var userComment = new Comment() { Body = comment, CreateDate = DateTime.Now, UpdateDate = DateTime.Now };
            userDomainService.AddComment(user, userComment);
            uow.SaveChanges();
        }

        private User GetCurrentUserDomainModelWithRoles()
        {
            int currentUserId = presentationAuthenticationService.GetCurrentUserId();
            User user = userDomainService.GetWithRoles(currentUserId);

            return user;
        }

        private List<SelectListItem> GetCountriesByCompanyId(int companyId)
        {
            IEnumerable<Country> countries = countryDomainService.GetActiveWithoutEmptyCitiesByCompany(companyId);
            List<SelectListItem> listCountries = presentationSelectListItemMapper.ConvertToSelectListItem(countries);

            return listCountries;
        }

        private List<SelectListItem> GetAllUserTitles()
        {
            IEnumerable<Title> titles = userDomainService.GetAllTitles();
            List<SelectListItem> listTitles = presentationSelectListItemMapper.ConvertToSelectListItem(titles);

            return listTitles;
        }

        private List<SelectListItem> GetAvailableRoles(IEnumerable<Role> currentUserRoles)
        {
            IEnumerable<Role> roles = null;

            if (currentUserRoles.Any(x => string.Equals(x.RoleName, DomainSecurityConstants.CompanyOwner, StringComparison.InvariantCultureIgnoreCase)))
            {
                roles = roleDomainService.GetAll();
            }
            else
            {
                roles = new List<Role>() { roleDomainService.GetByName(DomainSecurityConstants.User) };
            }

            List<SelectListItem> listRoles = presentationSelectListItemMapper.ConvertToSelectListItem(roles);

            return listRoles;
        }

        private List<SelectListItem> GetSelectListItemCities(int? countryId)
        {
            IEnumerable<City> cities = null;

            if (countryId != null)
            {
                cities = cityDomainService.GetAllActiveCitiesOfCountry(countryId.Value);
            }

            List<SelectListItem> listCities = presentationSelectListItemMapper.ConvertToSelectListItem(cities);

            return listCities;
        }
    }
}