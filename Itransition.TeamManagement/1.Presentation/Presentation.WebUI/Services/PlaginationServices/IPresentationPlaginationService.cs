﻿using Itransition.TeamManagement.Presentation.WebUI.ViewModels.CityViewModels;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.CountryViewModels;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.UserViewModels;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.PlaginationServices
{
    public interface IPresentationPlaginationService
    {
        UserPlaginationViewModel GetUserPlaginationModel(int page, int pageSize, string search, string requestUrlPathAndQuery, int companyId);

        CityPlaginationViewModel GetCityPlaginationModel(int page, int pageSize, int companyId);

        CountryPlaginationViewModel GetCountryPlaginationModel(int page, int pageSize, int companyId);
    }
}