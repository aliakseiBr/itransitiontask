﻿using System.Collections.Generic;
using System.Linq;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Filtering;
using Itransition.TeamManagement.Domain.Core.Sorting;
using Itransition.TeamManagement.Domain.Services.IServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.SortingService;
using Itransition.TeamManagement.Presentation.WebUI.UIMapper;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.CityViewModels;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.CountryViewModels;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.PlaginationViewModels;
using Itransition.TeamManagement.Presentation.WebUI.ViewModels.UserViewModels;

namespace Itransition.TeamManagement.Presentation.WebUI.Services.PlaginationServices
{
    public class PresentationPlaginationService : IPresentationPlaginationService
    {
        private IUserDomainService userDomainService;
        private ICountryDomainService countryDomainService;
        private ICityDomainService cityDomainService;
        private IPresentationSortingService sortingService;
        private IPresentationCountryMapper presentationCountryMapper;
        private IPresentationCityMapper presentationCityMapper;
        private IPresentationUserMapper presentationUserMapper;

        public PresentationPlaginationService(
            IUserDomainService userDomainService,
            ICountryDomainService countryDomainService,
            IPresentationSortingService sortingService,
            ICityDomainService cityDomainService,
            IPresentationCountryMapper presentationCountryMapper,
            IPresentationCityMapper presentationCityMapper,
            IPresentationUserMapper presentationUserMapper)
        {
            this.userDomainService = userDomainService;
            this.countryDomainService = countryDomainService;
            this.cityDomainService = cityDomainService;
            this.sortingService = sortingService;
            this.presentationCityMapper = presentationCityMapper;
            this.presentationCountryMapper = presentationCountryMapper;
            this.presentationUserMapper = presentationUserMapper;
        }

        public CountryPlaginationViewModel GetCountryPlaginationModel(int page, int pageSize, int companyId)
        {
            List<Country> domainCountries = sortingService.SortingAndFilteringCountryByCompany(companyId, new SortingAndFilteringParams(page, pageSize)).ToList();
            var countries = new List<CountryMainViewModel>();

            foreach (var item in domainCountries)
            {
                countries.Add(presentationCountryMapper.ConvertToCountryMainModel(item));
            }

            int totalItems = countryDomainService.GetActiveCountryCountByCompany(companyId);
            PageInfoViewModel pageInfo = new PageInfoViewModel { CurrentPage = page, PageSize = pageSize, TotalItems = totalItems };
            CountryPlaginationViewModel model = new CountryPlaginationViewModel { PageInfo = pageInfo, Countries = countries };

            return model;
        }

        public CityPlaginationViewModel GetCityPlaginationModel(int page, int pageSize, int companyId)
        {
            List<City> domainCities = sortingService.SortingAndFilteringCityByCompany(companyId, new SortingAndFilteringParams(page, pageSize)).ToList();
            var cities = new List<CityMainViewModel>();

            foreach (var item in domainCities)
            {
                cities.Add(presentationCityMapper.ConvertToCityMainModel(item));
            }

            int totalItems = cityDomainService.GetActiveCitiesCountByCompany(companyId);
            var pageInfo = new PageInfoViewModel { CurrentPage = page, PageSize = pageSize, TotalItems = totalItems };
            var model = new CityPlaginationViewModel { PageInfo = pageInfo, Cities = cities };

            return model;
        }

        public UserPlaginationViewModel GetUserPlaginationModel(int page, int pageSize, string search, string requestUrlPathAndQuery, int companyId)
        {
            var usersPerPages = new List<UserMainViewModel>();
            SortedList<int, SortingFieldAndType> sortingKeys = sortingService.GetCurrentSortingDictionary(requestUrlPathAndQuery);
            SortedList<int, FilteringParams> filteringParams = sortingService.GetUserTableFilteringParams(search);
            IEnumerable<User> users = GetUsers(companyId, sortingKeys, filteringParams, page, pageSize);

            foreach (var item in users)
            {
                usersPerPages.Add(presentationUserMapper.ConvertToUserViewModel(item));
            }

            int totalItems = search != null ? userDomainService.GetActiveFilteringUsersCountByCompany(filteringParams, companyId) : userDomainService.GetActiveUsersCountByCompany(companyId);
            PageInfoViewModel pageInfo = pageInfo = new PageInfoViewModel { CurrentPage = page, PageSize = pageSize, TotalItems = totalItems };
            UserPlaginationViewModel model = new UserPlaginationViewModel { PageInfo = pageInfo, Users = usersPerPages, СurrentSorting = sortingKeys, Filter = search };

            return model;
        }

        private IEnumerable<User> GetUsers(int companyId, SortedList<int, SortingFieldAndType> sortingKeys, SortedList<int, FilteringParams> filteringParams, int page, int pageSize)
        {
            SortedList<int, SortingParams> sortingParams = sortingService.GetUserTableSortingParams(sortingKeys);
            IEnumerable<User> users = sortingService.SortingAndFilteringUserByCompany(companyId, new SortingAndFilteringParams(sortingParams, filteringParams, page, pageSize));

            return users;
        }
    }
}