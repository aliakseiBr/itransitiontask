﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.ICrossCutting;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using Itransition.TeamManagement.Domain.Services.IServices;
using Itransition.TeamManagement.Domain.Services.Services;
using Itransition.TeamManagement.Infrastructure.CrossCutting;
using Itransition.TeamManagement.Infrastructure.Data.Context;
using Itransition.TeamManagement.Infrastructure.Data.Repositories;
using Itransition.TeamManagement.Presentation.WebUI.Services.AccountServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.CityServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.CountryServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.EmailServise;
using Itransition.TeamManagement.Presentation.WebUI.Services.PlaginationServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.SecurityService;
using Itransition.TeamManagement.Presentation.WebUI.Services.SortingService;
using Itransition.TeamManagement.Presentation.WebUI.Services.TaskBoardServices;
using Itransition.TeamManagement.Presentation.WebUI.Services.UserServices;
using Itransition.TeamManagement.Presentation.WebUI.UIMapper;

namespace Itransition.TeamManagement.Presentation.WebUI.App_Start
{
    public class AutofacConfig
    {
        public static void ConfigureContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            ConfigureInfrastructureServices(builder);
            ConfigureDomainServices(builder);
            ConfigurePresentationServices(builder);
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        private static bool ConfigureInfrastructureServices(ContainerBuilder builder)
        {
            builder.RegisterType<AppDbContext>().As<ICoreDbContext>().InstancePerDependency();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterType<ExpressionGenerator<User>>().As<IExpressionGenerator<User>>();
            builder.RegisterType<ExpressionGenerator<City>>().As<IExpressionGenerator<City>>();
            builder.RegisterType<ExpressionGenerator<Country>>().As<IExpressionGenerator<Country>>();

            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<CityRepository>().As<ICityRepository>();
            builder.RegisterType<CountryRepository>().As<ICountryRepository>();
            builder.RegisterType<TitleRepository>().As<ITitleRepository>();
            builder.RegisterType<CompanyRepository>().As<ICompanyRepository>();
            builder.RegisterType<RoleRepository>().As<IRoleRepository>();
            builder.RegisterType<CommentRepository>().As<ICommentRepository>();
            builder.RegisterType<WorkLogRepository>().As<IWorkLogRepository>();
            builder.RegisterType<WorkTaskRepository>().As<IWorkTaskRepository>();

            return true;
        }

        private static bool ConfigureDomainServices(ContainerBuilder builder)
        {
            builder.RegisterType<CountryDomainService>().As<ICountryDomainService>();
            builder.RegisterType<CityDomainService>().As<ICityDomainService>();
            builder.RegisterType<UserDomainService>().As<IUserDomainService>();
            builder.RegisterType<RoleDomainService>().As<IRoleDomainService>();
            builder.RegisterType<CompanyDomainService>().As<ICompanyDomainService>();
            builder.RegisterType<SecurityDomainService>().As<ISecurityDomainService>();
            builder.RegisterType<WorkLogDomainService>().As<IWorkLogDomainService>();
            builder.RegisterType<WorkTaskDomainService>().As<IWorkTaskDomainService>();

            return true;
        }

        private static bool ConfigurePresentationServices(ContainerBuilder builder)
        {
            builder.RegisterType<PresentationPlaginationService>().As<IPresentationPlaginationService>();
            builder.RegisterType<PresentationSortingService>().As<IPresentationSortingService>();
            builder.RegisterType<PresentationAuthenticationService>().As<IPresentationAuthenticationService>();
            builder.RegisterType<PresentationCityMapper>().As<IPresentationCityMapper>().InstancePerRequest();
            builder.RegisterType<PresentationCountryMapper>().As<IPresentationCountryMapper>().InstancePerRequest();
            builder.RegisterType<PresentationUserMapper>().As<IPresentationUserMapper>().InstancePerRequest();
            builder.RegisterType<PresentationCityService>().As<IPresentationCityService>();
            builder.RegisterType<PresentationCountryService>().As<IPresentationCountryService>();
            builder.RegisterType<PresentationUserService>().As<IPresentationUserService>();
            builder.RegisterType<PresentationAccountService>().As<IPresentationAccountService>();
            builder.RegisterType<PresentationEmailSenderService>().As<IPresentationEmailSenderService>();
            builder.RegisterType<PresentationSelectListItemMapper>().As<IPresentationSelectListItemMapper>();
            builder.RegisterType<PresentationWorkTaskService>().As<IPresentationWorkTaskService>();
            builder.RegisterType<PresentationWorkTaskMapper>().As<IPresentationWorkTaskMapper>();

            return true;
        }
    }
}