﻿using System.Data.Entity;
using Itransition.TeamManagement.Domain.Services.IRepositories;

namespace Itransition.TeamManagement.Infrastructure.Data.Context
{
    public class UnitOfWork : IUnitOfWork
    {
        private ICoreDbContext db;

        public UnitOfWork(ICoreDbContext db)
        {
            this.db = db;
        }

        public DbSet<TEntity> Set<TEntity>()
            where TEntity : class
        {
            return db.Set<TEntity>();
        }

        public int SaveChanges()
        {
            return db.SaveChanges();
        }
    }
}