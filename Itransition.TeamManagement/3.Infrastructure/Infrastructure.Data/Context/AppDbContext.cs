﻿using System.Data.Entity;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using Itransition.TeamManagement.Infrastructure.Data.EFConfiguration;

namespace Itransition.TeamManagement.Infrastructure.Data.Context
{
    public class AppDbContext : DbContext, ICoreDbContext
    {
        public AppDbContext()
            : base("connectionString")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.AddFromAssembly(GetType().Assembly);
        }
    }
}
