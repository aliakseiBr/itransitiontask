﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IRepositories;

namespace Itransition.TeamManagement.Infrastructure.Data.Context
{
    public class TitleRepository : AbstractRepository<Title>, ITitleRepository
    {
        public TitleRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public new IEnumerable<Title> GetAll()
        {
            return base.GetAll();
        }
    }
}
