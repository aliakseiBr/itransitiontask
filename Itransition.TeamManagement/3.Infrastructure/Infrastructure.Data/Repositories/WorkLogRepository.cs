﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IRepositories;

namespace Itransition.TeamManagement.Infrastructure.Data.Context
{
    public class WorkLogRepository : AbstractRepository<WorkLog>, IWorkLogRepository
    {
        public WorkLogRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<WorkLog> GetByPeriod(int taskId, DateTime startPeriod, DateTime endPeriod)
        {
            return GetItems()
               .Where(x => x.WorkTask.Id == taskId && x.LogDay >= startPeriod && x.LogDay <= endPeriod);
        }

        public IEnumerable<WorkLog> GetByPeriodWithTask(int userId, DateTime startPeriod, DateTime endPeriod)
        {
            return GetItems()
                .Where(x => x.WorkTask.UserId == userId && x.LogDay >= startPeriod && x.LogDay <= endPeriod)
                .Include(x => x.WorkTask);
        }

        public bool LogHoursLimit(DateTime day, double timeSpent, double timeLimit)
        {
            double sum = GetItems()
               .Where(x => x.LogDay == day)
               .Select(x => x.Time)
               .DefaultIfEmpty(0)
               .Sum(x => x);

            return (sum + timeSpent) > timeLimit;
        }
    }
}
