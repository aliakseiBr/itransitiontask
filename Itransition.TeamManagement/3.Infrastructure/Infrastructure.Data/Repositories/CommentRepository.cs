﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Itransition.TeamManagement.Infrastructure.Data.Context
{
    public class CommentRepository : AbstractRepository<Comment>, ICommentRepository
    {
        public CommentRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<Comment> GetAll(int userId)
        {
            return GetItems()
                .Where(x => x.User.Id == userId);
        }
    }
}
