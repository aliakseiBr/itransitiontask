﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Sorting;
using Itransition.TeamManagement.Domain.Services.IRepositories;

namespace Itransition.TeamManagement.Infrastructure.Data.Context
{
    public class CityRepository : AbstractRepository<City>, ICityRepository
    {
        public CityRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<City> GetActiveByCompany(
            int companyId,
            int page,
            int pageSize,
            SortedList<int, ExpressionSortingParams<City>> sortingParams = null,
            Expression<Func<City, bool>> filter = null)
        {
            IQueryable<City> items = GetActiveByCompany(companyId);

            if (filter != null)
            {
                items = FilterItems(items, filter);
            }

            if (sortingParams != null)
            {
                items = SortItems(items, sortingParams);
            }
            else
            {
                items = items.OrderBy(x => x.CreateDate);
            }

            return IncludeItems(items)
                .Skip(page)
                .Take(pageSize);
        }

        public bool DependsOnDeletedItems(City city)
        {
            return GetItems()
                .Any(x => x.Id == city.Id && x.Users.Any(s => s.IsDeleted));
        }

        public bool DependsOnActiveItems(City city)
        {
            return GetItems()
                .Any(x => x.Id == city.Id && x.Users.Any(s => !s.IsDeleted));
        }

        public IEnumerable<City> GetAllActiveByCountry(Country country)
        {
            return GetActive()
                .Where(x => x.CountryId == country.Id);
        }

        public int GetActiveCitiesCountByCountry(Country country)
        {
            return GetActive()
                .Where(x => x.CountryId == country.Id)
                .Count();
        }

        public int GetActiveCitiesCountByCompany(int companyId)
        {
            return GetActiveByCompany(companyId).Count();
        }

        public bool CheckContainsCityName(string cityName, int countryId)
        {
            return GetActive()
                .Any(x => x.CountryId == countryId && x.CityName == cityName);
        }

        private IQueryable<City> GetActiveByCompany(int companyId)
        {
            return GetItems()
                .Where(x => !x.IsDeleted && x.Country.CompanyId == companyId);
        }

        private IQueryable<City> GetActive()
        {
            return GetItems()
                .Where(x => !x.IsDeleted);
        }

        private IQueryable<City> GetDeleted()
        {
            return GetItems()
                .Where(x => x.IsDeleted);
        }

        private IQueryable<City> IncludeItems(IQueryable<City> cities)
        {
            return cities.Include(x => x.Country);
        }
    }
}
