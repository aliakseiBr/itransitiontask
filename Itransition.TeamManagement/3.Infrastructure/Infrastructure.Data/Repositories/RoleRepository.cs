﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Security;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using Itransition.TeamManagement.Infrastructure.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Infrastructure.Data.Repositories
{
    public class RoleRepository : AbstractRepository<Role>, IRoleRepository
    {
        public RoleRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<Role> GetAllRoles()
        {
            return GetAll().AsEnumerable<Role>();
        }

        public Role GetByName(string roleName)
        {
            return GetItems()
                .Where(x => x.RoleName == roleName)
                .SingleOrDefault();
        }

        public bool IsCompanyOwner(int userId)
        {
            bool result = GetItems()
                .Any(x => x.Users.Any(z => z.Id == userId) && x.RoleName == DomainSecurityConstants.CompanyOwner);

            return result;
        }
    }
}
