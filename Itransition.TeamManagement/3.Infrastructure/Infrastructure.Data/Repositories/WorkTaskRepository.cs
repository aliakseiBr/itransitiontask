﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IRepositories;

namespace Itransition.TeamManagement.Infrastructure.Data.Context
{
    public class WorkTaskRepository : AbstractRepository<WorkTask>, IWorkTaskRepository
    {
        public WorkTaskRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<WorkTask> GetActiveWithoutLogsBeforePeriod(int userId, DateTime startPeriod, DateTime endPeriod)
        {
            return GetItems()
                .Where(x => x.UserId == userId && x.CloseDate == null)
                .Where(x => !x.WorkLogs.All(y => y.LogDay > startPeriod && y.LogDay < endPeriod) || x.WorkLogs.Count == 0);
        }

        public IEnumerable<WorkTask> GetByPeriod(int userId, DateTime startPeriod, DateTime endPeriod)
        {
            return GetItems()
                .Where(x => x.CreateDate >= startPeriod && x.CreateDate < endPeriod && x.UserId == userId);
        }
    }
}
