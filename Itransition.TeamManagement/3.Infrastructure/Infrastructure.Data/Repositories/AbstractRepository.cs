﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Itransition.TeamManagement.Domain.Core.Sorting;
using Itransition.TeamManagement.Domain.Services.IRepositories;

namespace Itransition.TeamManagement.Infrastructure.Data.Context
{
    public class AbstractRepository<T>
        where T : class
    {
        private IUnitOfWork unitOfWork;

        public AbstractRepository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public T Get(object id)
        {
            return DbSet().Find(id);
        }

        public virtual int Count()
        {
            return DbSet().Count();
        }

        public virtual bool Create(T item)
        {
            if (item != null)
            {
                DbSet().Add(item);

                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual bool Delete(T item)
        {
            if (item != null)
            {
                DbSet().Remove(item);

                return true;
            }
            else
            {
                return false;
            }
        }

        protected virtual IQueryable<T> GetAll()
        {
            return DbSet();
        }

        protected virtual IQueryable<T> FilterItems(IQueryable<T> items, Expression<Func<T, bool>> filter)
        {
            return items.Where(filter);
        }

        protected virtual IQueryable<T> SortItems(IQueryable<T> items, SortedList<int, ExpressionSortingParams<T>> sortingParams)
        {
            IOrderedQueryable<T> sortItems = FirstSorting(items, sortingParams);
            sortItems = SecondSorting(sortItems, sortingParams);

            return sortItems;
        }

        protected virtual IQueryable<T> GetItems()
        {
            return DbSet().AsQueryable();
        }

        private IOrderedQueryable<T> FirstSorting(IQueryable<T> items, SortedList<int, ExpressionSortingParams<T>> sortKeys)
        {
            IOrderedQueryable<T> sortItems = null;

            if (sortKeys.First().Value.DescendingSort)
            {
                sortItems = items.OrderByDescending(sortKeys.First().Value.ExpressionSortFild);
            }
            else
            {
                sortItems = items.OrderBy(sortKeys.First().Value.ExpressionSortFild);
            }

            return sortItems;
        }

        private IOrderedQueryable<T> SecondSorting(IOrderedQueryable<T> items, SortedList<int, ExpressionSortingParams<T>> sortKeys)
        {
            foreach (var item in sortKeys.Skip(1))
            {
                if (item.Value.DescendingSort)
                {
                    items = items.ThenByDescending(item.Value.ExpressionSortFild);
                }
                else
                {
                    items = items.ThenBy(item.Value.ExpressionSortFild);
                }
            }

            return items;
        }

        private DbSet<T> DbSet()
        {
            return unitOfWork.Set<T>();
        }
    }
}
