﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Sorting;
using Itransition.TeamManagement.Domain.Services.IRepositories;

namespace Itransition.TeamManagement.Infrastructure.Data.Context
{
    public class UserRepository : AbstractRepository<User>, IUserRepository
    {
        public UserRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public User GetWithRoles(int id)
        {
            var user = GetActive()
                .Where(x => x.Id == id)
                .Include(x => x.Roles);

            return IncludeItems(user).SingleOrDefault();
        }

        public User GetByEmail(string userEmail)
        {
            var user = GetActive()
                .Where(x => x.Email == userEmail);

            return IncludeItems(user).SingleOrDefault();
        }

        public IEnumerable<User> GetActiveByCompany(
            int companyId,
            int page,
            int pageSize,
            SortedList<int, ExpressionSortingParams<User>> sortingParams = null,
            Expression<Func<User, bool>> filter = null)
        {
            IQueryable<User> items = GetActiveByCompany(companyId);

            if (filter != null)
            {
                items = FilterItems(items, filter);
            }

            if (sortingParams != null)
            {
                items = SortItems(items, sortingParams);
            }
            else
            {
                items = items.OrderBy(x => x.CreateDate);
            }

            return IncludeItems(items)
                .Include(x => x.Roles)
                .Skip(page)
                .Take(pageSize);
        }

        public User GetByEmailWithRoles(string userEmail)
        {
            var users = GetActive()
                .Where(x => x.Email == userEmail)
                .Include(x => x.Roles);

            return IncludeItems(users).SingleOrDefault();
        }

        public int GetActiveUsersCount()
        {
            return GetActive().Count();
        }

        public int GetActiveUsersCountByCompany(int companyId)
        {
            return GetActiveByCompany(companyId).Count();
        }

        public int GetActiveFilteringUsersCountByCompany(Expression<Func<User, bool>> filter, int companyId)
        {
            IQueryable<User> items = GetActiveByCompany(companyId);

            return FilterItems(items, filter).Count();
        }

        public bool UniqueEmail(string email)
        {
            return !GetActive()
                .Any(x => x.Email == email);
        }

        private IQueryable<User> GetActive()
        {
            return GetItems()
                .Where(x => !x.IsDeleted);
        }

        private IQueryable<User> GetActiveByCompany(int companyId)
        {
            return GetItems()
                .Where(x => !x.IsDeleted && x.CompanyId == companyId);
        }

        private IQueryable<User> GetDeleted()
        {
            return GetItems()
                .Where(x => x.IsDeleted);
        }

        private IQueryable<User> IncludeItems(IQueryable<User> users)
        {
            return users
                .Include(x => x.City.Country)
                .Include(x => x.Company)
                .Include(x => x.Title);
        }
    }
}
