﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Sorting;
using Itransition.TeamManagement.Domain.Services.IRepositories;

namespace Itransition.TeamManagement.Infrastructure.Data.Context
{
    public class CountryRepository : AbstractRepository<Country>, ICountryRepository
    {
        public CountryRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<Country> GetActiveByCompany(int companyId)
        {
            IQueryable<Country> countries = GetActiveQueryableByCompany(companyId);

            return IncludeItems(countries);
        }

        public IEnumerable<Country> GetActiveByCompany(
           int companyId,
           int page,
           int pageSize,
           SortedList<int, ExpressionSortingParams<Country>> sortingParams = null,
           Expression<Func<Country, bool>> filter = null)
        {
            IQueryable<Country> items = GetActiveQueryableByCompany(companyId);

            if (filter != null)
            {
                items = FilterItems(items, filter);
            }

            if (sortingParams != null)
            {
                items = SortItems(items, sortingParams);
            }
            else
            {
                items = items.OrderBy(x => x.CreateDate);
            }

            return IncludeItems(items)
                .Skip(page)
                .Take(pageSize);
        }

        public IEnumerable<Country> GetActiveWithoutEmptyCitiesByCompany(int companyId)
        {
            IQueryable<Country> countries = GetActiveQueryableByCompany(companyId)
                .Where(x => x.Cities.Count() > 0);

            return IncludeItems(countries);
        }

        public bool DependsOnDeletedItems(Country country)
        {
            IQueryable<Country> countries = GetItems()
                .Where(x => x.Id == country.Id);

            bool countainsCities = countries.Any(c => c.Cities.Any(s => s.IsDeleted));

            bool countainsUsers = countries.Any(c => c.Cities.Any(s => s.Users.Any(q => q.IsDeleted)));

            return countainsCities | countainsUsers;
        }

        public bool DependsOnActiveItems(Country country)
        {
            IQueryable<Country> countries = GetItems()
                .Where(x => x.Id == country.Id);

            return countries.Any(c => c.Cities.Any(s => s.Users.Any(q => !q.IsDeleted)));
        }

        public bool CheckContainsCountryName(string countryName)
        {
            return GetActive()
                .Any(x => x.CountryName == countryName);
        }

        public int GetActiveCountryCountByCompany(int companyId)
        {
            return GetActiveQueryableByCompany(companyId).Count();
        }

        private IQueryable<Country> GetActiveQueryableByCompany(int companyId)
        {
            return GetItems()
                .Where(x => !x.IsDeleted && x.CompanyId == companyId);
        }

        private IQueryable<Country> GetActive()
        {
            return GetItems()
                .Where(x => !x.IsDeleted);
        }

        private IQueryable<Country> IncludeItems(IQueryable<Country> countries)
        {
            return countries.Include(x => x.Company);
        }
    }
}
