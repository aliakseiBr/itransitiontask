﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using Itransition.TeamManagement.Infrastructure.Data.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Itransition.TeamManagement.Infrastructure.Data.Repositories
{
    public class CompanyRepository : AbstractRepository<Company>, ICompanyRepository
    {
        public CompanyRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IEnumerable<Company> GetAllActive()
        {
            return GetActive();
        }

        private IQueryable<Company> GetActive()
        {
            return GetItems()
                .Where(x => !x.IsDeleted);
        }

        private IQueryable<Company> GetDeleted()
        {
            return GetItems()
                .Where(x => x.IsDeleted);
        }
    }
}
