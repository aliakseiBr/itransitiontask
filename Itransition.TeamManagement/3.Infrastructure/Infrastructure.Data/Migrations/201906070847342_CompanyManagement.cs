using System;
using System.Data.Entity.Migrations;

namespace Itransition.TeamManagement.Infrastructure.Data.Migrations
{
    public partial class CompanyManagement : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        DeletionStatus = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleName = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.RoleUser",
                c => new
                    {
                        RoleRefId = c.Int(nullable: false),
                        UserRefId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RoleRefId, t.UserRefId })
                .ForeignKey("dbo.Roles", t => t.RoleRefId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserRefId, cascadeDelete: true)
                .Index(t => t.RoleRefId)
                .Index(t => t.UserRefId);

            AddColumn("dbo.Users", "CompanyId", c => c.Int(nullable: false));
            AddColumn("dbo.UserCountries", "CompanyId", c => c.Int(nullable: false));
            CreateIndex("dbo.Users", "CompanyId");
            CreateIndex("dbo.UserCountries", "CompanyId");
            AddForeignKey("dbo.UserCountries", "CompanyId", "dbo.Companies", "Id");
            AddForeignKey("dbo.Users", "CompanyId", "dbo.Companies", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.RoleUser", "UserRefId", "dbo.Users");
            DropForeignKey("dbo.RoleUser", "RoleRefId", "dbo.Roles");
            DropForeignKey("dbo.Users", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.UserCountries", "CompanyId", "dbo.Companies");
            DropIndex("dbo.RoleUser", new[] { "UserRefId" });
            DropIndex("dbo.RoleUser", new[] { "RoleRefId" });
            DropIndex("dbo.UserCountries", new[] { "CompanyId" });
            DropIndex("dbo.Users", new[] { "CompanyId" });
            DropColumn("dbo.UserCountries", "CompanyId");
            DropColumn("dbo.Users", "CompanyId");
            DropTable("dbo.RoleUser");
            DropTable("dbo.Roles");
            DropTable("dbo.Companies");
        }
    }
}
