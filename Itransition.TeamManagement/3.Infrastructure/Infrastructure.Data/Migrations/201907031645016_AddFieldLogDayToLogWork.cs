using System;
using System.Data.Entity.Migrations;

namespace Itransition.TeamManagement.Infrastructure.Data.Migrations
{
    public partial class AddFieldLogDayToLogWork : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkLogs", "LogDay", c => c.DateTime(nullable: false));
        }

        public override void Down()
        {
            DropColumn("dbo.WorkLogs", "LogDay");
        }
    }
}
