using System;
using System.Data.Entity.Migrations;

namespace Itransition.TeamManagement.Infrastructure.Data.Migrations
{
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            Sql("ALTER TABLE dbo.Users ADD FullName AS FirstName + ' ' + LastName");
        }

        public override void Down()
        {
            DropTable("dbo.Users");
        }
    }
}
