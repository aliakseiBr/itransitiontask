using System.Data.Entity.Migrations;

namespace Itransition.TeamManagement.Infrastructure.Data.Migrations
{
    public partial class WorkTaskField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkTasks", "Description", c => c.String());
        }

        public override void Down()
        {
            DropColumn("dbo.WorkTasks", "Description");
        }
    }
}
