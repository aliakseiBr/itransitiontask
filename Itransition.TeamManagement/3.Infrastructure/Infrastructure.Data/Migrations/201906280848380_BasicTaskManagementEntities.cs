using System;
using System.Data.Entity.Migrations;

namespace Itransition.TeamManagement.Infrastructure.Data.Migrations
{
    public partial class BasicTaskManagementEntities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WorkLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Time = c.Double(nullable: false),
                        Description = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        WorkTaskId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WorkTasks", t => t.WorkTaskId, cascadeDelete: true)
                .Index(t => t.WorkTaskId);

            CreateTable(
                "dbo.WorkTasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(),
                        CloseDate = c.DateTime(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
        }

        public override void Down()
        {
            DropForeignKey("dbo.WorkLogs", "WorkTaskId", "dbo.WorkTasks");
            DropForeignKey("dbo.WorkTasks", "UserId", "dbo.Users");
            DropIndex("dbo.WorkTasks", new[] { "UserId" });
            DropIndex("dbo.WorkLogs", new[] { "WorkTaskId" });
            DropTable("dbo.WorkTasks");
            DropTable("dbo.WorkLogs");
        }
    }
}
