using System;
using System.Data.Entity.Migrations;

namespace Itransition.TeamManagement.Infrastructure.Data.Migrations
{
    public partial class RenameCommentId : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.UserComments", name: "CommentId", newName: "UserId");
            RenameIndex(table: "dbo.UserComments", name: "IX_CommentId", newName: "IX_UserId");
        }

        public override void Down()
        {
            RenameIndex(table: "dbo.UserComments", name: "IX_UserId", newName: "IX_CommentId");
            RenameColumn(table: "dbo.UserComments", name: "UserId", newName: "CommentId");
        }
    }
}
