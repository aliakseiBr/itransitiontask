using System;
using System.Data.Entity.Migrations;

namespace Itransition.TeamManagement.Infrastructure.Data.Migrations
{
    public partial class RenameFieldsToIsDeleted : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserCities", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserCountries", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Companies", "IsDeleted", c => c.Boolean(nullable: false));
            DropColumn("dbo.Users", "DeletionStatus");
            DropColumn("dbo.UserCities", "DeletionStatus");
            DropColumn("dbo.UserCountries", "DeletionStatus");
            DropColumn("dbo.Companies", "DeletionStatus");
        }

        public override void Down()
        {
            AddColumn("dbo.Companies", "DeletionStatus", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserCountries", "DeletionStatus", c => c.Boolean(nullable: false));
            AddColumn("dbo.UserCities", "DeletionStatus", c => c.Boolean(nullable: false));
            AddColumn("dbo.Users", "DeletionStatus", c => c.Boolean(nullable: false));
            DropColumn("dbo.Companies", "IsDeleted");
            DropColumn("dbo.UserCountries", "IsDeleted");
            DropColumn("dbo.UserCities", "IsDeleted");
            DropColumn("dbo.Users", "IsDeleted");
        }
    }
}
