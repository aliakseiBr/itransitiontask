using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Infrastructure.Data.Context;

namespace Itransition.TeamManagement.Infrastructure.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<AppDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Itransition.TeamManagement.Infrastructure.Data.Context.AppDbContext";
        }

        protected override void Seed(AppDbContext context)
        {
            SetRoles(context);
            SetTitles(context);
            SetCompanyItransitionWithCountries(context);
            SetItransitionCompanyOwner(context);
            SetMyCompanyWithCountries(context);
            SetMyCompanyCompanyOwner(context);
            SetTasksWithLogs(context);
        }

        private void SetRoles(AppDbContext context)
        {
            var roleContext = context.Set<Role>();
            var role1 = new Role { RoleName = "User", CreateDate = DateTime.Now, UpdateDate = DateTime.Now };
            var role2 = new Role { RoleName = "CompanyOwner", CreateDate = DateTime.Now, UpdateDate = DateTime.Now };
            roleContext.Add(role1);
            roleContext.Add(role2);
            context.SaveChanges();
        }

        private void SetTitles(AppDbContext context)
        {
            var titleContext = context.Set<Title>();
            var title1 = new Title() { TitleName = "male", CreateDate = DateTime.Now, UpdateDate = DateTime.Now };
            var title2 = new Title() { TitleName = "female", CreateDate = DateTime.Now, UpdateDate = DateTime.Now };
            var title3 = new Title() { TitleName = "shemale", CreateDate = DateTime.Now, UpdateDate = DateTime.Now };
            titleContext.Add(title1);
            titleContext.Add(title2);
            titleContext.Add(title3);
            context.SaveChanges();
        }

        private void SetCompanyItransitionWithCountries(AppDbContext context)
        {
            List<City> cities = new List<City>()
            {
                new City() { CityName = "Minsk", CreateDate = DateTime.Now, UpdateDate = DateTime.Now },
                new City() { CityName = "Brest", CreateDate = DateTime.Now, UpdateDate = DateTime.Now },
                new City() { CityName = "Vitebsk", CreateDate = DateTime.Now, UpdateDate = DateTime.Now },
                new City() { CityName = "Gomel", CreateDate = DateTime.Now, UpdateDate = DateTime.Now },
                new City() { CityName = "Mogilyov", CreateDate = DateTime.Now, UpdateDate = DateTime.Now },
                new City() { CityName = "Grodno", CreateDate = DateTime.Now, UpdateDate = DateTime.Now },
            };

            var company = new Company() { CompanyName = "Itransition", CreateDate = DateTime.Now, UpdateDate = DateTime.Now };

            var countryContext = context.Set<Country>();
            var country = new Country()
            {
                CountryName = "Belarus",
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                Company = company,
                Cities = cities
            };
            countryContext.Add(country);
            context.SaveChanges();
        }

        private void SetItransitionCompanyOwner(AppDbContext context)
        {
            var company = context.Set<Company>().Where(x => x.CompanyName == "Itransition").Include(x => x.Countries).FirstOrDefault();
            var role = context.Set<Role>().Where(x => x.RoleName == "CompanyOwner").FirstOrDefault();
            var country = company.Countries.FirstOrDefault();
            var city = context.Set<City>().Where(x => x.CountryId == country.Id).FirstOrDefault();
            var userContext = context.Set<User>();

            var user = new User()
            {
                FirstName = "Aleksey",
                LastName = "Buryj",
                City = city,
                Email = "abs@gmail.com",
                Phone = "+375 00 000 00 00",
                Title = context.Set<Title>().FirstOrDefault(),
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                IsDeleted = false,
                Company = company,
                Password = "hRf8wNedh6j21Rac3/PsgQ==",
                PasswordSalt = "3F#Y7kPw",
                Roles = new List<Role>() { role }
            };
            userContext.Add(user);
            context.SaveChanges();
        }

        private void SetMyCompanyWithCountries(AppDbContext context)
        {
            List<City> cities = new List<City>()
            {
                new City() { CityName = "M1", CreateDate = DateTime.Now, UpdateDate = DateTime.Now },
                new City() { CityName = "B1", CreateDate = DateTime.Now, UpdateDate = DateTime.Now },
                new City() { CityName = "V1", CreateDate = DateTime.Now, UpdateDate = DateTime.Now },
                new City() { CityName = "G1", CreateDate = DateTime.Now, UpdateDate = DateTime.Now },
                new City() { CityName = "Mq1", CreateDate = DateTime.Now, UpdateDate = DateTime.Now },
                new City() { CityName = "Gr1", CreateDate = DateTime.Now, UpdateDate = DateTime.Now },
            };

            var company = new Company() { CompanyName = "MyCompany", CreateDate = DateTime.Now, UpdateDate = DateTime.Now };

            var countryContext = context.Set<Country>();
            var country = new Country()
            {
                CountryName = "MyCountry",
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                Company = company,
                Cities = cities
            };
            countryContext.Add(country);
            context.SaveChanges();
        }

        private void SetMyCompanyCompanyOwner(AppDbContext context)
        {
            var company = context.Set<Company>().Where(x => x.CompanyName == "MyCompany").Include(x => x.Countries).FirstOrDefault();
            var role = context.Set<Role>().Where(x => x.RoleName == "CompanyOwner").FirstOrDefault();
            var country = company.Countries.FirstOrDefault();
            var city = context.Set<City>().Where(x => x.CountryId == country.Id).FirstOrDefault();
            var userContext = context.Set<User>();
            var user = new User()
            {
                FirstName = "Bubu",
                LastName = "Buny",
                City = city,
                Email = "111@gmail.com",
                Phone = "+375 00 000 00 00",
                Title = context.Set<Title>().FirstOrDefault(),
                CreateDate = DateTime.Now,
                UpdateDate = DateTime.Now,
                IsDeleted = false,
                Company = company,
                Password = "hRf8wNedh6j21Rac3/PsgQ==",
                PasswordSalt = "3F#Y7kPw",
                Roles = new List<Role>() { role }
            };
            userContext.Add(user);
            context.SaveChanges();
        }

        private void SetTasksWithLogs(AppDbContext context)
        {
            var userContext = context.Set<User>();
            User user = userContext.Where(x => x.Email == "abs@gmail.com").SingleOrDefault();

            List<WorkLog> logs = new List<WorkLog>()
            {
                new WorkLog() { LogDay = DateTime.Today.AddDays(-6), CreateDate = DateTime.Now.AddDays(-6), Time = 6 },
                new WorkLog() { LogDay = DateTime.Today.AddDays(-5), CreateDate = DateTime.Now.AddDays(-5), Time = 7 },
                new WorkLog() { LogDay = DateTime.Today.AddDays(-4), CreateDate = DateTime.Now.AddDays(-4), Time = 1 }
            };

            WorkTask task = new WorkTask() { Name = "FirstTask", Description = "Description 1", CreateDate = DateTime.Now.AddDays(-6), UpdateDate = DateTime.Now };

            List<WorkLog> logs2 = new List<WorkLog>()
            {
                new WorkLog() { LogDay = DateTime.Today.AddDays(-6), CreateDate = DateTime.Now.AddDays(-6), Time = 2 },
                new WorkLog() { LogDay = DateTime.Today.AddDays(-5), CreateDate = DateTime.Now.AddDays(-5), Time = 5 },
                new WorkLog() { LogDay = DateTime.Today.AddDays(-4), CreateDate = DateTime.Now.AddDays(-4), Time = 4 }
            };

            WorkTask task2 = new WorkTask() { Name = "FirstTask2", Description = "Description 2", CreateDate = DateTime.Now.AddDays(-6), UpdateDate = DateTime.Now };

            List<WorkLog> logs3 = new List<WorkLog>()
            {
                new WorkLog() { LogDay = DateTime.Today.AddDays(-6), CreateDate = DateTime.Now.AddDays(-6), Time = 3 },
                new WorkLog() { LogDay = DateTime.Today.AddDays(-5), CreateDate = DateTime.Now.AddDays(-5), Time = 4 },
                new WorkLog() { LogDay = DateTime.Today.AddDays(-4), CreateDate = DateTime.Now.AddDays(-4), Time = 2 }
            };

            WorkTask task3 = new WorkTask() { Name = "FirstTask3", Description = "Description 3", CreateDate = DateTime.Now.AddDays(-6), UpdateDate = DateTime.Now };

            task.WorkLogs = logs;
            task2.WorkLogs = logs2;
            task3.WorkLogs = logs3;

            var tasks = new List<WorkTask> { task, task2, task3 };

            for (int i = 2; i < 20; i++)
            {
                tasks.Add(new WorkTask() { Name = "SecondTask" + i, Description = "Description" + i, CreateDate = DateTime.Now.AddDays(-6), UpdateDate = DateTime.Now });
                tasks[i].WorkLogs = new List<WorkLog>()
                {
                    new WorkLog() { LogDay = DateTime.Today.AddDays(-6), CreateDate = DateTime.Now.AddDays(-6), Time = 3 },
                    new WorkLog() { LogDay = DateTime.Today.AddDays(-5), CreateDate = DateTime.Now.AddDays(-5), Time = 4 },
                    new WorkLog() { LogDay = DateTime.Today.AddDays(-4), CreateDate = DateTime.Now.AddDays(-4), Time = 2 }
                };
            }

            user.WorkTasks = tasks;
            context.SaveChanges();
        }
    }
}
