using System;
using System.Data.Entity.Migrations;

namespace Itransition.TeamManagement.Infrastructure.Data.Migrations
{
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Phone = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        PasswordSalt = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        DeletionStatus = c.Boolean(nullable: false),
                        TitleId = c.Int(nullable: false),
                        CityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserCities", t => t.CityId)
                .ForeignKey("dbo.UserTitles", t => t.TitleId)
                .Index(t => t.TitleId)
                .Index(t => t.CityId);

            CreateTable(
                "dbo.UserCities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CityName = c.String(),
                        CountryId = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        DeletionStatus = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserCountries", t => t.CountryId, cascadeDelete: true)
                .Index(t => t.CountryId);

            CreateTable(
                "dbo.UserCountries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CountryName = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        DeletionStatus = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.UserComments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Body = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                        CommentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.CommentId)
                .Index(t => t.CommentId);

            CreateTable(
                "dbo.UserTitles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TitleName = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        UpdateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
        }

        public override void Down()
        {
            DropForeignKey("dbo.Users", "TitleId", "dbo.UserTitles");
            DropForeignKey("dbo.UserComments", "CommentId", "dbo.Users");
            DropForeignKey("dbo.Users", "CityId", "dbo.UserCities");
            DropForeignKey("dbo.UserCities", "CountryId", "dbo.UserCountries");
            DropIndex("dbo.UserComments", new[] { "CommentId" });
            DropIndex("dbo.UserCities", new[] { "CountryId" });
            DropIndex("dbo.Users", new[] { "CityId" });
            DropIndex("dbo.Users", new[] { "TitleId" });
            DropTable("dbo.UserTitles");
            DropTable("dbo.UserComments");
            DropTable("dbo.UserCountries");
            DropTable("dbo.UserCities");
            DropTable("dbo.Users");
        }
    }
}
