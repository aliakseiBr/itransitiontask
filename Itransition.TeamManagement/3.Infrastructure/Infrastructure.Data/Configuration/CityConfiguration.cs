﻿using System.Data.Entity.ModelConfiguration;
using Itransition.TeamManagement.Domain.Core.Entities;

namespace Itransition.TeamManagement.Infrastructure.Data.EFConfiguration
{
    public class CityConfiguration : EntityTypeConfiguration<City>
    {
        public CityConfiguration()
        {
            this.ToTable("UserCities");

            this.HasKey<int>(s => s.Id);

            this.Property(p => p.CityName);
            this.Property(p => p.IsDeleted);
            this.Property(p => p.CreateDate);
            this.Property(p => p.UpdateDate);

            this.HasRequired<Country>(s => s.Country)
               .WithMany(c => c.Cities)
               .HasForeignKey<int?>(s => s.CountryId)
               .WillCascadeOnDelete(true);
        }
    }
}
