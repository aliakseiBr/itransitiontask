﻿using System.Data.Entity.ModelConfiguration;
using Itransition.TeamManagement.Domain.Core.Entities;

namespace Itransition.TeamManagement.Infrastructure.Data.EFConfiguration
{
    internal class TitleConfiguration : EntityTypeConfiguration<Title>
    {
        public TitleConfiguration()
        {
            this.ToTable("UserTitles");

            this.HasKey<int>(s => s.Id);

            this.Property(p => p.TitleName);
            this.Property(p => p.CreateDate);
            this.Property(p => p.UpdateDate);
        }
    }
}
