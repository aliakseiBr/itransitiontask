﻿using System.Data.Entity.ModelConfiguration;
using Itransition.TeamManagement.Domain.Core.Entities;

namespace Itransition.TeamManagement.Infrastructure.Data.EFConfiguration
{
    public class CompanyConfiguration : EntityTypeConfiguration<Company>
    {
        public CompanyConfiguration()
        {
            this.ToTable("Companies");

            this.HasKey<int>(s => s.Id);

            this.Property(p => p.CompanyName);
            this.Property(p => p.IsDeleted);
            this.Property(p => p.CreateDate);
            this.Property(p => p.UpdateDate);
        }
    }
}
