﻿using System.Data.Entity.ModelConfiguration;
using Itransition.TeamManagement.Domain.Core.Entities;

namespace Itransition.TeamManagement.Infrastructure.Data.EFConfiguration
{
    public class WorkLogConfiguration : EntityTypeConfiguration<WorkLog>
    {
        public WorkLogConfiguration()
        {
            this.ToTable("WorkLogs");

            this.HasKey<int>(s => s.Id);

            this.Property(p => p.Time);

            this.HasRequired<WorkTask>(s => s.WorkTask)
              .WithMany(c => c.WorkLogs)
              .HasForeignKey<int?>(s => s.WorkTaskId)
              .WillCascadeOnDelete(true);
        }
    }
}
