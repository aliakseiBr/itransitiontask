﻿using System.Data.Entity.ModelConfiguration;
using Itransition.TeamManagement.Domain.Core.Entities;

namespace Itransition.TeamManagement.Infrastructure.Data.EFConfiguration
{
    public class WorkTaskConfiguration : EntityTypeConfiguration<WorkTask>
    {
        public WorkTaskConfiguration()
        {
            this.ToTable("WorkTasks");

            this.HasKey<int>(s => s.Id);

            this.Property(p => p.Name);
            this.Property(p => p.Description);
            this.Property(p => p.CreateDate);
            this.Property(p => p.UpdateDate);
            this.Property(p => p.CloseDate);

            this.HasRequired<User>(s => s.User)
              .WithMany(c => c.WorkTasks)
              .HasForeignKey<int?>(s => s.UserId)
              .WillCascadeOnDelete(true);
        }
    }
}
