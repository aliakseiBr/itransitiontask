﻿using System.Data.Entity.ModelConfiguration;
using Itransition.TeamManagement.Domain.Core.Entities;

namespace Itransition.TeamManagement.Infrastructure.Data.EFConfiguration
{
    public class RoleConfiguration : EntityTypeConfiguration<Role>
    {
        public RoleConfiguration()
        {
            this.ToTable("Roles");

            this.HasKey<int>(s => s.Id);

            this.Property(p => p.RoleName);
            this.Property(p => p.CreateDate);
            this.Property(p => p.UpdateDate);

            this.HasMany<User>(s => s.Users)
                .WithMany(c => c.Roles)
                .Map(cs =>
                {
                    cs.MapLeftKey("RoleRefId");
                    cs.MapRightKey("UserRefId");
                    cs.ToTable("RoleUser");
                });
        }
    }
}
