﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Itransition.TeamManagement.Domain.Core.Entities;

namespace Itransition.TeamManagement.Infrastructure.Data.EFConfiguration
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            this.ToTable("Users");

            this.HasKey<int>(s => s.Id);

            this.Property(p => p.FirstName)
                    .HasColumnName("FirstName");

            this.Property(p => p.LastName)
                    .HasColumnName("LastName");

            this.Property(p => p.Phone);
            this.Property(p => p.Email);
            this.Property(p => p.Password);
            this.Property(p => p.PasswordSalt);
            this.Property(p => p.IsDeleted);
            this.Property(p => p.CreateDate);
            this.Property(p => p.UpdateDate);
            this.Property(p => p.FullName)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);

            this.HasRequired<Title>(s => s.Title)
                .WithMany(c => c.Users)
                .HasForeignKey<int?>(s => s.TitleId)
                .WillCascadeOnDelete(false);
            this.HasRequired<City>(s => s.City)
                .WithMany(c => c.Users)
                .HasForeignKey<int?>(s => s.CityId)
                .WillCascadeOnDelete(false);
            this.HasRequired<Company>(s => s.Company)
               .WithMany(c => c.Users)
               .HasForeignKey<int?>(s => s.CompanyId)
               .WillCascadeOnDelete(false);
        }
    }
}
