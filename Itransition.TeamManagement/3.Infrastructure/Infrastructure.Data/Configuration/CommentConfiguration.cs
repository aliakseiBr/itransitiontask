﻿using Itransition.TeamManagement.Domain.Core.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Itransition.TeamManagement.Infrastructure.Data.EFConfiguration
{
    public class CommentConfiguration : EntityTypeConfiguration<Comment>
    {
        public CommentConfiguration()
        {
            this.ToTable("UserComments");

            this.HasKey<int>(s => s.Id);

            this.Property(p => p.CreateDate);
            this.Property(p => p.UpdateDate);
            this.Property(p => p.Body);

            this.HasRequired<User>(s => s.User)
               .WithMany(c => c.Comments)
               .HasForeignKey<int?>(s => s.UserId)
               .WillCascadeOnDelete(false);
        }
    }
}
