﻿using System.Data.Entity.ModelConfiguration;
using Itransition.TeamManagement.Domain.Core.Entities;

namespace Itransition.TeamManagement.Infrastructure.Data.EFConfiguration
{
    public class CountryConfiguration : EntityTypeConfiguration<Country>
    {
        public CountryConfiguration()
        {
            this.ToTable("UserCountries");

            this.HasKey<int>(s => s.Id);

            this.Property(p => p.CountryName);
            this.Property(p => p.IsDeleted);
            this.Property(p => p.CreateDate);
            this.Property(p => p.UpdateDate);

            this.HasRequired<Company>(s => s.Company)
              .WithMany(c => c.Countries)
              .HasForeignKey<int?>(s => s.CompanyId)
              .WillCascadeOnDelete(false);
        }
    }
}
