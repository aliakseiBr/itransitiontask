﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Filtering;
using Itransition.TeamManagement.Domain.Core.Sorting;
using Itransition.TeamManagement.Domain.Services.ICrossCutting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Infrastructure.CrossCutting
{
    public class ExpressionGenerator<T> : IExpressionGenerator<T>
        where T: class
    {
        public SortedList<int, ExpressionSortingParams<T>> GetSortDictionaryExpressions(SortedList<int, SortingParams> sortingParams)
        {
            if(sortingParams == null)
            {
                return null;
            }

            var expressionSortingParams = new SortedList<int, ExpressionSortingParams<T>>();
            var param = Expression.Parameter(typeof(T), "x");
            MemberExpression member = null;

            foreach (var item in sortingParams)
            {                
                member = Expression.Property(param, item.Value.SortField);
                if (item.Value.ChildFields != null)
                {
                    foreach (var chield in item.Value.ChildFields)
                    {
                        member = Expression.Property(member, chield);                       
                    }
                }

                var exp = new ExpressionSortingParams<T>() { ExpressionSortFild = Expression.Lambda<Func<T, string>>(member, param), DescendingSort = item.Value.DescendingSort };
                expressionSortingParams.Add(item.Key, exp);
            }

            return expressionSortingParams;
        }

        public Expression<Func<T, bool>> GetFilterExpression(SortedList<int, FilteringParams> filteringParams)
        {
            if (filteringParams == null)
            {
                return null;
            }

            ParameterExpression param = Expression.Parameter(typeof(T), "x");
            MemberExpression member = null;
            Expression comparingExpressionFull = null;
            Expression comparingExpressionPrev = default(BinaryExpression);

            foreach (var item in filteringParams)
            {
                if(item.Value.FilterString == null)
                {
                    continue;
                }

                member = CreateFilterMemberExpression(param, item);
                Expression comparingExpression = CreateFilterComparingExpression(member, item);

                if (comparingExpressionPrev == default(BinaryExpression))
                {
                    comparingExpressionPrev = comparingExpression;
                }
                else if (comparingExpressionFull != null)
                {
                    comparingExpressionFull = Expression.Or(comparingExpressionFull, comparingExpression);
                }
                else if (comparingExpressionPrev != default(BinaryExpression))
                {
                    comparingExpressionFull = Expression.Or(comparingExpressionPrev, comparingExpression);
                    comparingExpressionPrev = comparingExpression;
                }
            }

            return CreateFilterLambda(comparingExpressionFull, comparingExpressionPrev, param);
        }
        
        private MemberExpression CreateFilterMemberExpression(ParameterExpression param, KeyValuePair<int, FilteringParams> item)
        {
            MemberExpression member = Expression.Property(param, item.Value.FilterField);

            if (item.Value.ChildFields != null)
            {
                foreach (var chield in item.Value.ChildFields)
                {
                    member = Expression.Property(member, chield);
                }
            }

            return member;
        }
        
        private Expression CreateFilterComparingExpression(MemberExpression member, KeyValuePair<int, FilteringParams> item )
        {
            Type propertyType = member.Type;
            TypeConverter converter = TypeDescriptor.GetConverter(propertyType);
            var result = converter.ConvertFrom(item.Value.FilterString.ToString());
            ConstantExpression constant = Expression.Constant(result);            
            MethodInfo method = typeof(string).GetMethod("Contains", new[] { typeof(string) }); 

            return Expression.Call(member, method, constant);
        }

        private Expression<Func<T, bool>> CreateFilterLambda(Expression comparingExpressionFull, Expression comparingExpressionPrev, ParameterExpression param)
        {
            Expression<Func<T, bool>> lambda = null;

            if (comparingExpressionFull != null)
            {
                lambda = Expression.Lambda<Func<T, bool>>(comparingExpressionFull, param);
            }
            else if (comparingExpressionPrev != default(BinaryExpression))
            {
                lambda = Expression.Lambda<Func<T, bool>>(comparingExpressionPrev, param);
            }

            return lambda;
        }       
    }   
}
