﻿namespace Itransition.TeamManagement.Infrastructure.Services.Search
{
    using System.Collections.Generic;
    using Itransition.TeamManagement.Domain.Core.Entities;

    public interface ISearchService
    {
        IEnumerable<User> GetUsers(string search);
    }
}
