﻿namespace Itransition.TeamManagement.Infrastructure.Services.Managers
{
    using System.Collections.Generic;
    using Itransition.TeamManagement.Domain.Core.Entities;

    public interface ICountryManager
    {
        IEnumerable<UserCountry> GetAllCountries();

        IEnumerable<UserCity> GetAllCitiesOfCountry(int? countryId);

        UserCountry GetCountry(int countryId);

        UserCity GetCityOfCountry(int cityId, int countryId);
    }
}
