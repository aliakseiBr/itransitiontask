﻿namespace Itransition.TeamManagement.Infrastructure.Services.Managers
{
    using System.Collections.Generic;
    using Itransition.TeamManagement.Domain.Core.Entities;

    public interface IUserManager
    {
        User GetUser(int userId);

        IEnumerable<User> GetAllUsers();

        void SaveChanges();

        void CreateUser(User user);

        void DeleteUser(int id);

        IEnumerable<UserTitle> GetAllUserTitles();

        UserTitle GetUserTitle(int titleId);

        int UsersCount();
    }
}
