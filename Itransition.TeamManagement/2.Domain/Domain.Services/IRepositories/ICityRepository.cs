﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Sorting;

namespace Itransition.TeamManagement.Domain.Services.IRepositories
{
    public interface ICityRepository
    {
        bool Create(City city);

        bool Delete(City city);

        bool DependsOnDeletedItems(City city);

        bool DependsOnActiveItems(City city);

        IEnumerable<City> GetAllActiveByCountry(Country country);

        IEnumerable<City> GetActiveByCompany(
            int companyId,
            int page,
            int pageSize,
            SortedList<int, ExpressionSortingParams<City>> sortingParams = null,
            Expression<Func<City, bool>> filter = null);

        City Get(object id);

        int GetActiveCitiesCountByCountry(Country country);

        int GetActiveCitiesCountByCompany(int companyId);

        bool CheckContainsCityName(string cityName, int countryId);
    }
}
