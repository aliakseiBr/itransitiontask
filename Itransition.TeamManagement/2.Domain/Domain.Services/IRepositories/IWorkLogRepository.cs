﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Sorting;

namespace Itransition.TeamManagement.Domain.Services.IRepositories
{
    public interface IWorkLogRepository
    {
        WorkLog Get(object id);

        bool Create(WorkLog item);

        bool Delete(WorkLog item);

        IEnumerable<WorkLog> GetByPeriod(int taskId, DateTime startPeriod, DateTime endPeriod);

        IEnumerable<WorkLog> GetByPeriodWithTask(int userId, DateTime startPeriod, DateTime endPeriod);

        bool LogHoursLimit(DateTime day, double timeSpent, double timeLimit);
    }
}
