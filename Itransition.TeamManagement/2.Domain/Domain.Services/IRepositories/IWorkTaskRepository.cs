﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Sorting;

namespace Itransition.TeamManagement.Domain.Services.IRepositories
{
    public interface IWorkTaskRepository
    {
        bool Create(WorkTask item);

        bool Delete(WorkTask item);

        IEnumerable<WorkTask> GetByPeriod(int userId, DateTime startPeriod, DateTime endPeriod);

        IEnumerable<WorkTask> GetActiveWithoutLogsBeforePeriod(int userId, DateTime startPeriod, DateTime endPeriod);
    }
}
