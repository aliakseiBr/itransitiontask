﻿using System.Data.Entity;

namespace Itransition.TeamManagement.Domain.Services.IRepositories
{
    public interface IUnitOfWork
    {
        DbSet<TEntity> Set<TEntity>()
            where TEntity : class;

        int SaveChanges();
    }
}
