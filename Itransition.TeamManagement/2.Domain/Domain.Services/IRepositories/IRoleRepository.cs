﻿using Itransition.TeamManagement.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Services.IRepositories
{
    public interface IRoleRepository
    {
        Role GetByName(string roleName);

        Role Get(object id);

        bool IsCompanyOwner(int userId);

        IEnumerable<Role> GetAllRoles();
    }
}
