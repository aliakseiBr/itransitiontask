﻿using System.Data.Entity;

namespace Itransition.TeamManagement.Domain.Services.IRepositories
{
    public interface ICoreDbContext
    {
        DbSet<TEntity> Set<TEntity>()
            where TEntity : class;

        int SaveChanges();
    }
}