﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Sorting;

namespace Itransition.TeamManagement.Domain.Services.IRepositories
{
    public interface IUserRepository
    {
        bool Create(User user);

        bool Delete(User user);

        IEnumerable<User> GetActiveByCompany(
            int companyId,
            int page,
            int pageSize,
            SortedList<int, ExpressionSortingParams<User>> sortingParams = null,
            Expression<Func<User, bool>> filter = null);

        User Get(object id);

        User GetWithRoles(int id);

        User GetByEmail(string userEmail);

        User GetByEmailWithRoles(string userEmail);

        bool UniqueEmail(string email);

        int GetActiveUsersCount();

        int GetActiveUsersCountByCompany(int companyId);

        int GetActiveFilteringUsersCountByCompany(Expression<Func<User, bool>> filter, int companyId);
    }
}
