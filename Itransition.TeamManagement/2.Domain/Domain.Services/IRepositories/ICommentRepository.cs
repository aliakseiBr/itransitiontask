﻿using Itransition.TeamManagement.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Services.IRepositories
{
    public interface ICommentRepository
    {
        bool Create(Comment comment);

        bool Delete(Comment comment);

        Comment Get(object id);

        IEnumerable<Comment> GetAll(int userId);
    }
}
