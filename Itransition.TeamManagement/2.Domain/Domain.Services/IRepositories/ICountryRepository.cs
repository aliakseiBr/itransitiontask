﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Sorting;

namespace Itransition.TeamManagement.Domain.Services.IRepositories
{
    public interface ICountryRepository
    {
        bool Create(Country country);

        bool Delete(Country country);

        bool DependsOnDeletedItems(Country county);

        bool DependsOnActiveItems(Country country);

        IEnumerable<Country> GetActiveByCompany(
            int companyId,
            int page,
            int pageSize,
            SortedList<int, ExpressionSortingParams<Country>> sortingParams = null,
            Expression<Func<Country, bool>> filter = null);

        IEnumerable<Country> GetActiveWithoutEmptyCitiesByCompany(int companyId);

        IEnumerable<Country> GetActiveByCompany(int companyId);

        Country Get(object id);

        int GetActiveCountryCountByCompany(int companyId);

        bool CheckContainsCountryName(string countryName);
    }
}
