﻿using System.Collections.Generic;
using System.Linq;
using Itransition.TeamManagement.Domain.Core.Entities;

namespace Itransition.TeamManagement.Domain.Services.IRepositories
{
    public interface ITitleRepository
    {
        bool Create(Title title);

        bool Delete(Title title);

        IEnumerable<Title> GetAll();

        Title Get(object id);
    }
}
