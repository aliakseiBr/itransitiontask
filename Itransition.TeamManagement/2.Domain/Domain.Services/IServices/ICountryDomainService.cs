﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Filtering;
using Itransition.TeamManagement.Domain.Core.Sorting;

namespace Itransition.TeamManagement.Domain.Services.IServices
{
    public interface ICountryDomainService
    {
        IEnumerable<Country> GetActiveByCompany(
            int companyId,
            int page,
            int pageSize,
            SortedList<int, SortingParams> sortingParams,
            SortedList<int, FilteringParams> filteringParams);

        IEnumerable<Country> GetActiveByCompany(int companyId, int page, int pageSize);

        IEnumerable<Country> GetActiveByCompany(int companyId);

        IEnumerable<Country> GetActiveWithoutEmptyCitiesByCompany(int companyId);

        Country Get(int countryId);

        bool Create(Country country);

        bool Delete(Country country);

        int CountCitiesOfCountry(Country country);

        int GetActiveCountryCountByCompany(int companyId);

        bool CheckContainsCountryName(string countryName);
    }
}
