﻿using Itransition.TeamManagement.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Services.IServices
{
    public interface IWorkLogDomainService
    {
        WorkLog Get(int id);

        bool Create(WorkLog item);

        bool Delete(WorkLog item);

        IEnumerable<WorkLog> GetByPeriodWithTask(int userId, DateTime startPeriod, DateTime endPeriod);

        IEnumerable<WorkLog> GetByPeriod(int taskId, DateTime startPeriod, DateTime endPeriod);

        bool LogHoursLimit(DateTime day, double timeSpent, double timeLimit);
    }
}
