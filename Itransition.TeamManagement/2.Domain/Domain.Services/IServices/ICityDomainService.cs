﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Filtering;
using Itransition.TeamManagement.Domain.Core.Sorting;

namespace Itransition.TeamManagement.Domain.Services.IServices
{
    public interface ICityDomainService
    {
        IEnumerable<City> GetAllActiveCitiesOfCountry(int countryId);

        IEnumerable<City> GetActiveByCompany(
           int companyId,
           int page,
           int pageSize,
           SortedList<int, SortingParams> sortingParams,
           SortedList<int, FilteringParams> filteringParams);

        IEnumerable<City> GetActiveByCompany(int companyId, int page, int pageSize);

        City Get(int cityId);

        int GetActiveCitiesCountByCompany(int companyId);

        bool CreateCity(Country country, City city);

        bool Delete(City city);

        bool CheckContainsCityName(string cityName, int countryId);
    }
}
