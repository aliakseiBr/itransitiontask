﻿using Itransition.TeamManagement.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Services.IServices
{
    public interface IRoleDomainService
    {
        Role GetByName(string roleName);

        Role Get(object id);

        IEnumerable<Role> GetAll();

        bool IsCompanyOwner(int userId);
    }
}
