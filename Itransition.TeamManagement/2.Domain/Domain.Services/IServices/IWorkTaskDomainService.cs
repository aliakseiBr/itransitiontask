﻿using Itransition.TeamManagement.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Itransition.TeamManagement.Domain.Services.IServices
{
    public interface IWorkTaskDomainService
    {
        bool Create(WorkTask item);

        bool Delete(WorkTask item);

        IEnumerable<WorkTask> GetByPeriod(int userId, DateTime startPeriod, DateTime endPeriod);

        IEnumerable<WorkTask> GetActiveWithoutLogsBeforePeriod(int userId, DateTime startPeriod, DateTime endPeriod);
    }
}
