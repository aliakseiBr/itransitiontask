﻿using Itransition.TeamManagement.Domain.Core.Entities;

namespace Itransition.TeamManagement.Domain.Services.IServices
{
    public interface IPresentationAuthenticationService
    {
        int GetCurrentUserId();

        int GetCurrentCompanyId();

        string GetCurrentUserEmail();

        bool IsInRole(string role);

        void AddRandomPassword(User user);

        void ChangePassword(User user, string newPassword);

        User GetUser(string email, string password);

        User GetUser(int userId, string password);

        User GetUserWithRoles(string email, string password);

        User GetUserWithRoles(int userId, string password);
    }
}