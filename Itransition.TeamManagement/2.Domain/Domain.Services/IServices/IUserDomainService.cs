﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Filtering;
using Itransition.TeamManagement.Domain.Core.Sorting;

namespace Itransition.TeamManagement.Domain.Services.IServices
{
    public interface IUserDomainService
    {
        User Get(int userId);

        User GetWithRoles(int userId);

        IEnumerable<User> GetActiveByCompanyWithRoles(
            int companyId,
            int page,
            int pageSize,
            SortedList<int, SortingParams> sortingParams,
            SortedList<int, FilteringParams> filteringParams);

        bool Create(User user);

        bool Delete(User user);

        IEnumerable<Title> GetAllTitles();

        Title GetTitle(int titleId);

        int GetActiveUsersCount();

        int GetActiveUsersCountByCompany(int companyId);

        int GetActiveFilteringUsersCountByCompany(SortedList<int, FilteringParams> filteringParams, int companyId);

        bool AddComment(User user, Comment comment);

        bool DeleteComment(Comment comment);

        IEnumerable<Comment> GetAllComments(int userId);

        bool UniqueEmail(string email);
    }
}
