﻿using Itransition.TeamManagement.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Services.IServices
{
    public interface ISecurityDomainService
    {
        string AddRandomPassword(User user);

        void ChangePassword(User user, string newPassword);

        User GetUser(string email, string password);

        User GetUser(int userId, string password);

        User GetUserWithRoles(string email, string password);

        User GetUserWithRoles(int userId, string password);
    }
}
