﻿using Itransition.TeamManagement.Domain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Services.IServices
{
    public interface ICompanyDomainService
    {
        IEnumerable<Company> GetAllActive();
    }
}
