﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using Itransition.TeamManagement.Domain.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Services.Services
{
    public class CompanyDomainService : ICompanyDomainService
    {
        private ICompanyRepository companyRepository;

        public CompanyDomainService(ICompanyRepository companyRepository)
        {
            this.companyRepository = companyRepository;
        }

        public IEnumerable<Company> GetAllActive()
        {
            return companyRepository.GetAllActive();
        }
    }
}
