﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using Itransition.TeamManagement.Domain.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Services.Services
{
   public class SecurityDomainService : ISecurityDomainService
   {
        private const byte NumberSequences = 4;
        private const int PasswordLength = 8;
        private const int SaltByteLength = 8;
        private const string UpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private const string LowerCase = "abcdefghijklmnopqrstuvwxyz";
        private const string Numbers = "0123456789";
        private const string Symbols = "/+-*^%#@!";
        private static RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();
        private IUserRepository userRepository;

        public SecurityDomainService(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public User GetUser(string email, string password)
        {
            User user = userRepository.GetByEmail(email);

            if (user == null)
            {
                return null;
            }

            bool result = CheckUserPassword(user, password);

            return result ? user : null;
        }

        public User GetUser(int userId, string password)
        {
            User user = userRepository.Get(userId);

            if (user == null)
            {
                return null;
            }

            bool result = CheckUserPassword(user, password);

            return result ? user : null;
        }

        public User GetUserWithRoles(string email, string password)
        {
            User user = userRepository.GetByEmailWithRoles(email);

            if (user == null)
            {
                return null;
            }

            bool result = CheckUserPassword(user, password);

            return result ? user : null;
        }

        public User GetUserWithRoles(int userId, string password)
        {
            User user = userRepository.GetWithRoles(userId);

            if (user == null)
            {
                return null;
            }

            bool result = CheckUserPassword(user, password);

            return result ? user : null;
        }

        public string AddRandomPassword(User user)
        {
            string password = RandomStringGeneration(PasswordLength);
            string salt = RandomStringGeneration(SaltByteLength);
            SetPassword(user, password, salt);

            return password;
        }

        public void ChangePassword(User user, string newPassword)
        {
            string salt = RandomStringGeneration(SaltByteLength);
            SetPassword(user, newPassword, salt);
        }

        private static bool IsFairRoll(byte randomValue, byte sequenceLenght)
        {
            int fullSetsOfValues = byte.MaxValue / sequenceLenght;

            return randomValue < sequenceLenght * fullSetsOfValues;
        }

        private string RandomStringGeneration(int length)
        {
            StringBuilder password = new StringBuilder();

            for (int i = 0; i < length; i++)
            {
                byte numberSequence = GetRandomNumberInSequence(NumberSequences);

                switch (numberSequence)
                {
                    case 0:
                        password.Append(UpperCase[GetRandomNumberInSequence((byte)UpperCase.Length)]);
                        break;
                    case 1:
                        password.Append(LowerCase[GetRandomNumberInSequence((byte)LowerCase.Length)]);
                        break;
                    case 2:
                        password.Append(Numbers[GetRandomNumberInSequence((byte)Numbers.Length)]);
                        break;
                    case 3:
                        password.Append(Symbols[GetRandomNumberInSequence((byte)Symbols.Length)]);
                        break;
                    default:
                        throw new ArgumentException($"{nameof(numberSequence)} is not correct");
                }
            }

            return password.ToString();
        }

        private byte[] GenerateSaltedHash(byte[] plainText, byte[] salt)
        {
            var hmacMD5 = new HMACMD5(salt);
            byte[] saltedHash = hmacMD5.ComputeHash(plainText);

            return saltedHash;
        }

        private string ConvertHashToString(byte[] bytes)
        {
            return Convert.ToBase64String(bytes);
        }

        private string ConvertByteToString(byte[] bytes)
        {
            return Encoding.UTF8.GetString(bytes);
        }

        private byte[] ConvertStringToByte(string text)
        {
            return Encoding.UTF8.GetBytes(text);
        }

        private byte GetRandomNumberInSequence(byte sequenceLenght)
        {
            if (sequenceLenght == 0)
            {
                throw new ArgumentOutOfRangeException(nameof(sequenceLenght));
            }

            byte[] randomValue = new byte[1];

            do
            {
                rngCsp.GetBytes(randomValue);
            }
            while (!IsFairRoll(randomValue[0], sequenceLenght));

            return (byte)(randomValue[0] % sequenceLenght);
        }

        private bool CheckUserPassword(User user, string password)
        {
            string passwordHash = user.Password;
            string salt = user.PasswordSalt;
            byte[] bytePassword = ConvertStringToByte(password);
            byte[] byteSalt = ConvertStringToByte(salt);
            byte[] saltHash = GenerateSaltedHash(bytePassword, byteSalt);
            string checkPasswordHash = ConvertHashToString(saltHash);

            return passwordHash == checkPasswordHash;
        }

        private void SetPassword(User user, string password, string salt)
        {
            byte[] bytePassword = ConvertStringToByte(password);
            byte[] byteSalt = ConvertStringToByte(salt);
            byte[] saltHash = GenerateSaltedHash(bytePassword, byteSalt);
            user.PasswordSalt = ConvertByteToString(byteSalt);
            user.Password = ConvertHashToString(saltHash);
        }
    }
}
