﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using Itransition.TeamManagement.Domain.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Itransition.TeamManagement.Domain.Services.Services
{
    public class WorkTaskDomainService : IWorkTaskDomainService
    {
        private IWorkTaskRepository workTaskRepository;

        public WorkTaskDomainService(IWorkTaskRepository workTaskRepository)
        {
            this.workTaskRepository = workTaskRepository;
        }

        public bool Create(WorkTask item)
        {
            return workTaskRepository.Create(item);
        }

        public bool Delete(WorkTask item)
        {
            return workTaskRepository.Delete(item);
        }

        public IEnumerable<WorkTask> GetActiveWithoutLogsBeforePeriod(int userId, DateTime startPeriod, DateTime endPeriod)
        {
            return workTaskRepository.GetActiveWithoutLogsBeforePeriod(userId, startPeriod, endPeriod);
        }

        public IEnumerable<WorkTask> GetByPeriod(int userId, DateTime startPeriod, DateTime endPeriod)
        {
            return workTaskRepository.GetByPeriod(userId, startPeriod, endPeriod);
        }
    }
}
