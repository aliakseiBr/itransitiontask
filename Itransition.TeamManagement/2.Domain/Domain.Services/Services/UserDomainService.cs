﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Filtering;
using Itransition.TeamManagement.Domain.Core.Sorting;
using Itransition.TeamManagement.Domain.Services.ICrossCutting;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using Itransition.TeamManagement.Domain.Services.IServices;

namespace Itransition.TeamManagement.Domain.Services.Services
{
    public class UserDomainService : IUserDomainService
    {
        private IUserRepository userRepository;
        private ITitleRepository titleRepository;
        private ICommentRepository commentRepository;
        private IExpressionGenerator<User> expressionGenerator;
        private IRoleDomainService roleDomainService;
        private IPresentationAuthenticationService presentationAuthenticationService;

        public UserDomainService(
            IUserRepository userRepository,
            ITitleRepository titleRepository,
            ICommentRepository commentRepository,
            IExpressionGenerator<User> expressionGenerator,
            IRoleDomainService roleDomainService,
            IPresentationAuthenticationService presentationAuthenticationService)
        {
            this.userRepository = userRepository;
            this.titleRepository = titleRepository;
            this.commentRepository = commentRepository;
            this.expressionGenerator = expressionGenerator;
            this.roleDomainService = roleDomainService;
            this.presentationAuthenticationService = presentationAuthenticationService;
        }

        public bool Create(User user)
        {
            return userRepository.Create(user);
        }

        public bool Delete(User user)
        {
            user.IsDeleted = true;

            return user.IsDeleted;
        }

        public bool AddComment(User user, Comment comment)
        {
            comment.UserId = user.Id;

            return commentRepository.Create(comment);
        }

        public bool DeleteComment(Comment comment)
        {
            return commentRepository.Delete(comment);
        }

        public IEnumerable<Comment> GetAllComments(int userId)
        {
            return commentRepository.GetAll(userId);
        }

        public IEnumerable<Comment> GetComments(int userId)
        {
            var user = userRepository.Get(userId);

            return user.Comments.OrderByDescending(p => p.CreateDate);
        }

        public IEnumerable<Title> GetAllTitles()
        {
            return titleRepository.GetAll();
        }

        public Title GetTitle(int titleId)
        {
            return titleRepository.Get(titleId);
        }

        public User Get(int userId)
        {
            bool result = AccessToUsers(userId);

            if (result)
            {
                User user = userRepository.Get(userId);
                return user;
            }
            else
            {
                return null;
            }
        }

        public User GetWithRoles(int userId)
        {
            bool result = AccessToUsers(userId);

            if (result)
            {
                User user = userRepository.GetWithRoles(userId);
                return user;
            }
            else
            {
                return null;
            }
        }

        public int GetActiveUsersCount()
        {
            return userRepository.GetActiveUsersCount();
        }

        public IEnumerable<User> GetActiveByCompanyWithRoles(
            int companyId,
            int page,
            int pageSize,
            SortedList<int, SortingParams> sortingParams,
            SortedList<int, FilteringParams> filteringParams)
        {
            SortedList<int, ExpressionSortingParams<User>> expressionSortingParams = expressionGenerator.GetSortDictionaryExpressions(sortingParams);
            Expression<Func<User, bool>> filter = expressionGenerator.GetFilterExpression(filteringParams);

            return userRepository.GetActiveByCompany(companyId, page, pageSize, expressionSortingParams, filter);
        }

        public bool UniqueEmail(string email)
        {
            return userRepository.UniqueEmail(email);
        }

        public int GetActiveUsersCountByCompany(int companyId)
        {
            return userRepository.GetActiveUsersCountByCompany(companyId);
        }

        public int GetActiveFilteringUsersCountByCompany(SortedList<int, FilteringParams> filteringParams, int companyId)
        {
            Expression<Func<User, bool>> filter = expressionGenerator.GetFilterExpression(filteringParams);

            return userRepository.GetActiveFilteringUsersCountByCompany(filter, companyId);
        }

        private bool AccessToUsers(int userId)
        {
            int currentUserId = presentationAuthenticationService.GetCurrentUserId();

            if (roleDomainService.IsCompanyOwner(currentUserId))
            {
                return true;
            }
            else
            {
                if (roleDomainService.IsCompanyOwner(userId))
                {
                    return false;
                }

                return true;
            }
        }
    }
}
