﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using Itransition.TeamManagement.Domain.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Services.Services
{
    public class WorkLogDomainService : IWorkLogDomainService
    {
        private IWorkLogRepository workLogRepository;

        public WorkLogDomainService(IWorkLogRepository workLogRepository)
        {
            this.workLogRepository = workLogRepository;
        }

        public WorkLog Get(int id)
        {
            return workLogRepository.Get(id);
        }

        public bool Create(WorkLog item)
        {
            item.LogDay = item.LogDay.Date;

            return workLogRepository.Create(item);
        }

        public bool Delete(WorkLog item)
        {
            return workLogRepository.Delete(item);
        }

        public IEnumerable<WorkLog> GetByPeriod(int taskId, DateTime startPeriod, DateTime endPeriod)
        {
            return workLogRepository.GetByPeriod(taskId, startPeriod, endPeriod);
        }

        public IEnumerable<WorkLog> GetByPeriodWithTask(int userId, DateTime startPeriod, DateTime endPeriod)
        {
            return workLogRepository.GetByPeriodWithTask(userId, startPeriod, endPeriod);
        }

        public bool LogHoursLimit(DateTime day, double timeSpent, double timeLimit)
        {
            day = day.Date;

            return workLogRepository.LogHoursLimit(day, timeSpent, timeLimit);
        }
    }
}
