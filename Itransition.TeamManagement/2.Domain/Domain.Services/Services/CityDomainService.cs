﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Filtering;
using Itransition.TeamManagement.Domain.Core.Sorting;
using Itransition.TeamManagement.Domain.Services.ICrossCutting;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using Itransition.TeamManagement.Domain.Services.IServices;

namespace Itransition.TeamManagement.Domain.Services.Services
{
    public class CityDomainService : ICityDomainService
    {
        private ICityRepository cityRepository;
        private ICountryRepository countryRepository;
        private IExpressionGenerator<City> expressionGenerator;

        public CityDomainService(ICountryRepository countryRepository, ICityRepository cityRepository, IExpressionGenerator<City> expressionGenerator)
        {
            this.countryRepository = countryRepository;
            this.cityRepository = cityRepository;
            this.expressionGenerator = expressionGenerator;
        }

        public bool CreateCity(Country country, City city)
        {
            bool result = cityRepository.CheckContainsCityName(city.CityName, country.Id);

            if (result)
            {
                return false;
            }
            else
            {
                city.CountryId = country.Id;
                cityRepository.Create(city);

                return true;
            }
        }

        public bool CheckContainsCityName(string cityName, int countryId)
        {
            return cityRepository.CheckContainsCityName(cityName, countryId);
        }

        public bool Delete(City city)
        {
            if (cityRepository.DependsOnActiveItems(city))
            {
                return false;
            }
            else if (cityRepository.DependsOnDeletedItems(city))
            {
                city.IsDeleted = true;

                return true;
            }
            else
            {
                cityRepository.Delete(city);

                return true;
            }
        }

        public IEnumerable<City> GetAllActiveCitiesOfCountry(int countryId)
        {
            var country = countryRepository.Get(countryId);

            return country != null ? cityRepository.GetAllActiveByCountry(country) : null;
        }

        public City Get(int cityId)
        {
            return cityRepository.Get(cityId);
        }

        public IEnumerable<City> GetActiveByCompany(
            int companyId,
            int page,
            int pageSize,
            SortedList<int, SortingParams> sortingParams,
            SortedList<int, FilteringParams> filteringParams)
        {
            SortedList<int, ExpressionSortingParams<City>> expressionSortingParams = expressionGenerator.GetSortDictionaryExpressions(sortingParams);
            Expression<Func<City, bool>> filter = expressionGenerator.GetFilterExpression(filteringParams);

            return cityRepository.GetActiveByCompany(companyId, page, pageSize, expressionSortingParams, filter);
        }

        public IEnumerable<City> GetActiveByCompany(int companyId, int page, int pageSize)
        {
            return cityRepository.GetActiveByCompany(companyId, page, pageSize);
        }

        public int GetActiveCitiesCountByCompany(int companyId)
        {
            return cityRepository.GetActiveCitiesCountByCompany(companyId);
        }
    }
}
