﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Core.Filtering;
using Itransition.TeamManagement.Domain.Core.Sorting;
using Itransition.TeamManagement.Domain.Services.ICrossCutting;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using Itransition.TeamManagement.Domain.Services.IServices;

namespace Itransition.TeamManagement.Domain.Services.Services
{
    public class CountryDomainService : ICountryDomainService
    {
        private IExpressionGenerator<Country> expressionGenerator;
        private ICountryRepository countryRepository;
        private ICityRepository cityRepository;

        public CountryDomainService(ICountryRepository countryRepository, ICityRepository cityRepository, IExpressionGenerator<Country> expressionGenerator)
        {
            this.countryRepository = countryRepository;
            this.cityRepository = cityRepository;
            this.expressionGenerator = expressionGenerator;
        }

        public bool Create(Country country)
        {
            bool result = countryRepository.CheckContainsCountryName(country.CountryName);

            if (result)
            {
                return false;
            }
            else
            {
                return countryRepository.Create(country);
            }
        }

        public bool Delete(Country country)
        {
            if (countryRepository.DependsOnActiveItems(country))
            {
                return false;
            }
            else if (countryRepository.DependsOnDeletedItems(country))
            {
                country.IsDeleted = true;
                var cities = cityRepository.GetAllActiveByCountry(country);

                foreach (var item in cities)
                {
                    item.IsDeleted = true;
                }

                return true;
            }
            else
            {
                return countryRepository.Delete(country);
            }
        }

        public Country Get(int countryId)
        {
            return countryRepository.Get(countryId);
        }

        public IEnumerable<Country> GetActiveByCompany(
            int companyId,
            int page,
            int pageSize,
            SortedList<int, SortingParams> sortingParams,
            SortedList<int, FilteringParams> filteringParams)
        {
            SortedList<int, ExpressionSortingParams<Country>> expressionSortingParams = expressionGenerator.GetSortDictionaryExpressions(sortingParams);
            Expression<Func<Country, bool>> filter = expressionGenerator.GetFilterExpression(filteringParams);

            return countryRepository.GetActiveByCompany(companyId, page, pageSize, expressionSortingParams, filter);
        }

        public IEnumerable<Country> GetActiveByCompany(int companyId, int page, int pageSize)
        {
            return countryRepository.GetActiveByCompany(companyId, page, pageSize);
        }

        public int CountCitiesOfCountry(Country country)
        {
            return cityRepository.GetActiveCitiesCountByCountry(country);
        }

        public bool CheckContainsCountryName(string countryName)
        {
            return countryRepository.CheckContainsCountryName(countryName);
        }

        public IEnumerable<Country> GetActiveByCompany(int companyId)
        {
            return countryRepository.GetActiveByCompany(companyId);
        }

        public IEnumerable<Country> GetActiveWithoutEmptyCitiesByCompany(int companyId)
        {
            return countryRepository.GetActiveWithoutEmptyCitiesByCompany(companyId);
        }

        public int GetActiveCountryCountByCompany(int companyId)
        {
            return countryRepository.GetActiveCountryCountByCompany(companyId);
        }
    }
}
