﻿using Itransition.TeamManagement.Domain.Core.Entities;
using Itransition.TeamManagement.Domain.Services.IRepositories;
using Itransition.TeamManagement.Domain.Services.IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Services.Services
{
    public class RoleDomainService : IRoleDomainService
    {
        private IRoleRepository roleRepository;

        public RoleDomainService(IRoleRepository roleRepository)
        {
            this.roleRepository = roleRepository;
        }

        public IEnumerable<Role> GetAll()
        {
            return roleRepository.GetAllRoles();
        }

        public Role Get(object id)
        {
            return roleRepository.Get(id);
        }

        public Role GetByName(string roleName)
        {
            return roleRepository.GetByName(roleName);
        }

        public bool IsCompanyOwner(int userId)
        {
            return roleRepository.IsCompanyOwner(userId);
        }
    }
}
