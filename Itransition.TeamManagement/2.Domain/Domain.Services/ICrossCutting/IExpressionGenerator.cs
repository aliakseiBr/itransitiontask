﻿using Itransition.TeamManagement.Domain.Core.Filtering;
using Itransition.TeamManagement.Domain.Core.Sorting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Services.ICrossCutting
{
    public interface IExpressionGenerator<T>
        where T : class
    {
        SortedList<int, ExpressionSortingParams<T>> GetSortDictionaryExpressions(SortedList<int, SortingParams> sortingParams);

        Expression<Func<T, bool>> GetFilterExpression(SortedList<int, FilteringParams> filteringParams);
    }
}
