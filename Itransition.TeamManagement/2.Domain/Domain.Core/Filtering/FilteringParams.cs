﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Core.Filtering
{
    public class FilteringParams
    {
        public string FilterField { get; set; }

        public List<string> ChildFields { get; set; }

        public string FilterString { get; set; }
    }
}
