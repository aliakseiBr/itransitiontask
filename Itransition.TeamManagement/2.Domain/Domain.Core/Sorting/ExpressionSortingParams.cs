﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Core.Sorting
{
    public class ExpressionSortingParams<T>
        where T : class
    {
        public Expression<Func<T, string>> ExpressionSortFild { get; set; }

        public bool DescendingSort { get; set; }
    }
}
