﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Core.Sorting
{
    public class SortingParams
    {
        public string SortField { get; set; }

        public List<string> ChildFields { get; set; }

        public bool DescendingSort { get; set; }
    }
}
