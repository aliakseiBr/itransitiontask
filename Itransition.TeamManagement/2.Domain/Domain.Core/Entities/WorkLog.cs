﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Core.Entities
{
    public class WorkLog
    {
        public int Id { get; set; }

        public double Time { get; set; }

        public string Description { get; set; }

        public DateTime LogDay { get; set; }

        public DateTime CreateDate { get; set; }

        public int? WorkTaskId { get; set; }

        public WorkTask WorkTask { get; set; }
    }
}
