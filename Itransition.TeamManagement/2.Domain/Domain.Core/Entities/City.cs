﻿using System;
using System.Collections.Generic;

namespace Itransition.TeamManagement.Domain.Core.Entities
{
    public class City
    {
        public int Id { get; set; }

        public string CityName { get; set; }

        public int? CountryId { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public bool IsDeleted { get; set; }

        public Country Country { get; set; }

        public ICollection<User> Users { get; set; }
    }
}
