﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Core.Entities
{
    public class WorkTask
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public DateTime? CloseDate { get; set; }

        public int? UserId { get; set; }

        public User User { get; set; }

        public ICollection<WorkLog> WorkLogs { get; set; }
    }
}
