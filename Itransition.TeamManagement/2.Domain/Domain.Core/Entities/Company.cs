﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Core.Entities
{
    public class Company
    {
        public int Id { get; set; }

        public string CompanyName { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public bool IsDeleted { get; set; }

        public ICollection<User> Users { get; set; }

        public ICollection<Country> Countries { get; set; }
    }
}
