﻿using System;
using System.Collections.Generic;

namespace Itransition.TeamManagement.Domain.Core.Entities
{
    public class Title
    {
        public int Id { get; set; }

        public string TitleName { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public ICollection<User> Users { get; set; }
    }
}
