﻿using System;
using System.Collections.Generic;

namespace Itransition.TeamManagement.Domain.Core.Entities
{
    public class Country
    {
        public int Id { get; set; }

        public string CountryName { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public bool IsDeleted { get; set; }

        public int? CompanyId { get; set; }

        public Company Company { get; set; }

        public ICollection<City> Cities { get; set; }
    }
}
