﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Core.Entities
{
    public class Role
    {
        public int Id { get; set; }

        public string RoleName { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public ICollection<User> Users { get; set; }
    }
}
