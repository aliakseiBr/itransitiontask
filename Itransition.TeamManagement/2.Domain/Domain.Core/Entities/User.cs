﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Itransition.TeamManagement.Domain.Core.Entities
{
    public class User
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string FullName { get; private set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string PasswordSalt { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public bool IsDeleted { get; set; }

        public int? TitleId { get; set; }

        public Title Title { get; set; }

        public int? CityId { get; set; }

        public City City { get; set; }

        public int? CompanyId { get; set; }

        public Company Company { get; set; }

        public ICollection<Comment> Comments { get; set; }

        public ICollection<Role> Roles { get; set; }

        public ICollection<WorkTask> WorkTasks { get; set; }
    }
}
