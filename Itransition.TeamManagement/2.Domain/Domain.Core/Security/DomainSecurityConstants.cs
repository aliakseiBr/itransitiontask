﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Itransition.TeamManagement.Domain.Core.Security
{
    public static class DomainSecurityConstants
    {
        public const string CompanyOwner = "CompanyOwner";
        public const string User = "User";
    }
}
